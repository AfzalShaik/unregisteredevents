'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

//Department Address
describe(' Department Address Cases ;', function() {
  it('departmentAddressPost returns 200', function(done) {
    var input = {

      'addressTypeValueId': 18,
      'addressLine1': 'madeenaguda',
      'addressLine2': 'hyderabad',
      'addressLine3': 'india',
      'postalCode': '515556',
      'cityId': 4098,
      'stateCode': '98',
      'countryCode': '91',
      'primaryInd': 'N',
      'departmentId': 1004,
      'campusId': 388589390,
      'createDatetime': '2017-08-02T10:07:26.681Z',
      'updateDatetime': '2017-08-02T10:07:26.681Z',
      'createUserId': 1,
      'updateUserId': 1,
    };

    var url = 'http://localhost:3000/api/Addresses';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  //department Address PUT
  it('departmentAddressPUT returns 200', function(done) {
    var input =
     {'departmentId': 9999,
      'campusId': 959816205,
      'addressId': 40751584,
      'addressLine2': 'ece block',
      'addressTypeValueId': 19,
      'cityId': 1,
      'stateCode': 'AN',
      'countryCode': 'IN'};

    var url = 'http://localhost:3000/api/DepartmentAddresses/UpdateDepartmentAddress';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //department Address GET
  it('departmentAddress GET returns 200', function(done) {
    var url = 'http://localhost:3000/api/DepartmentAddresses/getCampusDeptAddress?departmentId=9999&campusId=959816205&addressId=40751584';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'GET',
      json: true,


    }, function(err, response, body) {
      if (err) {
        return err;
      }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //Department Address Delete Method
  it('departmentAddress DELETE returns 200', function(done) {
    var url = 'http://localhost:3000/api/DepartmentAddresses/getCampusDeptAddress?departmentId=9999&campusId=959816205&addressId=40751584';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'DELETE',
      json: true,


    }, function(err, response, body) {
      if (err) {
        return err;
      }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });
});
