var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require("chai").expect;
var request = require("request");
chai.use(chaiHttp);


//Organization Address
describe(' Organization Address Cases ;', function () {
//Organization address post method
  it('Organization Address POST returns 200', function (done) {
var input = {
"organizationId":56767,
"companyId" :1007,
 "addressTypeValueId": 19,
  "addressLine1": "string",
  "addressLine2": "string",
  "addressLine3": "string",
  "postalCode": "700108",
  "cityId": 7,
  "stateCode": "WB",
  "countryCode": "IN",
  "primaryInd": "N",
  "createDatetime": "2017-08-08T05:51:50.369Z",
  "updateDatetime": "2017-08-08T05:51:50.369Z",
  "createUserId": 1,
  "updateUserId": 1
}
var url = "http://localhost:3000/api/OrganizationAddresses/createOrganizationAddress";
var request = require('request');

request({
  url: url,
  method: "POST",
  json: true,
  body: input

}, function(err, response, body) {
  if(err) { return err; }
    expect(response.statusCode).to.equal(200);

  done();
});

})
//Organization address put method
  it('Organization Address PUT returns 200', function (done) {
var input =
{
"organizationId":56767,
"addressId":40751562,
"companyId" :1007,
 "addressTypeValueId": 18,
"addressLine1":"addressLine1"
}

var url = "http://localhost:3000/api/OrganizationAddresses/UpdateOrganizationAddress";
var request = require('request');

request({
  url: url,
  method: "PUT",
  json: true,
  body: input

}, function(err, response, body) {
  if(err) { return err; }
    expect(response.statusCode).to.equal(200);

  done();
});
})

//Organization address get method
  it('Organization Address get returns 200', function (done) {
var input =
{
      "organizationId": 56767,
      "companyId": 1007,
      "addressId": 40751562
}

var url = "http://localhost:3000/api/OrganizationAddresses/getOrganizationAddress?organizationId=56767&companyId=1007&addressId=40751562";
var request = require('request');

request({
  url: url,
  method: "GET",
  json: true

}, function(err, response, body) {
  if(err) { return err; }
    expect(response.statusCode).to.equal(200);

  done();
});
})
})
