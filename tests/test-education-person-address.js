var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require("chai").expect;
var request = require("request");
chai.use(chaiHttp);

//education person address
describe(' education person address Cases ;', function () {
//education person address POST service
  it('education person address POST returns 200', function (done) {

      var input =
      {
  "educationPersonId": 556,
"addressTypeValueId": 20,
    "addressLine1": "string",
    "addressLine2": "string",
    "addressLine3": "string",
    "postalCode": "380060",
    "cityId": 4,
    "stateCode": "GJ",
    "countryCode": "IN",
    "primaryInd": "N",
    "createDatetime": "2017-08-08T09:51:46.052Z",
    "updateDatetime": "2017-08-08T09:51:46.052Z",
    "createUserId": 1,
    "updateUserId": 1,
    "campusId": 388589390
}

    var url = "http://localhost:3000/api/EducationPersonAddresses/createEducationPersonAddress";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "POST",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });
//education person address PUT service
  it('education person address PUT returns 200', function (done) {

      var input =

  {"campusId":388589390,
  "educationPersonId":555,
  "addressId":40133,
  "addressLine1":"addressLine1",
   "addressTypeValueId": 18
  }




    var url = "http://localhost:3000/api/EducationPersonAddresses/updateEducationPersonAddress";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "PUT",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //education person address GET service
    it('education person address GET returns 200', function (done) {

        var input =
        {
          "educationPersonId": 556,
        "addressId": 40751708,
        "campusId": 388589390
    }

      var url = "http://localhost:3000/api/EducationPersonAddresses/getEducationPersonAddress?educationPersonId=556&campusId=388589390&addressId=40751708";
      var request = require('request');

      // console.log('Request:');
      // console.log(input);
      request({
        url: url,
        method: "GET",
        json: true

      }, function(err, response, body) {
        if(err) { return err; }

          expect(response.statusCode).to.equal(200);

        done();
      });
    });
});
