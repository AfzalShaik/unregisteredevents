'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

//Employer job role
describe(' Employer job role Cases ;', function() {
  it('Employer job role POST returns 200', function(done) {
    var input = {
      'companyId': 1026,
      'jobRoleName': 'Lecturer',
      'jobRoleTypeValueId': 323,
      'skillSet': 'Mean,Java,Webservices',
      'summary': 'programmer analyst',
      'createUserId': 1,

    };

    var url = 'http://localhost:3000/api/JobRoles';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  //job role  put
  it('Employer job role PUT returns 200', function(done) {
    var input = {
      'jobRoleId': 3333,
      'companyId': 1026,
      'jobRoleName': 'job role Name',
      'jobRoleTypeValueId': 323,
      'skillSet': 'Mean,Java,Webservices',
      'summary': 'programmer analyst',
      'createDatetime': '2017-08-11T09:21:30.000Z',
      'updateDatetime': '2017-08-18T12:15:18.563Z',
      'createUserId': 1,
      'updateUserId': 1,
    };
    var url = 'http://localhost:3000/api/JobRoles/updateJobRole';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //job role get
  it('Employer job role GET returns 200', function(done) {
    var url = 'http://localhost:3000/api/JobRoles/getJobRoleDetails?companyId=1026&jobRoleId=3333';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'GET',
      json: true,
    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('body', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
});
