'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);
//Program
describe(' Program Cases ;', function() {
  //program post method
  it('Program POST returns 200', function(done) {
    var input = {

      'campusId': 959816197,
      'departmentId': 9999,
      'programName': 'program name',
      'description': 'desc',
      'programTypeValueId': 62,
      'programClassValueId': 68,
      'programCatValueId': 55,
      'numberOfStudents': 434334,
      'startDate': '2017-08-08T07:06:47.347Z',
      'createDatetime': '2017-08-08T07:06:47.347Z',
      'updateDatetime': '2017-08-08T07:06:47.347Z',
      'createUserId': 1,
      'updateUserId': 1,
      'programMajorValueId': 99,
      'offCampusInd': 'N',
    };

    var url = 'http://localhost:3000/api/Programs';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //program put method
  it('Program PUT returns 200', function(done) {
    var input = {

      'programId': 101011,
      'campusId': 959816197,
      'departmentId': 9999,
      'programName': 'program name',
      'description': 'description',
    };

    var url = 'http://localhost:3000/api/Programs/updateProgram';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //program get method
  it('Program GET returns 200', function(done) {
    var input = {

      'programId': 101011,
    };

    var url = 'http://localhost:3000/api/Programs/getProgram?programId=101011';
    var request = require('request');

    request({
      url: url,
      method: 'GET',
      json: true,
    }, function(err, response, body) {
      if (err) {
        return err;
      }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });
});
