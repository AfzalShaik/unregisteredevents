/**
 * Created by Yogi on 09-08-2017.
 */
'use strict';
function persistContactDataInForeignEntity(lengthOfSavedContacts,
         savedContactsInfo, foreignEntiy, inputObjToPersist, callBack) {
  var count = 0;
  savedContactsInfo.map(function(contactData) {
    inputObjToPersist['contactId'] = contactData.contactId;
    inputObjToPersist['primaryInd'] = contactData.primaryInd;
    foreignEntiy.create(inputObjToPersist, function(error, savedEntityResp) {
      if (error) {
        error.requestStatus = false;
        callBack(error, null);
      } else {
        console.log('contact foreign entity record created.....');
        count++;
        if (lengthOfSavedContacts == count) {
          savedContactsInfo.requestStatus = true;
          callBack(null, savedEntityResp);
        }
      }
    });
  });
}
exports.persistContactDataInForeignEntity = persistContactDataInForeignEntity;
