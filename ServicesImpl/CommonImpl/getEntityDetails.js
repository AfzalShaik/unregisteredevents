/**
 * Created by Yogi on 31-07-2017.
 * Author: Yogi
 * Purpose: defined a function to get address details of various entities like campus,company,campus department addresses
 * Input: while calling this function we need to pass filter and include model along with entity name
 * Output: This will retrieve address data based on filter and model name
 */
'use strict';
var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var async = require('async');
// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Does this only deal with addressDetails or is it a copy paste mistake that has to be fixed?

/**
 * This is a getEntityFunction for including data from related models.
 *It takes input an inputObj that is used to find the deatails in the database
 * @param {Object} inputObj
 * @param {Object} modelName
 * @param {Object} includeRelationModel
 * @param {Function} callBc
 */
function entityDetailsUsingIncludeFilter(
  inputObj,
  modelName,
  includeRelationModel,
  callBc) {
  console.log('entityDetailsUsingIncludeFilter: ' +
    JSON.stringify(inputObj) +
    ' include: ' +
    includeRelationModel);
  modelName.find({
    'where': {
      'and': [inputObj],
    },
    'include': [includeRelationModel],
  }, function(error, entityRes) {
    if (error) {
      error.requestStatus = false;
      callBc(error, null);
    } else if (entityRes.length > 0) {
      var response = {};
      response.data = entityRes;
      response.requestStatus = true;
      callBc(null, response);
    } else if (entityRes.length == 0) {
      var response = {};
      response.data = entityRes;
      response.requestStatus = false;
      callBc(null, response);
    } else {
      // To throw an error when ever request contains invalid input parameters
      errorResponse(callBc);
    }
  });
}

// TODO: CODEREVIEW - SWAROOP - 15AUG17 --  Use logger API.
/**
 *entityDetailsById-function deals getting the data by taking id as an input parameter
 *@constructor
 * @param {object} inputFilterObject - contains the id details that need to be found
 * @param {string} modelName - contains the model name to search in that model
 * @param {function} callBack - deals with the response
 */
function entityDetailsById(inputFilterObject, modelName, callBack) {
  // console.log('entityDetailsById: ' + JSON.stringify(inputFilterObject));
  // console.log(inputFilterObject);
  modelName.find({
    'where': {
      'and': [inputFilterObject],
    },
  }, function(error, modelResponse) {
    // console.log('in model response', modelResponse);

    if (error) {
      error.requestStatus = false;
      console.log('in error');
      callBack(error, null);
    } else if (modelResponse.length === 0) {
      errorResponse(callBack);
    } else {
      var response = {};
      response.data = modelResponse;
      response.requestStatus = true;
      callBack(null, response);
    }
  });
}
/**
 *getEntityDetailsCount-function deals with getting the count
 *@constructor
 * @param {object} inputFilterObject - contains the id forr which it should find the count
 * @param {string} modelName - contains the model name to search in that model
 * @param {function} done - deals with the response
 */
function getEntityDetailsCount(inputFilterObject, modelName, done) {
  console.log('getEntityDetailsCount: ' + JSON.stringify(inputFilterObject));
  modelName.count({
    'and': [inputFilterObject],
  }, function(error, entityRecordsCount) {
    if (error) {
      done(error, null);
    } else {
      console.log('entityRecordsCount: ' + entityRecordsCount);
      done(null, entityRecordsCount);
    }
  });
}
exports.getEntityDetailsCount = getEntityDetailsCount;
exports.entityDetailsById = entityDetailsById;
exports.entityDetailsUsingIncludeFilter = entityDetailsUsingIncludeFilter;
// to get address details
function getCompleteAddress(
  inputObj,
  modelName,
  includeRelationModel,
  callBc) {
  modelName.find({
    'where': {
      'and': [inputObj],
    },
    'include': [includeRelationModel],
  }, function(error, entityRes) {
    if (error) {
      error.requestStatus = false;
      callBc(error, null);
    } else if (entityRes.length > 0) {
      async.map(entityRes, getCityDetails, function(error, addressResp) {
        if (error) {
          callBc(error, null);
        } else {
          var response = {};
          response.campusId = entityRes[0].campusId;
          response.companyId = entityRes[0].companyId;
          response.departmentId = entityRes[0].departmentId;
          response.addressDetails = addressResp;
          response.requestStatus = true;
          callBc(null, response);
        }
      });
    } else if (entityRes.length == 0) {
      var response = {};
      response.data = entityRes;
      response.requestStatus = false;
      callBc(null, response);
    } else {
      // To throw an error when ever request contains invalid input parameters
      errorResponse(callBc);
    }
  });
}

function getCityDetails(obj, cb) {
  var server = require('../../server/server');
  var addressRelation = server.models.Address;
  var inputObj = {};
  inputObj['cityId'] = obj.cityId;
  inputObj['addressId'] = obj.addressId;
  addressRelation.findOne({
    'where': {
      'and': [inputObj],
    },
    'include': ['cityDetails'],
  }, function(error, addRes) {
    addRes.primaryInd = obj.primaryInd;
    cb(null, addRes);
  });
}
exports.getCompleteAddress = getCompleteAddress;

function totalCampusDetailsUsingIncludeFilter(inputObj, modelName, includeRelationModel, callBc) {
  console.log('entityDetailsUsingIncludeFilter: ' + JSON.stringify(inputObj) + ' include: ' + includeRelationModel);
  modelName.find({
    'where': {
      'and': [inputObj],
    },
    'include': [includeRelationModel],
  }, function(error, entityRes) {
    if (error) {
      error.requestStatus = false;
      callBc(error, null);
    } else if (entityRes.length > 0) {
      // var response = {};
      // response.data = entityRes;
      // response.requestStatus = true;
      callBc(null, entityRes);
    } else if (entityRes.length == 0) {
      var response = {};
      response.data = entityRes;
      response.requestStatus = false;
      callBc(null, response);
    } else {
      // To throw an error when ever request contains invalid input parameters
      errorResponse(callBc);
    }
  });
}
exports.totalCampusDetailsUsingIncludeFilter = totalCampusDetailsUsingIncludeFilter;
