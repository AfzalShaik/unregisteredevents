/**
 * Created by Yogi on 01-08-2017.
 */
'use strict';
var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;

// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Breakup into smaller methods.
// Add code comments too on what this method does.
/**
   *deleteEntityRecordExceptPrimary-deletes the record by seacrhing data with input fields
   *@constructor
   * @param {object} primaryCheckInput - contains all the input need to get find
   * @param {object} inputFilterObject - contains all the data need to get deleated
   * @param {object} primaryModelName - contains model name
   * @param {object} foreignModelName - contains model name
   * @param {function} callBc - deals with the response
   */
function deleteEntityRecordExceptPrimary(primaryCheckInput,
                                          inputFilterObject,
                                          primaryModelName,
                                          foreignModelName,
                                          callBack) {
  console.log('deleteEntityRecordExceptPrimary: ' + 'inputFilterObject: ' +
    JSON.stringify(inputFilterObject) + ' primaryCheckInput: ' + JSON.stringify(primaryCheckInput)); // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Move to debug() logger too
  primaryModelName.findOne({'where': {'and': [primaryCheckInput, {'primaryInd': 'Y'}]}}, function(error, primaryModelResp) {
    if (error) {
      console.log('error:::::::::::::::::::::::::::::::::::; ', error); // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Remove all console.log and move to logger API
      error.requestStatus = false;
      callBack(error, null);
    } else if (primaryModelResp) {
      console.log('primaryModelResp:------------------------------------------------------ ', primaryModelResp);
      errorResponse(callBack);
    } else {
      foreignModelName.destroyAll({'and': [inputFilterObject]}, function(error, foreignModelResp) {
        console.log('foreignModelResp: ' + JSON.stringify(foreignModelResp));
        if (error) {
          error.requestStatus = false;
          callBack(error, null);
        } else if (foreignModelResp && foreignModelResp.count >= 1) {
          primaryModelName.destroyAll(primaryCheckInput, function(error, primaryMdlResp) {
            if (error) {
              error.requestStatus = false;
              callBack(error, null);
            } else if (primaryMdlResp && primaryMdlResp.count >= 1) {
              var response = {};
              response.data = primaryMdlResp;
              response.requestStatus = true;
              callBack(null, response);
            } else {
              errorResponse(callBack);
            }
          });
        } else {
          errorResponse(callBack);
        }
      });
    } });
}
/**
   *deleteSingleEntityDetails-delete single entity details
   *@constructor
   * @param {object} inputFilterObject - contains all the data by which we find
   * @param {string} modelName - contains model name from where it shuould delete
   * @param {function} cb - deals with the response
   */
function deleteSingleEntityDetails(inputFilterObject, modelName, callBack) {
  modelName.destroyAll(inputFilterObject, function(error, modelResponse) {
    if (error) {
      error.requestStatus = false;
      callBack(error, null);
    } else if (modelResponse && modelResponse.count >= 1) {
      var response = {};
      response.data = modelResponse;
      response.requestStatus = true;
      callBack(null, response);
    } else {
      errorResponse(callBack);
    }
  });
}
exports.deleteSingleEntityDetails = deleteSingleEntityDetails;
exports.deleteEntityRecordExceptPrimary = deleteEntityRecordExceptPrimary;