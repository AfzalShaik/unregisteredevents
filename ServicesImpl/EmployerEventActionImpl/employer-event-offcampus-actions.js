'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');

var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;

//off campus event actions
function offCampusActions(eventResponseData, inputData, cb) {
  if (inputData.action == 'Publish Event to Off Campus Students') {
    //publish event to off-campus students
    publishEventToOffCampusStudents(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb (null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Shortlisted') {
    //off-campus student short listed function
    shortListedOffCampusStudents(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb (null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Offer') {
    //employer event offer action for off-campus students
    offerOffCampusStudent(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb (null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Reject') {
    //employer event reject action for off-campus students
    rejectOffCampusStudent(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb (null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Schedule') {
    //employer event schedule action for off-campus students
    scheduleOffCampusStudent(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb (null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else {
    var errResp = {};
    errResp.message = 'Action Not Found';
    errResp.status = 'failure';
    cb(errResp, null);
  }
};

//Employer publish event  to off-campus studetns
function publishEventToOffCampusStudents(eventResponseData, inputData, cb) {
  if (eventResponseData.eventStatusValueId == 313 || eventResponseData.eventStatusValueId == 317 || eventResponseData.eventStatusValueId == 318 || eventResponseData.eventStatusValueId == 319 || eventResponseData.eventStatusValueId == 320) {
    var eventApprovedObj = {};
    eventApprovedObj = eventResponseData;
    eventApprovedObj.eventStatusValueId = 314;
    eventResponseData.updateAttributes(eventApprovedObj, function(err, info) {
      if (err) {
        cb(err, null);
      } else {
        var resp = {};
        resp.message = 'event published';
        cb(null, resp);
      }
    });
  } else {
    var errResp = {};
    errResp.message = 'Can"t publish';
    errResp.status = 'failure';
    cb(errResp, null);
  }
}

//Employer event short listed off campus students
function shortListedOffCampusStudents(eventResponseData, inputData, cb) {
  var eventStudentListModel = server.models.EventStudentList;
  var inputOfferStudentObject = {};
  inputOfferStudentObject['studentId'] = inputData.studentId;
  //find student in event student list
  eventStudentListModel.findOne({
    'where': {
      'and': [inputOfferStudentObject],
    },
  }, function(err, eventStudentListResp) {
    if (err) {
      cb(err, null);
    } else {
      var eventStudentListObj = {};
      eventStudentListObj = eventStudentListResp;
      eventStudentListObj.candidateStatusValueId = 377;
      eventStudentListResp.updateAttributes(eventStudentListObj, function(err, studentListUpdateInfo) {
        if (err) {
          cb(err, null);
        } else {
          var response = {};
          response.message = 'student shortlisted';
          cb(null, response);
        }
      });
    }
  });
}

//Employer event Offer action
function offerOffCampusStudent(eventResponseData, inputData, cb) {
  var eventStudentListModel = server.models.EventStudentList;
  var inputOfferStudentObject = {};
  inputOfferStudentObject['studentId'] = inputData.studentId;
    //find student in event student list
  eventStudentListModel.findOne({
    'where': {
      'and': [inputOfferStudentObject],
    },
  }, function(err, eventStudentListResp) {
    if (err) {
      cb(err, null);
    } else {
      var eventStudentListObj = {};
      eventStudentListObj = eventStudentListResp;
      eventStudentListObj.offerStatusValueId = 249;
      eventStudentListObj.candidateStatusValueId = 381;
      eventStudentListResp.updateAttributes(eventStudentListObj, function(err, studentListUpdateInfo) {
        if (err) {
          cb(err, null);
        } else {
          var response = {};
          response.message = 'student offered';
          cb(null, response);
        }
      });
    }
  });
    //end update student list
}

//Employer event reject action
function rejectOffCampusStudent(eventResponseData, inputData, cb) {
  var eventStudentListModel = server.models.EventStudentList;
  var inputRejectStudentObject = {};
  inputRejectStudentObject['studentId'] = inputData.studentId;
      //find student in event student list
  eventStudentListModel.findOne({
    'where': {
      'and': [inputRejectStudentObject],
    },
  }, function(err, eventStudentListResp) {
          //
    if (err) {
      cb(err, null);
    } else {
      var eventStudentListObj = {};
      eventStudentListObj = eventStudentListResp;
      eventStudentListObj.candidateStatusValueId = 378;
      eventStudentListResp.updateAttributes(eventStudentListObj, function(err, studentListUpdateInfo) {
        if (err) {
          cb(err, null);
        } else {
          var response = {};
          response.message = 'student rejected';
          cb(null, response);
        }
      });
            //
    }
          //
  });
      //end update student list
}

//schedule off-campus student
function scheduleOffCampusStudent(eventResponseData, inputData, cb) {
  var updateEmpEvent = {};
  updateEmpEvent = eventResponseData;
  updateEmpEvent.eventStatusValueId = 319;
  updateEmpEvent.scheduledDate = new Date();
  updateEmpEvent.scheduledStartTime = new Date().getTime();
  console.log(updateEmpEvent.scheduledStartTime);
  eventResponseData.updateAttributes(updateEmpEvent, function(err, info) {
    if (err) {
      cb(err, null);
    } else {
      var response = {};
      response.message = 'event scheduled';
      cb(null, response);
    }
  });
}

exports.offCampusActions = offCampusActions;
