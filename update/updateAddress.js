var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var Address = server.models.Address;
 /**
   *updateAddress-function deals with updating the address
   *@constructor
   * @param {object} addressData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function updateAddress (addressData,cb){
  Address.findOne({'where':{"addressId": addressData.addressId}}, function (err, AddressResp) {
    //if record found it will the  updateAttributes
    AddressResp.updateAttributes(addressData,function(err, info){
      if(err){
        cb(err,info)
      }else{
        //making requestStatus is true;
        logger.info("Campus Address Updated Successfully");
        info["requestStatus"] = true;
        info["updateDatetime"] = new Date();
        cb(null, info);
      }

    })
  })

}
exports.updateAddress = updateAddress;
