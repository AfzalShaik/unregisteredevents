'use strict';
var server = require('../server/server');

// TODO: CODEREVIEW - SWAROOP - 15AUG17 --  Use proper 2 spaces indentation.
//validating addressTypeValueId in addressTypeCodeFunction
function addressTypeCodeFunction(addressData, callbackAddressFunction) {
  var addressVal = false;
  if (addressData.addressTypeValueId) {
    var LookupValue = server.models.LookupValue;
    var LookupType = server.models.LookupType;
    LookupValue.find({
      'where': {
        'lookupValueId': addressData.addressTypeValueId,
      },
    }, function(err, lookupValueId) {
      if (err) {
        addressVal = false;

        callbackAddressFunction(addressVal);
      } else if (lookupValueId.length > 0) {
        var typeId = lookupValueId[0].lookupTypeId;
        LookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            addressVal = false;
            callbackAddressFunction(addressVal);
          } else if (lookupTypeId.length > 0) {
            var lookupCode = lookupTypeId[0].lookupCode;
            if (lookupCode == 'ADDRESS_TYPE_CODE') {
              addressVal = true;
              callbackAddressFunction(addressVal);
            } else {
              addressVal = false;
              callbackAddressFunction(addressVal);
            }
          } else {
            addressVal = false;

            callbackAddressFunction(addressVal);
          }
        });
      } else {
        addressVal = false;

        callbackAddressFunction(addressVal);
      }
    });
  } else {
    addressVal = true;
    callbackAddressFunction(addressVal);
  }
}

//addressFunction for validating cityId,countryId and StateID
function addressFunction(addData, callbackAddFunction) {
  var addressValue = false;
  if (addData.cityId || addData.stateCode || addData.countryCode) {
    if (addData.cityId && addData.stateCode && addData.countryCode) {
      var City = server.models.City;

      City.findOne({
        'where': {
          'and': [{
            'cityId': addData.cityId,
          }, {
            'stateCode': addData.stateCode,
          }, {
            'countryCode': addData.countryCode,
          }],
        },
      }, function(err, CityID) {
        if (err) {
          addressValue = false;
          callbackAddFunction(addressValue);
        } else if (CityID) {
          addressValue = true;
          callbackAddFunction(addressValue);
        } else {
          addressValue = false;
          callbackAddFunction(addressValue);
        }
      });
    } else {
      addressValue = false;
      callbackAddFunction(addressValue);
    }
  } else {
    addressValue = true;
    callbackAddFunction(addressValue);
  }
}

//validating contactTypeValueId in contactTypeCodeFunction
function contactTypeCodeFunction(contactData, callbackContactFunction) {
  var contactVal = false;
  if (contactData.contactTypeValueId) {
    var LookupValue = server.models.LookupValue;
    var LookupType = server.models.LookupType;
    LookupValue.find({
      'where': {
        'lookupValueId': contactData.contactTypeValueId,
      },
    }, function(err, lookupValueId) {
      if (err) {
        contactVal = false;

        callbackContactFunction(contactVal);
      } else if (lookupValueId.length > 0) {
        var typeId = lookupValueId[0].lookupTypeId;
        LookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            contactVal = false;
            callbackContactFunction(contactVal);
          } else if (lookupTypeId.length > 0) {
            var lookupCode = lookupTypeId[0].lookupCode;
            if (lookupCode == 'CONTACT_TYPE_CODE') {
              contactVal = true;
              callbackContactFunction(contactVal);
            } else {
              contactVal = false;
              callbackContactFunction(contactVal);
            }
          } else {
            contactVal = false;

            callbackContactFunction(contactVal);
          }
        });
      } else {
        contactVal = false;

        callbackContactFunction(contactVal);
      }
    });
  } else {
    contactVal = true;
    callbackContactFunction(contactVal);
  }
}

//validating companySizeValueId in companySizeFunction
function companySizeFunction(companySizeData, callbackSizeFunction) {
  var sizeVal = false;
  if (companySizeData.companySizeValueId) {
    var LookupValue = server.models.LookupValue;
    var LookupType = server.models.LookupType;
    LookupValue.find({
      'where': {
        'lookupValueId': companySizeData.companySizeValueId,
      },
    }, function(err, lookupValueId) {
      if (err) {
        sizeVal = false;

        callbackSizeFunction(sizeVal);
      } else if (lookupValueId.length > 0) {
        var typeId = lookupValueId[0].lookupTypeId;
        LookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            sizeVal = false;
            callbackSizeFunction(sizeVal);
          } else if (lookupTypeId.length > 0) {
            var lookupCode = lookupTypeId[0].lookupCode;
            if (lookupCode == 'COMPANY_SIZE_CODE') {
              sizeVal = true;
              callbackSizeFunction(sizeVal);
            } else {
              sizeVal = false;
              callbackSizeFunction(sizeVal);
            }
          } else {
            sizeVal = false;

            callbackSizeFunction(sizeVal);
          }
        });
      } else {
        sizeVal = false;

        callbackSizeFunction(sizeVal);
      }
    });
  } else {
    sizeVal = true;
    callbackSizeFunction(sizeVal);
  }
}

//validating companyTypeValueId in companyTypeFunction
function companyTypeFunction(companyTypeData, callbackTypeFunction) {
  var typeVal = false;
  if (companyTypeData.companyTypeValueId) {
    var LookupValue = server.models.LookupValue;
    var LookupType = server.models.LookupType;
    LookupValue.find({
      'where': {
        'lookupValueId': companyTypeData.companyTypeValueId,
      },
    }, function(err, lookupValueId) {
      if (err) {
        typeVal = false;

        callbackTypeFunction(typeVal);
      } else if (lookupValueId.length > 0) {
        var typeId = lookupValueId[0].lookupTypeId;
        LookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            typeVal = false;
            callbackTypeFunction(typeVal);
          } else if (lookupTypeId.length > 0) {
            var lookupCode = lookupTypeId[0].lookupCode;
            if (lookupCode == 'COMPANY_TYPE_CODE') {
              typeVal = true;
              callbackTypeFunction(typeVal);
            } else {
              typeVal = false;
              callbackTypeFunction(typeVal);
            }
          } else {
            typeVal = false;

            callbackTypeFunction(typeVal);
          }
        });
      } else {
        typeVal = false;

        callbackTypeFunction(typeVal);
      }
    });
  } else {
    typeVal = true;
    callbackTypeFunction(typeVal);
  }
}

//validating industryTypeValueId in industryTypeFunction
function industryTypeFunction(industryTypeData, callbackIndFunction) {
  var industryTypeVal = false;
  if (industryTypeData.industryTypeValueId) {
    var LookupValue = server.models.LookupValue;
    var LookupType = server.models.LookupType;
    LookupValue.find({
      'where': {
        'lookupValueId': industryTypeData.industryTypeValueId,
      },
    }, function(err, lookupValueId) {
      if (err) {
        industryTypeVal = false;

        callbackIndFunction(industryTypeVal);
      } else if (lookupValueId.length > 0) {
        var typeId = lookupValueId[0].lookupTypeId;
        LookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            industryTypeVal = false;
            callbackIndFunction(industryTypeVal);
          } else if (lookupTypeId.length > 0) {
            var lookupCode = lookupTypeId[0].lookupCode;
            if (lookupCode == 'INDUSTRY_TYPE_CODE') {
              industryTypeVal = true;
              callbackIndFunction(industryTypeVal);
            } else {
              industryTypeVal = false;
              callbackIndFunction(industryTypeVal);
            }
          } else {
            industryTypeVal = false;

            callbackIndFunction(industryTypeVal);
          }
        });
      } else {
        industryTypeVal = false;

        callbackIndFunction(industryTypeVal);
      }
    });
  } else {
    industryTypeVal = true;
    callbackIndFunction(industryTypeVal);
  }
}
exports.contactTypeCodeFunction = contactTypeCodeFunction;
exports.addressTypeCodeFunction = addressTypeCodeFunction;
exports.addressFunction = addressFunction;
exports.companySizeFunction = companySizeFunction;
exports.companyTypeFunction = companyTypeFunction;
exports.industryTypeFunction = industryTypeFunction;
