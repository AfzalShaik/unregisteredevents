'use strict';
var server = require('../server/server');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var eventdetails = server.models.EventStudentList;
 /**
   *updateEventDetails-function deals with updating the event details
   *@constructor
   * @param {object} input - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function updateEventDetails(input, cb) {
  if (input) {
    if (input.studentListId) {
      var inputData = input;
      findAndUpdate(inputData, function(error, output) {
        if (error) {
          throwError(JSON.stringify(error), cb);
        } else {
          cb(null, output);
        }
      });
    }
    else {
      throwError('Invalid Input', cb);
    }
  }
  else {
    throwError('Invalid Input', cb);
  }
};
 /**
   *findAndUpdate-function deals finding and updating the data
   *@constructor
   * @param {object} campusData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function findAndUpdate(input, cb) {
  var inputFilterObject = {};
  inputFilterObject['studentListId'] = input.studentListId;
  findEntity(inputFilterObject, eventdetails, function(error, response) {
    if (error) {
      throwError(error, cb);
    } else if (response == 0) {
      throwError('Invalid studentListId', cb);
    } else {
      response.data.updateAttributes(input, function(err, info) {
        if (err) {
          throwError(err, cb);
        } else {
          logger.info('eventdetails record Updated Successfully');
          info['requestStatus'] = true;
          info['updateDatetime'] = new Date();
          cb(null, info);
        }
      });
    }
  });
};
exports.updateEventDetails = updateEventDetails;
