var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var allmethods = require("./allmethods");
var Joi = require('joi');
 /**
   *updateOrgContact-function deals with updating organization contact
   *@constructor
   * @param {object} contactData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function updateOrgContact (contactData,cb){

  //validating the input
  if(contactData.createDatetime || contactData.updateDatetime || contactData.createUserId || contactData.updateUserId){
    var error = new Error("You cannot perform update operation: ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, contactData);
  }else{
    //validating the input json
    var schema = Joi.object().keys({
        organizationId: Joi.number().integer().max(999999999999999).required(),
        companyId: Joi.number().integer().max(999999999999999).required(),
        contactId: Joi.number().max(999999999999999).required(),
        contactTypeValueId:Joi.number().max(999999999999999),
        contactInfo:Joi.string().max(100),
    });
    Joi.validate(contactData, schema, function (err, value) {
      if(err){
        cb(err,contactData)
      }else{
        var typeCodeValue;
         var OrganizationContact = server.models.OrganizationContact;
          var Contact = server.models.Contact;
          //checking record with companyId,organizationId and contactId
          allmethods.contactTypeCodeFunction(contactData,function(typeCodeCheck){
            typeCodeValue = typeCodeCheck;
            if(typeCodeValue==false){
              var error = new Error("contactTypeValueId is not valid");
              error.statusCode = 422;
              error.requestStatus=false;
              cb(error, contactData);
            } else{
               OrganizationContact.findOne({'where':{"and":[{"companyId": contactData.companyId},{"organizationId":contactData.organizationId},{"contactId": contactData.contactId}]}}, function (err, orgList) {
                 if(err){
                    cb(err, contactData);
                 }else if (orgList){
                   // console.log("orgList--------------------- ",orgList);

                   //checking record with contactId in contact table
                     Contact.findOne({'where':{"contactId": contactData.contactId}}, function (err, contactResp) {
                       //update the contact table
                       if(err){
                         cb(err,contactResp)
                       }else{
                         contactResp.updateAttributes(contactData,function(err, infoOrg){
                           if(err){
                             cb(err,infoOrg)
                           }else{
                             //if success requestStatus will becomes true
                             infoOrg["requestStatus"] = true;
                             infoOrg["updateDatetime"] = new Date();
                             cb(null, infoOrg);
                           }

                         })
                       }

                     })

                 }
                 else{
                   //throws error when invalid contatcId or organizationId or companyId
                   var error = new Error("Invalid contactId or companyId or organizationId ");
                   error.statusCode = 422;
                   error.requestStatus=false;
                    cb(error, contactData);
                 }
               })
             }
           })
      }
    })
  }

}
exports.updateOrgContact = updateOrgContact;
