'use strict';
var server = require('../server/server');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var unregstudent = server.models.UnregisterCampusStudent;
 /**
   *updateUnregStudentDetails-function deals with updating the unregistered student details
   *@constructor
   * @param {object} input - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function updateUnregStudentDetails(input, cb) {
  if (input) {
    if (input.unregStudentId) {
      var inputData = input;
      findAndUpdate(inputData, function(error, output) {
        if (error) {
          throwError(JSON.stringify(error), cb);
        } else {
          cb(null, output);
        }
      });
    } else {
      throwError('cant update without id', cb);
    }
  } else {
    throwError('Invalid Input', cb);
  }
};
 /**
   *findAndUpdate-function deals with finding and updating the details
   *@constructor
   * @param {object} input - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function findAndUpdate(input, cb) {
  var inputFilterObject = {};
  inputFilterObject['unregStudentId'] = input.unregStudentId;
  findEntity(inputFilterObject, unregstudent, function(error, response) {
    if (error) {
      throwError(error, cb);
    } else if (response) {
      response.data.updateAttributes(input, function(err, info) {
        if (err) {
          throwError(err, cb);
        } else {
          logger.info('unregstudent record Updated Successfully');
          info['requestStatus'] = true;
          info['updateDatetime'] = new Date();
          cb(null, info);
        }
      });
    } else {
      throwError('Invalid unregstudentId', cb);
    }
  });
};
exports.updateUnregStudentDetails = updateUnregStudentDetails;
