'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var throwError = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var async = require('async');
var notificationEventId, input;
var notificationOutput = [];
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
module.exports = function(NOTIFICATION_MESSAGE_TEMPLATES) {
  NOTIFICATION_MESSAGE_TEMPLATES.pushNotification = function(input, callBc) {
    input = input;
    notificationOutput = [];
    var unsubscribeArray = [];
    var scheduleArray = [];
    if (input.notificationName == 9 || input.notificationName == 10) {
      unsubscribeArray = [9, 10];
      async.map(unsubscribeArray, getTemplate, function(tempErr, template) {
        if (template) {
          async.map(template, notificationEventCreate, function(eventError, templateResp) {
            callBc(null, templateResp);
          });
        } else {
          callBc(tempErr, null);
        }
      });
    } else if (input.notificationName == 7 || input.notificationName == 8) {
      scheduleArray = [7, 8];
      async.map(scheduleArray, getTemplate, function(tempErr, template) {
        if (template) {
          async.map(template, notificationEventCreate, function(eventError, templateResp) {
            callBc(null, templateResp);
          });
        } else {
          callBc(tempErr, null);
        }
      });
    } else {
      NOTIFICATION_MESSAGE_TEMPLATES.find({
        'where': {
          'notificationTemplateId': input.notificationName,
        },
      }, function(notifyErr, notificationResponse) {
        // console.log('notificationResponseeeeeeeeeeeeeeeeeeeeeeeeee ', notificationResponse);
        async.map(notificationResponse, notificationEventCreate, function(eventError, template) {
          callBc(null, template);
        });
        // function getTemplate(obj, cb) {
        //   NOTIFICATION_MESSAGE_TEMPLATES.findOne({
        //     'where': {
        //       'notificationTemplateId': obj,
        //     },
        //   }, function(notifyErr, notificationResponse) {
        //     cb(null, notificationResponse);
        //   });
        // }
      });
    }

    function notificationEventCreate(notification, callback) {
      createNotificationEvent(input, notification, function(eventErr, eventResponse) {
        notificationEventId = (eventResponse) ? eventResponse.notificationEventId : null;
        // console.log('..', notificationEventId);
        if (notification.broadcastFlag === 'GRP' && notification.receiverRoleCode === 'RECDIR') {
          createNotificationDetails(input, notification, eventResponse, function(err, detailsOut) {
            if (err) {
              throwError(err, callback);
            } else {
              var finalResponse = {};
              finalResponse.notificationTemplate = notification;
              finalResponse.notificationEvent = eventResponse;
              finalResponse.notificationDetails = detailsOut;
              callback(null, finalResponse);
            }
          });
        } else if (notification.broadcastFlag === 'IND' && notification.receiverRoleCode === 'STUDENT') {
          createStudentDetail(input, function(err, detailsOut) {
            if (err) {
              throwError(err, callback);
            } else {
              var finalResponse = {};
              finalResponse.notificationTemplate = notification;
              finalResponse.notificationEvent = eventResponse;
              finalResponse.notificationDetails = detailsOut;
              callback(null, finalResponse);
            }
          });
        } else if (notification.broadcastFlag === 'GRP' && notification.receiverRoleCode === 'PLCDIR') {
          createNotificationDetails(input, notification, eventResponse, function(err, detailsOut) {
            if (err) {
              throwError(err, callback);
            } else {
              var finalResponse = {};
              finalResponse.notificationTemplate = notification;
              finalResponse.notificationEvent = eventResponse;
              finalResponse.notificationDetails = detailsOut;
              callback(null, finalResponse);
            }
          });
        } else {
          throwError('Invalid Data', callBc);
        }
      });
    }
  };
  // remote method
  NOTIFICATION_MESSAGE_TEMPLATES.remoteMethod('pushNotification', {
    description: 'Company Will Publish Event ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: {
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    },
    http: {
      path: '/pushNotification',
      verb: 'POST',
    },
  });

  // function createNotificationEvent(input, notification, cb) {
  //   var notificationEvent = server.models.NotificationEvents;
  //   var getLookupId = require('../../commonValidation/lookupMethods').getLookupId;
  //   var roleTypeValueId, parentId;
  //   getLookupId('ROLE_TYPE_CODE', 'Employer', function(roleTypeId) {
  //     getLookupId('ROLE_TYPE_CODE', 'Campus', function(campusRoleTypeId) {
  //       getLookupId('ROLE_TYPE_CODE', 'Student', function(studentRoleTypeId) {
  //         if (input.role == 'PLCDIR' || input.role == 'RECDIR') {
  //           roleTypeValueId = (notification.receiverRoleCode == 'PLCDIR') ? roleTypeId.lookupValueId : campusRoleTypeId.lookupValueId;
  //           parentId = (notification.receiverRoleCode == 'PLCDIR') ? input.companyId : input.campusId;
  //         } else {
  //           roleTypeValueId = studentRoleTypeId.lookupValueId;
  //           parentId = input.campusId;
  //         }
  //         // console.log('............................ ', notification.receiverRoleCode, input.empEventId);
  //         var transaction = (input.campusEventId) ? input.campusEventId : input.empEventId;
  //         var eventObj = {
  //           'notificationTemplateId': notification.notificationTemplateId,
  //           'roleTypeValueId': roleTypeValueId,
  //           'createDatetime': new Date(),
  //           'updateDatetime': new Date(),
  //           'createUserId': input.userId,
  //           'updateUserId': input.userId,
  //           'parentId': parentId,
  //           'transactionId': transaction,
  //           'senderId': input.userId,
  //         };
  //         notificationEvent.create(eventObj, function(createdErr, eventResponse) {
  //           if (createdErr) {
  //             throwError(createdErr, cb);
  //           } else {
  //             cb(null, eventResponse);
  //           }
  //         });
  //       });
  //     });
  //   });
  // }
  //..............for campus person.........................
  function createNotificationEvent(input, notification, cb) {
    var notificationEvent = server.models.NotificationEvents;
    var getLookupId = require('../../commonValidation/lookupMethods').getLookupId;
    var roleTypeValueId, parentId, transaction;
    getLookupId('ROLE_TYPE_CODE', 'Employer', function(roleTypeId) {
      getLookupId('ROLE_TYPE_CODE', 'Campus', function(campusRoleTypeId) {
        getLookupId('ROLE_TYPE_CODE', 'Student', function(studentRoleTypeId) {
          if (notification.receiverRoleCode == 'PLCDIR') {
            roleTypeValueId = (input.role == 'STUDENT') ? 28 : roleTypeId.lookupValueId;
            parentId = (input.role == 'STUDENT') ? input.campusId : input.companyId;
            transaction = (input.role == 'STUDENT') ? input.campusEventId : input.empEventId;
            // console.log(input, transaction, input.role == 'STUDENT');
          } else if (notification.receiverRoleCode == 'RECDIR') {
            roleTypeValueId = (input.role == 'STUDENT') ? 28 : campusRoleTypeId.lookupValueId;
            parentId = input.campusId;
            transaction = (input.role == 'STUDENT') ? input.empEventId : input.campusEventId;
          } else if (input.role == 'PLCDIR') {
            roleTypeValueId = campusRoleTypeId.lookupValueId;
            parentId = input.campusId;
            transaction = input.empEventId;
          } else {
            roleTypeValueId = studentRoleTypeId.lookupValueId;
            parentId = input.campusId;
          }
          // console.log(',,,,,,,,,,,,,,,,,,,,,,,,,, ', roleTypeValueId, parentId, transaction);
          // if (input.role == 'PLCDIR' || input.role == 'RECDIR') {
          //   roleTypeValueId = (notification.receiverRoleCode == 'PLCDIR') ? roleTypeId.lookupValueId : campusRoleTypeId.lookupValueId;
          //   parentId = (notification.receiverRoleCode == 'PLCDIR') ? input.companyId : input.campusId;
          // } else {
          //   roleTypeValueId = studentRoleTypeId.lookupValueId;
          //   parentId = input.campusId;
          // }
          // console.log('............................ ', notification.receiverRoleCode, input.empEventId);
          var transaction = (input.campusEventId) ? input.campusEventId : input.empEventId;
          if (input.role == 'STUDENT') {
            if (notification.receiverRoleCode == 'PLCDIR') {
              transaction = input.campusEventId;
            } else if (notification.receiverRoleCode == 'RECDIR') {
              // transaction = input.empEventId;
              transaction = input.empEventId;
            }
          }
          // console.log('transactionnnnnnnnnnnnnnnnnnnnn ', transaction);
          var eventObj = {
            'notificationTemplateId': notification.notificationTemplateId,
            'roleTypeValueId': roleTypeValueId,
            'createDatetime': new Date(),
            'updateDatetime': new Date(),
            'createUserId': input.userId,
            'updateUserId': input.userId,
            'parentId': parentId,
            'transactionId': transaction,
            'senderId': input.userId,
          };
          // console.log('eeventttttttttt input:            ', eventObj);
          notificationEvent.create(eventObj, function(createdErr, eventResponse) {
            // console.log('notificationnnnnnnnnnnnnnnnnnnn ', createdErr, eventResponse);
            if (createdErr) {
              throwError(createdErr, cb);
            } else {
              cb(null, eventResponse);
            }
          });
          // });
        });
      });
    });
  }
  //.......................for cmpus person....................
  function createNotificationDetails(input, notification, eventResponse, callBack) {
    var listOfEntities = (notification.receiverRoleCode == 'PLCDIR') ? input.campusList : input.companyList;
    async.map(listOfEntities, createDetail, function(error, detailResponse) {
      if (error) {
        throwError(error, callBack);
      } else {
        callBack(null, detailResponse);
      }
    });
    // }

    function createDetail(obj, detailCB) {
      // console.log('000000000000000000000000000000 ', obj);
      var educationPerson = server.models.EducationPerson;
      var employerPerson = server.models.EmployerPerson;
      var modelsName = (obj.campusId) ? educationPerson : employerPerson;
      var inputObj = {};
      var campusObj = {};
      var companyObj = {};
      campusObj['campusId'] = obj.campusId;
      companyObj['companyId'] = obj.companyId;
      inputObj = (obj.campusId) ? campusObj : companyObj;
      // console.log('------------------------------------ ', inputObj);
      modelsName.find({
        'where': {
          'and': [inputObj],
        },
      }, function(eduErr, eduUsers) {
        // console.log('=========================== ', eduUsers);
        if (eduErr) {
          throwError(eduErr, detailCB);
        } else {
          async.map(eduUsers, createNotificationDetail, function(createDetailErr, detailsResponse) {
            if (createDetailErr) {
              throwError(createDetailErr, detailCB);
            } else {
              var out = [];
              out = cleanArray(detailsResponse);
              detailCB(null, out);
            }
          });
        }
      });
    }

    function createNotificationDetail(object, notificationCB) {
      // console.log('objecttttttttttttttttttttttttttttttttt ', eventResponse);
      var notificationDetails = server.models.NotificationDetails;
      var getLookupId = require('../../commonValidation/lookupMethods').getLookupId;
      getLookupId('ROLE_TYPE_CODE', 'Campus', function(roleTypeId) {
        getLookupId('ROLE_TYPE_CODE', 'Employer', function(employerRoleTypeId) {
          var roleTypeValueId = (object.campusId) ? roleTypeId.lookupValueId : employerRoleTypeId.lookupValueId;
          var parentId = (object.campusId) ? object.campusId : object.companyId;
          if (eventResponse.notificationEventId) {
            var detailsObj = {
              'notificationEventId': eventResponse.notificationEventId,
              'roleTypeValueId': roleTypeValueId,
              'parentId': parentId,
              'recipientId': object.id,
              'notificationRead': new Date(),
              'notificationDismissedInd': 'N',
              'createDatetime': new Date(),
              'notificationReadInd': 'N',
            };
            notificationDetails.create(detailsObj, function(detailsErr, details) {
              // console.log('detailssssssssssssssssssssssssssssssssssss ', detailsErr, details);
              if (detailsErr) {
                throwError(detailsErr, notificationCB);
              } else {
                notificationCB(null, details);
              }
            });
          } else {
            notificationCB(null, null);
          }
        });
      });
    }
  }

  function createStudentDetail(object, notificationCallBc) {
    var studentList = object.studentList;
    async.map(studentList, createStudentDetails, function(studentErr, studentResponse) {
      notificationCallBc(null, studentResponse);
    });

    function createStudentDetails(obj, studentCB) {
      var notificationDetails = server.models.NotificationDetails;
      var getLookupId = require('../../commonValidation/lookupMethods').getLookupId;
      getLookupId('ROLE_TYPE_CODE', 'Campus', function(roleTypeId1) {
        getLookupId('ROLE_TYPE_CODE', 'Employer', function(employerRoleTypeId) {
          getLookupId('ROLE_TYPE_CODE', 'Student', function(roleTypeId) {
            var roleTypeValueId;
            if (obj.role == 'PLCDIR') {
              roleTypeValueId = roleTypeId1.lookupValueId;
            } else if (obj.role == 'RECDIR') {
              roleTypeValueId = employerRoleTypeId.lookupValueId;
            } else {
              roleTypeValueId = roleTypeId.lookupValueId;
            }

            var parentId = object.campusId;
            var student = server.models.Student;
            student.findOne({
              'where': {
                'studentId': obj.studentId,
              },
            }, function(studentErr, studentInfo) {
              if (studentErr) {
                throwError(studentErr, studentCB);
              } else {
                var detailsObj = {
                  'notificationEventId': notificationEventId,
                  'roleTypeValueId': roleTypeValueId,
                  'parentId': parentId,
                  'recipientId': studentInfo.id,
                  'notificationRead': new Date(),
                  'notificationDismissedInd': 'N',
                  'createDatetime': new Date(),
                };
                notificationDetails.create(detailsObj, function(detailsErr, details) {
                  if (detailsErr) {
                    throwError(detailsErr, studentCB);
                  } else {
                    studentCB(null, details);
                  }
                });
              }
            });
          });
        });
      });
    }
  }

  function getTemplate(obj, cb) {
    NOTIFICATION_MESSAGE_TEMPLATES.findOne({
      'where': {
        'notificationTemplateId': obj,
      },
    }, function(notifyErr, notificationResponse) {
      cb(null, notificationResponse);
    });
  }
};
