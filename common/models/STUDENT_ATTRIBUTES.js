'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');

module.exports = function(STUDENT_ATTRIBUTES) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // when request comes to student-attribute model then we execute logic or change request object before saving into database
  STUDENT_ATTRIBUTES.observe('before save', function studentAttributeBeforeSave(ctx, next) {
    // console.log('ctx.isNewInstance: ' + ctx.isNewInstance);
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });

  // remote method definition to get student attribute details
    /**
 *getStudentAttribute- To get Student addribute details by taking required fields
 *@constructor
 * @param {object} studentId - Unique id for ecah and every student
 * @param {function} callBc - deals with response
 */
  STUDENT_ATTRIBUTES.getStudentAttribute = function(studentId, callBc) {
    var inputObj = {};
    var studentAttribute = server.models.StudentAttributes;

    var studentAttributetDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (studentId) {
      inputObj['studentId'] = studentId;
      var includeFilterArray = ['studentAttributesCatDetails', 'studentAttributesTypeDetails'];
      // below function will give student skill details for an entity based on loopback include filter
      studentAttributetDet(inputObj, studentAttribute, includeFilterArray, callBc);
    } else {
      errorResponse(callBc);
    }
  };

        // remote method declaration to get student skill details
  STUDENT_ATTRIBUTES.remoteMethod('getStudentAttribute', {
    description: 'To get student attribute details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
            ],
    http: {
      path: '/getStudentAttribute',
      verb: 'GET',
    },
  });

  // remote method definition to delete student attribute details
    /**
 *deleteStudentAttribute- To delete student addribute details by taking required fields
 *@constructor
 * @param {object} studentId - Unique id for ecah and every student
 * @param {number} studentAttributeId - unique id for each and every studentAttribute
 * @param {function} callBc - deals with response
 */
  STUDENT_ATTRIBUTES.deleteStudentAttribute = function(studentId, studentAttributeId, callBc) {
    if (studentId && studentAttributeId) {
      var studentAttributeModel = server.models.StudentAttributes;
      var deleteStudentAttribute = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteSingleEntityDetails;
      var inputFilterObject = {};
      inputFilterObject['studentId'] = studentId;
      inputFilterObject['studentAttributeId'] = studentAttributeId;
      deleteStudentAttribute(inputFilterObject, studentAttributeModel, callBc);
    } else {
      errorResponse(callBc);
    }
  };

  // remote method declaration to delete student attribute details
  STUDENT_ATTRIBUTES.remoteMethod('deleteStudentAttribute', {
    description: 'To delete student attribute',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'studentAttributeId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
        ],
    http: {
      path: '/deleteStudentAttribute',
      verb: 'DELETE',
    },
  });
};

