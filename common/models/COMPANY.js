'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var allmethods = require('../../update/allmethods.js');

// exporting function to use it in another modules if requires
module.exports = function(COMPANY) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var throwError = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  // when request comes to company model then we execute logic or change request object before saving into database
  COMPANY.observe('before save', function companyBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.searchName = (ctx.instance.name) ? ctx.instance.name.toUpperCase() : null;
      ctx.instance.searchShortName = (ctx.instance.shortName) ? ctx.instance.shortName.toUpperCase() : null;
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });
  // remote method definition to get company details based on company id
  /**
 *getCompany- To get company details based on the companyId
 *@constructor
 * @param {number} companyId - unique id of company
 * @param {function} callBc - deals with response
 */
  COMPANY.getCompany = function(companyId, callBc) {
    if (companyId) {
      var companyDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js')
        .entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = companyId;
      var company = server.models.Company;
      companyDetById(inputFilterObject, company, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get company details based on company id
  COMPANY.remoteMethod('getCompany', {
    description: 'To get company details based on company id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [
      {
        arg: 'companyId',
        type: 'number',
        required: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/getCompany',
      verb: 'get',
    },
  });
  var Joi = require('joi');
  var companyData;
  //update method starts from here
  /**
 *updateCompany- To update company details
 *@constructor
 * @param {number} companyData - contains data need to get updated
 * @param {function} cb - deals with response
 */
  COMPANY.updateCompany = function(companyData, cb) {
    var updateCompanyDet = require('../../commonCompanyFiles/update-company-profile');
    updateCompanyDet.updateCompanyProfile(companyData, function(err, resp) {
      logger.info('upating company profile ');
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //custom method for update company
  COMPANY.remoteMethod('updateCompany', {
    description: 'Send Valid companyId ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [
      {
        arg: 'data',
        type: 'object',
        http: {
          source: 'body',
        },
      },
    ],
    http: {
      path: '/updateCompany',
      verb: 'PUT',
    },
  });
  COMPANY.getEventStudentList = function(employerEventId, campusEventId, cb) {
    var getDetails = require('../../commonCompanyFiles/export-details');
    if (employerEventId) {
      getDetails.exportData(employerEventId, function(err, resp) {
        logger.info('upating company profile ');
        if (err) {
          cb(err, resp);
        } else {
          cb(null, resp);
        }
      });
    } else if (campusEventId) {
      getDetails.exportDetails(campusEventId, function(err, resp) {
        logger.info('upating company profile ');
        if (err) {
          cb(err, resp);
        } else {
          cb(null, resp);
        }
      });
    } else {
      errorResponse('employerEventId or campusEventId should be provided', cb);
    }
  };
  COMPANY.remoteMethod('getEventStudentList', {
    description: 'To get company address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [
      {arg: 'employerEventId', type: 'number', http: {source: 'query'}},
      {arg: 'campusEventId', type: 'number', http: {source: 'query'}},
    ],
    http: {
      path: '/getEventStudentList',
      verb: 'get',
    },
  });
  COMPANY.companyProfile = function(companyId, callBc) {
    if (companyId) {
      var companyDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js')
        .entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = companyId;
      var company = server.models.Company;
      companyDetById(inputFilterObject, company, function(err, res) {
        if (err) {
          throwError('invalid id', callBc);
        } else {
          var getProfile = require('../../commonCompanyFiles/company-profile').getProfile;
          getProfile(res, function(err, res) {
            if (err) {
              throwError('an error in line 160', callBc);
            } else {
              callBc(null, res);
            }
          });
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  COMPANY.remoteMethod('companyProfile', {
    description: 'To get company profile based on company id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [
      {
        arg: 'companyId',
        type: 'number',
        required: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/companyProfile',
      verb: 'get',
    },
  });
  COMPANY.Drives = function(companyId, callBc) {
    if (companyId) {
      var companyDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js')
        .entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = companyId;
      var company = server.models.Company;
      companyDetById(inputFilterObject, company, function(err, res) {
        if (err) {
          throwError('invalid id', callBc);
        } else {
          var getDrives = require('../../commonCompanyFiles/company-profile').getDrivesInProcess;
          getDrives(res.data[0], function(err, res) {
            if (err) {
              throwError('an error in line 160', callBc);
            } else {
              callBc(null, res);
            }
          });
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  COMPANY.remoteMethod('Drives', {
    description: 'To get company profile based on company id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [
      {
        arg: 'companyId',
        type: 'number',
        required: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/Drives',
      verb: 'get',
    },
  });
  var inprogress = 0;
  var totalEventDetails = [];
  COMPANY.totalDriveDetails = function(companyId, callBc) {
    if (companyId) {
      var drives = server.models.EmployerDrive;
      drives.find({'where': {'companyId': companyId, 'driveStatusValueId': 306}},
      function(err, inProgressDrives) {
        if (err) {
          callBc(err, null);
        } else {
          var async = require('async');
          async.map(inProgressDrives, getCountOfEvents,
            function(err, allEvents) {
              if (err) {
                callBc(err, null);
              } else {
                callBc(null, totalEventDetails);
                totalEventDetails = [];
              }
            });
        }
        function getCountOfEvents(obj, callback) {
          var employerEvents = server.models.EmployerEvent;
          employerEvents.find({'where': {
            'empDriveId': obj.empDriveId,
            'companyId': companyId,
          }}, function(err, totalEvents) {
            if (err) {
              callback(err, null);
            } else {
              var async = require('async');
              async.map(totalEvents, getInProgressEvents,
                function(err, inProgresCounts) {
                  if (err) {
                    callback(err, null);
                  } else {
                    var events = {};
                    events.DriveName = obj.driveName;
                    events.totalEvents = totalEvents.length;
                    events.inProgressEvents = inprogress;
                    totalEventDetails.push(events);
                    callback(null, inProgresCounts);
                    inprogress = 0;
                  }
                });
            }
          });
        }
        function getInProgressEvents(object, callBack) {
          if (object.eventStatusValueId == 320) {
            inprogress++;
            callBack(null, null);
          } else {
            callBack(null, null);
          }
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  COMPANY.remoteMethod('totalDriveDetails', {
    description: 'To get company profile based on company id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [
      {
        arg: 'companyId',
        type: 'number',
        required: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/totalDriveDetails',
      verb: 'get',
    },
  });
  COMPANY.getJobDetailsOfCompany = function(companyId, callBc) {
    if (companyId) {
      var companyDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js')
        .entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = companyId;
      var company = server.models.Company;
      companyDetById(inputFilterObject, company, function(err, res) {
        if (err) {
          throwError('invalid id', callBc);
        } else {
          var getJobDetails = require('../../commonCompanyFiles/company-profile').getJobDetails;
          getJobDetails(res.data[0].companyId, function(err, res) {
            if (err) {
              throwError('an error in line 160', callBc);
            } else {
              callBc(null, res);
            }
          });
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  COMPANY.remoteMethod('getJobDetailsOfCompany', {
    description: 'To get company profile based on company id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [
      {
        arg: 'companyId',
        type: 'number',
        required: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/getJobDetailsOfCompany',
      verb: 'get',
    },
  });
  COMPANY.getSheduledEvents = function(companyId, cb) {
    var events = require('../../commonCompanyFiles/get-Events');
    events.getSheduledEvents(companyId, function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        cb(null, response);
      }
    });
  };
  COMPANY.remoteMethod('getSheduledEvents', {
    description: 'To get company address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [
      {arg: 'companyId', type: 'number', http: {source: 'query'}},
    ],
    http: {
      path: '/getSheduledEvents',
      verb: 'get',
    },
  });
  var scheduled = 0;
  var inProgress = 0;
  var closed = 0;
  var driveData = [];
  COMPANY.driveDetails = function(companyId, cb) {
    var endDate = new Date();
    var startDate = new Date();
    var currentYear = new Date().getFullYear();
    var month = new Date().getMonth();
    console.log(currentYear + 1);
    if (month > 6) {
      startDate.setFullYear((currentYear), 5, 1);
      endDate.setFullYear((currentYear + 1), 4, 31);
    } else if (month <= 6) {
      startDate.setFullYear((currentYear - 1), 5, 1);
      endDate.setFullYear((currentYear), 4, 31);
    }
    var empDrives = server.models.EmployerDrive;
    empDrives.find({'where': {'companyId': companyId, 'driveStatusValueId': {
      'neq': 308,
    }, 'createDatetime': {
      between: [startDate, endDate],
    }}}, function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        var async = require('async');
        async.map(response, getAllDrivesAndStatusCounts,
          function(err, drivesResponse) {
            if (err) {
              cb(err, null);
            } else {
              var drive = {};
              drive.DriveInfo = driveData;
              drive.counts = {
                'inProgress': inProgress,
                'scheduled': scheduled,
                'closed': closed,
                'ActiveDrives': driveData.length,
              };
              cb(null, drive);
            }
          });
      }
    });
  };
  var driveCountClosed = 0;
  var driveCountInProgress = 0;
  var driveCountSheduled = 0;
  function getAllDrivesAndStatusCounts(object, callBack) {
    var empEvent = server.models.EmployerEvent;
    empEvent.find({'where': {'empDriveId': object.empDriveId}},
    function(err, eventDetails) {
      if (err) {
        callBack(err, null);
      } else {
        var async = require('async');
        async.map(eventDetails, getEventDetails, function(err, countResponse) {
          if (err) {
            callBack(err, null);
          } else {
            var driveDetails = {};
            driveDetails['Drive Name'] = object.driveName;
            driveDetails['No. of Events'] = eventDetails.length;
            driveDetails['Events Scheduled'] = driveCountSheduled;
            driveDetails['Events in Progress'] = driveCountInProgress;
            driveDetails.closed = driveCountClosed;
            driveData.push(driveDetails);
            callBack(null, countResponse);
            driveCountClosed = 0;
            driveCountSheduled = 0;
            driveCountInProgress = 0;
          }
        });
      }
    });
  }
  function getEventDetails(obj, cb) {
    if (obj.eventStatusValueId === 319) {
      scheduled++;
      driveCountSheduled++;
      cb(null, 'done');
    } else if (obj.eventStatusValueId === 320) {
      inProgress++;
      driveCountInProgress++;
      cb(null, 'done');
    } else if (obj.eventStatusValueId === 321) {
      closed++;
      driveCountClosed++;
      cb(null, 'done');
    } else {
      cb(null, 'done');
    }
  }
  COMPANY.remoteMethod('driveDetails', {
    description: 'To get company address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [
      {arg: 'companyId', type: 'number', http: {source: 'query'}},
    ],
    http: {
      path: '/driveDetails',
      verb: 'get',
    },
  });
  var empDriveId = 0;
  var selectedDetails = [];
  COMPANY.getRecentRecruitment = function(companyId, cb) {
    eventDetais(companyId, function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        cb(null, response.reverse());
      }
    });
  };
  function eventDetais(companyId, cb) {
    var endDate = new Date();
    var currentYear = new Date().getFullYear();
    var startDate = new Date();
    var month = new Date().getMonth();
    startDate.setFullYear(currentYear, (month - 1), 1);
    var eventList = server.models.EventStudentList;
    eventList.find({'where': {'companyId': companyId, 'candidateStatusValueId': 381, 'updateDatetime': {
      between: [startDate, endDate],
    }, order: 'updateDatetime DESC'}}, function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        var async = require('async');
        async.map(response, getNameAndDetails,
          function(err, resp) {
            if (err) {
              cb(err, null);
            } else {
              cb(null, resp);
            }
          });
      }
    });
    function getNameAndDetails(object, callBack) {
      var enroll = server.models.Enrollment;
      enroll.findOne({'where': {'studentId': object.studentId}},
      function(err, enrollRes) {
        if (err) {
          callBack(err, null);
        } else {
          var student = server.models.Student;
          student.findOne({'where': {'studentId': enrollRes.studentId}},
          function(err, studentRes) {
            if (err) {
              callBack(err, null);
            } else {
              var program = server.models.Program;
              program.findOne({'where': {'programId': enrollRes.programId}},
              function(err, programRes) {
                if (err) {
                  callBack(err, null);
                } else {
                  var campus = server.models.Campus;
                  campus.findOne({'where': {'campusId': programRes.campusId}},
                  function(er, res) {
                    if (er) {
                      callBack(err, null);
                    } else {
                      var employerEvent = server.models.EmployerEvent;
                      employerEvent.findOne({'where': {'empEventId': object.employerEventId}},
                      function(err, eventRes) {
                        if (err) {
                          callBack(err, null);
                        } else {
                          var empDrive = server.models.EmployerDrive;
                          empDrive.findOne({'where': {'empDriveId': eventRes.empDriveId},
                          'include': 'employerDriveIbfk3rel'}, function(err, driveRes) {
                            if (err) {
                              callBack(err, null);
                            } else {
                              var details = {};
                              var jobDetails = driveRes.toJSON();
                              details.StudentName = studentRes.firstName + studentRes.lastName;
                              details['Campus Name'] = res.name;
                              details['Program Name'] = programRes.programName;
                              // details.updateDatetime = object.updateDatetime;
                              details['Job Role'] = jobDetails.employerDriveIbfk3rel.jobRoleName;
                              selectedDetails.push(details);
                              callBack(null, details);
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  }
  COMPANY.remoteMethod('getRecentRecruitment', {
    description: 'To get company address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [
      {arg: 'companyId', type: 'number', http: {source: 'query'}},
    ],
    http: {
      path: '/getRecentRecruitment',
      verb: 'get',
    },
  });
  var campusDetails = [];
  var orgDetails = [];
  var details = [];
  var uniqueCampusIds = [];
  var campusSelectedCount = [];
  var selected = 0;
  var campusNameAndSelected = [];
  COMPANY.getCampusHiringData = function(companyId, organizationId, cb) {
    var endDate = new Date();
    var startDate = new Date();
    var currentYear = new Date().getFullYear();
    var month = new Date().getMonth();
    // console.log(currentYear + 1);
    if (month > 6) {
      startDate.setFullYear((currentYear), 5, 1);
      endDate.setFullYear((currentYear + 1), 4, 31);
    } else if (month <= 6) {
      startDate.setFullYear((currentYear - 1), 5, 1);
      endDate.setFullYear((currentYear), 4, 31);
    }
    if (companyId && organizationId) {
      var empDrive = server.models.EmployerDrive;
      empDrive.find({'where': {'companyId': companyId, 'organizationId': organizationId,
        'createDateTime':
        {
          between: [startDate, endDate],
        },
    }},
      function(err, res) {
        if (err) {
          cb(err, null);
        } else {
          var async = require('async');
          async.map(res, getOrgBasedEventDetails, function(err, orgResp) {
            if (err) {
              cb(err, null);
            } else {
              var async = require('async');
              async.map(uniqueCampusIds, getCampusSelectedCount, function(err, res) {
                if (err) {
                  cb(err, null);
                } else {
                  async.map(campusSelectedCount, getCampusNameAndDetails,
                    function(err, campNameRes) {
                      if (err) {
                        cb(err, null);
                      } else {
                        cb(null, campusNameAndSelected);
                        selected = 0;
                        uniqueCampusIds = [];
                        details = [];
                        orgDetails = [];
                        campusDetails = [];
                        campusSelectedCount = [];
                        campusNameAndSelected = [];
                      }
                    });
                }
              });
            }
          });
        }
      });
    } else {
      var statment = 'select count(student_id) totalOffers, campus_id campusId from EVENT_STUDENT_LIST where candidate_status_value_id = 381 and company_id =' + companyId + ' and update_datetime BETWEEN' +
      '' + JSON.stringify(startDate) + ' and ' + JSON.stringify(endDate) + ' group by campus_id';
      MySql(statment, function(err, response) {
        if (err) {
          cb(err, null);
        } else {
          var async = require('async');
          async.map(response, getCampusName, function(err, resp) {
            if (err) {
              cb(err, null);
            } else {
              cb(null, campusDetails);
              campusDetails = [];
            }
          });
        }
      });
    }
  };
  function getCampusNameAndDetails(obj, cb) {
    var campus = server.models.Campus;
    campus.findOne({'where': {'campusId': obj.campusId}, 'fields': {
      'name': true,
    }}, function(err, campName) {
      if (err) {
        cb(err, null);
      } else {
        var data = {};
        data.CampusName = campName.name;
        data.CampusId = obj.campusId;
        data.selectedPeople = obj.selected;
        campusNameAndSelected.push(data);
        cb(null, 'done');
      }
    });
  }
  // function getCampusNameAndSelectedCount(array, cb) {
  //   for (var i = 0; i < array.length; i++) {
  //     for (var j = 0; j < details.length; j++) {
  //       if (array[i] == details[j].campusId) {

  //       }
  //     }
  //   }
  // }
  function getCampusSelectedCount(campusId, cb) {
    selected = 0;
    for (var i = 0; i < details.length; i++) {
      if (details[i].campusId == campusId) {
        selected++;
      } if (i == details.length - 1) {
        var data = {};
        data.campusId = campusId;
        data.selected = selected;
        campusSelectedCount.push(data);
        cb(null, 'done');
        selected = 0;
      }
    }
  }
  // function getUniqueCampusDetails(array, cb) {
  //   for (var i = 0; i < array.length; i++) {
  //     uniqueCampus.push(details[i]);
  //     checkForRepeat(array[i].campusId, function(er, status) {
  //       if (status == true) {
  //         for (var j = 0; j < details.length; j++) {
  //           if (array[i].campusId == details[j].campusId) {
  //             uniqueCampus.push(details[j]);
  //             // console.log(uniqueCampus);
  //           }
  //         }
  //       }
  //     });
  //   }
  // }
  // function checkForRepeat(campusId, cb) {
  //   console.log(uniqueCampus.include(campusId));
  //   if (uniqueCampus.indexOf(campusId) == -1) {
  //     console.log('false');
  //     cb(null, false);
  //   } else if (uniqueCampus.indexOf(campusId) > -1) {
  //     console.log('true');
  //     cb(null, true);
  //   }
  // }
  function getOrgBasedEventDetails(obje, cb) {
    var empEvent = server.models.EmployerEvent;
    empEvent.find({'where': {'empDriveId': obje.empDriveId}},
    function(err, resp) {
      if (err) {
        cb(err, null);
      } else if (resp == null) {
        cb(null, 'done');
      } else {
        var async = require('async');
        // console.log('rr', resp);
        async.map(resp, getEventName, function(err, res) {
          if (err) {
            cb(err, null);
          } else {
            cb(null, res);
          }
        });
      }
    });
  }
  function getEventName(obj, cb) {
    var eventStudent = server.models.EventStudentList;
    // console.log(obj);
    eventStudent.find({'where': {'employerEventId': obj.empEventId}},
    function(er, response) {
      if (er) {
        cb(er, null);
      } else {
        var async = require('async');
        async.map(response, getCountsOfStudents,
          function(err, resp) {
            if (err) {
              cb(err, null);
            } else {
              cb(null, 'done');
            }
          });
      }
    });
  }
  function getCountsOfStudents(obj, cb) {
    if (obj.candidateStatusValueId == 381) {
      details.push(obj);
      if (uniqueCampusIds.includes(parseInt(obj.campusId))) {
        cb(null, 'done');
      } else {
        uniqueCampusIds.push(parseInt(obj.campusId));
        cb(null, 'done');
      }
    } else {
      cb(null, 'done');
    }
  }
  function getCampusName(obj, callBc) {
    var campus = server.models.Campus;
    campus.findOne({'where': {'campusId': obj.campusId},
    'fields': {
      'name': true,
    }}, function(err, resp) {
      if (err) {
        callBc(err, null);
      } else {
        var campDetails = {};
        campDetails.campusName = (resp) ? resp.name : null;
        campDetails.campusId = obj.campusId;
        campDetails.totalOffers = obj.totalOffers;
        campusDetails.push(campDetails);
        callBc(null, campDetails);
      }
    });
  }
  COMPANY.remoteMethod('getCampusHiringData', {
    description: 'To get company address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [
      {
        arg: 'companyId',
        type: 'number',
        http: {source:
          'query',
        },
      },
      {
        arg: 'organizationId',
        type: 'number',
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/getCampusHiringData',
      verb: 'get',
    },
  });
  function MySql(statment, sqlCb) {
    var datasource = require('../../server/datasources.json');
    var mysql = require('mysql');
    var con = mysql.createConnection({
      host: datasource.ScoraXChangeDB.host,
      user: datasource.ScoraXChangeDB.user,
      password: datasource.ScoraXChangeDB.password,
      database: datasource.ScoraXChangeDB.database,
    });
    con.query(statment, function(err, result, fields) {
      if (err) {
        sqlCb(err, null);
      } else {
        sqlCb(null, result);
      }
    });
  }
  COMPANY.upCommingEvents = function(companyId, cb) {
    var employerEvents = server.models.EmployerEvent;
    employerEvents.find({'where': {'companyId': companyId, 'scheduledDate': {
      gte: new Date(),
    }}},
    function(err, resp) {
      if (err) {
        cb(err, null);
      } else {
        var async = require('async');
        async.map(resp, getThisEventCount,
          function(err, response) {
            if (err) {
              cb(err, null);
            } else {
              cb(null, response);
            }
          });
      }
    });
  };
  var shortlisted = 0;
  function getThisEventCount(obj, cb) {
    var eventList = server.models.EventStudentList;
    eventList.find({'where': {'employerEventId': obj.empEventId}},
    function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        var async = require('async');
        async.map(response, getStudentShortListedCount,
          function(err, resp) {
            if (err) {
              cb(err, null);
            } else {
              var data = {};
              var date = new Date(obj.scheduledDate).getDay() + '/' +
              new Date(obj.scheduledDate).getMonth() + '/' +
              new Date(obj.scheduledDate).getFullYear();
              data['Event Name'] = obj.eventName;
              data['Students Shortlisted'] = shortlisted;
              data.Date = date;
              data['Event Type'] = obj.eventTypeValueId;
              cb(null, data);
              shortlisted = 0;
            }
          });
      }
    });
  }
  function getStudentShortListedCount(obj, cb) {
    if (obj.candidateStatusValueId == 377) {
      shortlisted++;
      cb(null, 'done');
    } else {
      cb(null, 'done');
    }
  }
  COMPANY.remoteMethod('upCommingEvents', {
    description: 'To get company address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [
      {arg: 'companyId', type: 'number', http: {source: 'query'}},
    ],
    http: {
      path: '/upCommingEvents',
      verb: 'get',
    },
  });
};
