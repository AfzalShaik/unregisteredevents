'use strict';

var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(CAMPUS_EVENT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  /**
   * campusEventBeforeSave-used for creation
   * @constructor
   * @param {object} ctx -details entered by the user
   * @param {function} next-used for skiping when we want to skip any
   */
  CAMPUS_EVENT.observe('before save', function campusEventBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });

  function chekingRes(input, cb) {
    console.log('inside the input............................ ', input);
    var campusEvent = server.models.CampusEvent;
    campusEvent.create(input, function(createErr, createResp) {
      console.log('responseeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ', createErr, createResp);
      //   var educationPersonId = server.models.EducationPerson;
      //   educationPersonId.findOne({'where': {'id': createResp.createUserId}}, function(eduErr, eduResp) {
      //     var actionInput = {
      //       'employerEventId': createResp.employerEventId,
      //       'userId': createResp.createUserId,
      //       'role': 'PLCDIR',
      //       'educationPersonId': eduResp.educationPersonId,
      //       'campusId': createResp.campusId,
      //       'Action': 'Accepted',
      //     };
      //     campusBussinessLogic(actionInput, function(err, resp) {
      //       var finalResp = {};
      //       finalResp.campusEvent = createResp;
      //       finalResp.acceptNotification = resp;
      //       cb(null, finalResp);
      //     });
      //   });
    });
  };
  var responseFunction = function(response, next) {
    next(null, response);
  };
  //.........................Campus Event Post....................................
  CAMPUS_EVENT.createCampusEvent = function(input, cb) {
    input.updateUserId = input.createUserId;
    input.createDatetime = new Date();
    input.updateDatetime = new Date();
    if (input.employerEventId) {
      var campusEvent = server.models.CampusEvent;
      campusEvent.create(input, function(createErr, createResp) {
        if (createErr) {
          cb(createErr, null);
        } else {
          var educationPersonId = server.models.EducationPerson;
          educationPersonId.findOne({
            'where': {
              'id': createResp.createUserId,
            },
          }, function(eduErr, eduResp) {
            var actionInput = {
              'employerEventId': createResp.employerEventId,
              'userId': createResp.createUserId,
              'role': 'PLCDIR',
              'educationPersonId': eduResp.educationPersonId,
              'campusId': createResp.campusId,
              'Action': 'Accepted',
            };
            // console.log('actionInputtttttttttttttttttttt ', actionInput);
            CAMPUS_EVENT.campusBussinessLogic(actionInput, function(err, resp) {
              // console.log('=================================== ', err, resp);
              var finalResp = {};
              finalResp.campusEvent = createResp;
              cb(null, createResp);
            });
          });
        }
      });
    } else {
      var campusEvent = server.models.CampusEvent;
      campusEvent.create(input, function(createErr, createResp) {
        if (createErr) {
          cb(createErr, null);
        } else {
          cb(null, createResp);
        }
      });
    }
  };
  CAMPUS_EVENT.remoteMethod('createCampusEvent', {
    description: 'Create Campus Event',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createCampusEvent',
      verb: 'post',
    },
  });
  /**
   * getCampusEventDet-used for getting details based on id
   *@constructor
   * @param {number} campusEventId-primary key for searching in the database
   * @param {function} callBc-callback function deals with response
   */
  CAMPUS_EVENT.getCampusEventDet = function(campusEventId, callBc) {
    var inputObj = {};
    var campusEventModel = server.models.CampusEvent;
    var findEntity = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    if (campusEventId) {
      inputObj['campusEventId'] = campusEventId;
      findEntity(inputObj, campusEventModel, function(err, res) {
        if (err) {
          errorResponse(err, callBc);
        } else {
          callBc(null, res);
        }
      });
    } else {
      errorResponse('results not found', callBc);
    }
  };
  /**
   * Remote method for the getCampusEventDet
   */
  CAMPUS_EVENT.remoteMethod('getCampusEventDet', {
    description: 'To get campus event details by id details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: {
      arg: 'campusEventId ',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    http: {
      path: '/getCampusEventDet',
      verb: 'get',
    },
  });
  /**
   *updateCampusEvent -function deals with updating
   *@constructor
   * @param {object} campusEventData-contains all the data which can get updated
   * @param {function} callBc-callback function
   */
  CAMPUS_EVENT.updateCampusEvent = function(campusEventData, callBc) {
    var campusEventUpdate = require('../../update/campusevent-update.js');
    campusEventUpdate.campusEvent(campusEventData, function(err, response) {
      if (err) {
        callBc(err, response);
      } else if (response == 0) {
        callBc(err, response);
      } else {
        callBc(null, response);
      }
    });
  };
  /**
   *Remote method for updateCampusEvent
   */
  CAMPUS_EVENT.remoteMethod('updateCampusEvent', {
    description: 'update campus event details',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCampusEvent',
      verb: 'PUT',
    },
  });
  /**
   *CampusBussinessLogic -function deals with Bussiness Logics in Campus Event
   *@constructor
   * @param {object} campusEventData-contains all the data which required
   * @param {function} cb-callback function
   */
  CAMPUS_EVENT.campusBussinessLogic = function(campusEventData, cb) {
    var campusDriveManagment = require('../../ServicesImpl/CampusImpl/campusEventManagment.js');
    campusDriveManagment.campusBussinessEvent(campusEventData, function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        response.requestStatus = true;
        cb(null, response);
      }
    });
  };
  //accpect method to update campus drive
  CAMPUS_EVENT.remoteMethod('campusBussinessLogic', {
    description: 'Event details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/campusBussinessLogic',
      verb: 'PUT',
    },
  });
  var nonClosedEvents = [];
  CAMPUS_EVENT.getAllEventsOfMyCampus = function(campusId, cb) {
    var campusDrive = server.models.CampusDrive;
    CAMPUS_EVENT.find({
      'where': {
        'campusId': campusId,
      },
      'fields': {
        'campusEventId': true,
        'eventName': true,
        'eventStatusValueId': true,
      },
    }, function(err, campusEvents) {
      if (err) {
        cb(err, null);
      } else if (campusEvents == []) {
        cb('No Events For that Campus', cb);
      } else {
        var async = require('async');
        async.map(campusEvents, getNonClosedEvents, function(err, events) {
          if (err) {
            cb(err, null);
          } else {
            cb(null, nonClosedEvents);
            nonClosedEvents = [];
          }
        });
      }
    });

    function getNonClosedEvents(object, eventsCallback) {
      if (object.eventStatusValueId != 244) {
        var eventsStudentList = server.models.EventStudentList;
        eventsStudentList.find({
            'where': {
              'campusEventId': object.campusEventId,
            },
          },
          function(err, eventStudentListDetails) {
            if (err) {
              eventsCallback(err, null);
            } else if (eventStudentListDetails == []) {
              eventsCallback(null, eventStudentListDetails);
            } else {
              nonClosedEvents.push(object);
              eventsCallback(null, eventStudentListDetails);
            }
          });
      }
    }
  };
  //accpect method to update campus drive
  CAMPUS_EVENT.remoteMethod('getAllEventsOfMyCampus', {
    description: 'Event details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      require: true,
    }],
    http: {
      path: '/getAllEventsOfMyCampus',
      verb: 'GET',
    },
  });

  // accepts campusId as input and returns all the campus events where empEventId is null
  CAMPUS_EVENT.getCampusUnregisteredEvents = function(campusId, callBc) {
    CAMPUS_EVENT.find({
      'where': {
        'and': [{
          'campusId': campusId,
        }, {
          'employerEventId': null,
        }],
      },
    }, function(campusEventErr, campusOut) {
      if (campusEventErr) {
        callBc(null, []);
      } else {
        callBc(null, campusOut);
      }
    });
  }
  CAMPUS_EVENT.remoteMethod('getCampusUnregisteredEvents', {
    description: 'Event details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      require: true,
    }],
    http: {
      path: '/getCampusUnregisteredEvents',
      verb: 'GET',
    },
  });
};
// 'use strict';

// var server = require('../../server/server');
// var logger = require('../../server/boot/lib/logger');
// module.exports = function(CAMPUS_EVENT) {
//   var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
// /**
//  * campusEventBeforeSave-used for creation
//  * @constructor
//  * @param {object} ctx -details entered by the user
//  * @param {function} next-used for skiping when we want to skip any
//  */
//   CAMPUS_EVENT.observe('before save', function campusEventBeforeSave(ctx, next) {
//     if (ctx.isNewInstance) {
//       ctx.instance.updateUserId = ctx.instance.createUserId;
//       ctx.instance.createDatetime = new Date();
//       ctx.instance.updateDatetime = new Date();
//       next();
//     } else {
//       next();
//     }
//   });
// /**
//  * getCampusEventDet-used for getting details based on id
//  *@constructor
//  * @param {number} campusEventId-primary key for searching in the database
//  * @param {function} callBc-callback function deals with response
//  */
//   CAMPUS_EVENT.getCampusEventDet = function(campusEventId, callBc) {
//     var inputObj = {};
//     var campusEventModel = server.models.CampusEvent;
//     var findEntity = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
//     if (campusEventId) {
//       inputObj['campusEventId'] = campusEventId;
//       findEntity(inputObj, campusEventModel, function(err, res) {
//         if (err) {
//           errorResponse(err, callBc);
//         } else {
//           callBc(null, res);
//         }
//       });
//     } else {
//       errorResponse('results not found', callBc);
//     }
//   };
// /**
//  * Remote method for the getCampusEventDet
//  */
//   CAMPUS_EVENT.remoteMethod('getCampusEventDet', {
//     description: 'To get campus event details by id details',
//     returns: {
//       type: 'array',
//       root: true,
//     },
//     accepts: {arg: 'campusEventId ',
//              type: 'number',
//              required: true,
//              http: {source: 'query'}},
//     http: {
//       path: '/getCampusEventDet',
//       verb: 'get',
//     },
//   });
// /**
//  *updateCampusEvent -function deals with updating
//  *@constructor
//  * @param {object} campusEventData-contains all the data which can get updated
//  * @param {function} callBc-callback function
//  */
//   CAMPUS_EVENT.updateCampusEvent = function(campusEventData, callBc) {
//     var campusEventUpdate = require('../../update/campusevent-update.js');
//     campusEventUpdate.campusEvent(campusEventData, function(err, response) {
//       if (err) {
//         callBc(err, response);
//       } else if (response == 0) {
//         callBc(err, response);
//       } else {
//         callBc(null, response);
//       }
//     });
//   };
// /**
//  *Remote method for updateCampusEvent
//  */
//   CAMPUS_EVENT.remoteMethod('updateCampusEvent', {
//     description: 'update campus event details',
//     returns: {
//       arg: 'data',
//       type: 'object',
//     },
//     accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
//     http: {
//       path: '/updateCampusEvent',
//       verb: 'PUT',
//     },
//   });
// /**
//  *CampusBussinessLogic -function deals with Bussiness Logics in Campus Event
//  *@constructor
//  * @param {object} campusEventData-contains all the data which required
//  * @param {function} cb-callback function
//  */
//   CAMPUS_EVENT.CampusBussinessLogic = function(campusEventData, cb) {
//     var campusDriveManagment = require('../../ServicesImpl/CampusImpl/campusEventManagment.js');
//     campusDriveManagment.campusBussinessEvent(campusEventData, function(err, response) {
//       if (err) {
//         cb(err, null);
//       }
//       else {
//         response.requestStatus = true;
//         cb(null, response);
//       }
//     });
//   };
//   //accpect method to update campus drive
//   CAMPUS_EVENT.remoteMethod('CampusBussinessLogic', {
//     description: 'Event details ',
//     returns: {
//       arg: 'data',
//       type: 'object',
//     },
//     accepts: {
//       arg: 'data',
//       type: 'object',
//       http: {
//         source: 'body',
//       },
//     },
//     http: {
//       path: '/CampusBussinessLogic',
//       verb: 'PUT',
//     },
//   });
// };
