'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');

module.exports = function(STUDENT_CONTACT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var throwerror = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;

  // remote method definition to create address for particular student
   /**
 *createStudentContact- To Create Student Conatct Details
 *@constructor
 * @param {object} data - contains all the data need to get created
 * @param {function} callBc - deals with response
 */
  STUDENT_CONTACT.createStudentContact = function(data, callBc) {
    if (data.studentId && data.contacts.length > 0) {
      var myArray = data.contacts;
      var iterateobj = {};
      var count = 0;
      for (var j = 0; j < myArray.length; j++) {
        iterateobj = myArray[j];
        if (myArray[j].primaryInd == 'Y') {
          iterateobj = myArray[j];
          count++;
        }
      }
      // console.log('this is count', count);
      if (count <= 1) {
        if (count == 1) {
          searchForInd(data, iterateobj, function(err, res) {
            if (err) {
              errorResponse('there was an error', callBc);
            } else {
              // console.log('sucess');
            }
          });
        }
        var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
        var inputFilterObject = {};
        inputFilterObject['studentId'] = data.studentId;
        var contactModel = server.models.Contact;
        var studentModel = server.models.Student;

        checkEntityExistence(studentModel, inputFilterObject, function(checkStatus) {
          if (checkStatus) {
            contactModel.create(data.contacts, function(error, contactRes) {
              if (error) {
                callBc(error, null);
              } else if (contactRes) {
                var persistContactDataInForeignEntity = require('../../ServicesImpl/ContactImpl/persistContactRelationData.js').persistContactDataInForeignEntity;
                var studentContactModel = server.models.StudentContact;
                var inputObj = {};
                inputObj['studentId'] = Number(data.studentId);
                persistContactDataInForeignEntity(contactRes.length, contactRes, studentContactModel, inputObj, callBc);
              } else {
                errorResponse(callBc);
              }
            });
          } else {
            errorResponse(callBc);
          }
        });
      } else {
        throwerror('cant have two contacts with primary indicator Y', callBc);
      }
    } else {
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    STUDENT_CONTACT.find({'where': {'studentId': data.studentId, 'primaryInd': 'Y'}},
    function(err, res) {
      if (err) {
        errorResponse('campusId was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
        priCallback(null, res);
      } else {
        var update = {};
        update.primaryInd = 'N';
        STUDENT_CONTACT.updateAll({'studentId': data.studentId, 'primaryInd': 'Y'},
        update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            // console.log('this is response of update', resp);
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create contact for particular student
  STUDENT_CONTACT.remoteMethod('createStudentContact', {
    description: 'Useful to create contact for particular student',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createStudentContact',
      verb: 'POST',
    },
  });

  // remote method definition to get campus contact details
    /**
 *getStudentContact- To get Student contact details by taking required fields
 *@constructor
 * @param {object} studentId - Unique id for ecah and every student
 * @param {number} contactId - unique id for each and every contact
 * @param {function} callBc - deals with response
 */
  STUDENT_CONTACT.getStudentContact = function(studentId, contactId, callBc) {
    var inputObj = {};
    var studentContactModel = server.models.StudentContact;
    var studentContactDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (studentId && contactId) {
      inputObj['studentId'] = studentId;
      inputObj['contactId'] = contactId;
      // below function will give address details for an entity based on loopback include filter
      studentContactDet(inputObj, studentContactModel, 'studentContactIbfk2rel', callBc);
    } else if (studentId) {
      inputObj['studentId'] = studentId;
      // below function will give campus address details for an entity based on loopback include filter
      studentContactDet(inputObj, studentContactModel, 'studentContactIbfk2rel', callBc);
    } else {
      errorResponse(callBc);
    }
  };

  // remote method declaration to get student contact details
  STUDENT_CONTACT.remoteMethod('getStudentContact', {
    description: 'To get student address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getStudentContact',
      verb: 'GET',
    },
  });

  // remote method definition to delete student contact details
    /**
 *deleteStudentContact- To Delete Student contact details by taking required fields
 *@constructor
 * @param {object} studentId - Unique id for ecah and every student
 * @param {number} contactId - unique id for each and every contact
 * @param {function} callBc - deals with response
 */
  STUDENT_CONTACT.deleteStudentContact = function(studentId, contactId, callBc) {
    if (studentId && contactId) {
      var studentContactModel = server.models.StudentContact;
      var contactModel = server.models.Contact;
      var deleteStudentContact = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['contactId'] = contactId;
      var inputFilterObject = {};
      inputFilterObject['studentId'] = studentId;
      inputFilterObject['contactId'] = contactId;
      deleteStudentContact(primaryCheckInput, inputFilterObject, contactModel, studentContactModel, callBc);
    } else {
      errorResponse(callBc);
    }
  };

  // remote method declaration to delete student contact details
  STUDENT_CONTACT.remoteMethod('deleteStudentContact', {
    description: 'To delete student contact',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteStudentContact',
      verb: 'DELETE',
    },
  });
  //updateStudentContact remote method starts here
   /**
 *updateStudentContact- To update Student Contact Details
 *@constructor
 * @param {object} sData - contains all the data need to get updated
 * @param {function} callBack - deals with response
 */
  STUDENT_CONTACT.updateStudentContact = function(stData, cb) {
    var studentUpdate = require('../../commonCampusFiles/update-student-contact');
    if (stData.contacts[0].primaryInd == 'Y') {
      searchForInd(stData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    studentUpdate.updateStudentContactService(stData.studentContacts, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = stData.primaryInd;
        STUDENT_CONTACT.updateAll({'campusId': stData.studentId, 'contactId': stData.contactId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };

  //updateStudentContact method creation
  STUDENT_CONTACT.remoteMethod('updateStudentContact', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateStudentContact',
      verb: 'PUT',
    },
  });
};
