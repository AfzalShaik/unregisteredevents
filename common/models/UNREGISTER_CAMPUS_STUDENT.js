'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var final = [];
var notInsertedIntoDataBase = [];
// var url = 'http://localhost:3000/apiLabelset';
// var request = require('request');
// request({
//   url: url,
//   method: 'GET',
//   json: true,
// }, function(err, response, body) {
//   console.log('this is body ........................', body[1]);
// });
module.exports = function(UNREGISTER_CAMPUS_STUDENT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var async = require('async');
  UNREGISTER_CAMPUS_STUDENT.unregisteredCampusStudentUpload = function(stdata, cb) {
    var employerevent = server.models.EmployerEvent;
    var companyFileUpload = server.models.CompanyUploadLog;
    var upload = {};
    //upload.companyUploadLogId = stdata.companyUploadLogId;
    upload.compUploadTypeValueId = stdata.compUploadTypeValueId;
    upload.companyId = stdata.companyId;
    upload.inputParameters = stdata.inputParameters;
    upload.createUserId = stdata.createUserId;
    upload.createDatetime = new Date();
    upload.csvFileLocation = stdata.fileDetails.csvFileLocation;
    upload.errorFileLocation = stdata.errorFileLocation;
    companyFileUpload.create(upload, function(err, uploadRes) {
      if (err) {
        errorResponse(err, cb);
      } else {
        // console.log('created');
        employerevent.find({'where': {'empEventId': stdata.employerEventId}}, function(err, res) {
          if (err) {
            errorResponse('An error occured', cb);
          } else if (res == 0) {
            errorResponse('Employer event id is invalid', cb);
          } else {
            var name = stdata.fileDetails.name;
            var container = stdata.fileDetails.container;
            var inputFile = './Attachments/company-upload/' + container + '/' + name;
            // var inputFile = './commonValidation/unregister-student-details.csv';
            var pathForm = require('path');
            inputFile = pathForm.join(__dirname, '../../../../', inputFile);
            var readCsvFile = require('../../commonValidation/common-mass-upload.js').readFile;
            var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
            var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').validateInput;
            var unregisteredCampusStudentJson = require('./UNREGISTER_CAMPUS_STUDENT.json');
            var logger = require('../../server/boot/lib/logger');
            readCsvFile(inputFile, function(err, fileResponse) {
              if (err) {
                cb(err, null);
              } else {
                validateModel(fileResponse, unregisteredCampusStudentJson, function(error, response) {
                  if (error) {
                    throwError(JSON.stringify(error), cb);
                  } else {
                    async.map(fileResponse, createDetails, function(err, res) {
                      var a = res.every(find);
                      function find(d) {
                        return d == 0;
                      }
                      res.forEach(i);
                      function i(da) {
                        if (da == 0 || da == null) {
                          // console.log('skipped');
                        } else {
                          da.forEach(insert);
                        }
                      }
                      function insert(nowtoInsert) {
                        final.push(nowtoInsert);
                      }
                      if (err) {
                        errorResponse('Cant create', cb);
                      } else if (a) {
                        errorResponse('some objects where not inserted to avoid duplication see in notsaved.csv', cb);
                      } else {
                        async.map(final, createEvent, function(err, eventResponse) {
                          if (err) {
                            errorResponse('there was error', cb);
                          } else {
                            cb(null, eventResponse);
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
        function createDetails(data, callb) {
          var skippedDetails = [];
          var inserteddata = [];
      //var totaldataDetails = [];
          var count = 0;
          var indicator = 0;
          checkexistense(data, function(err, status) {
            if (status == 'error') {
              errorResponse('an error occured', cb);
            } else if (status == 'false') {
              data.error = 'DuplicateEntry';
              notInsertedIntoDataBase.push(data);
              failed(notInsertedIntoDataBase);
              callb(null, 0);
            } else {
              data.createDatetime = new Date();
              data.updateDatetime = new Date();
              UNREGISTER_CAMPUS_STUDENT.create(data, function(err, response) {
                if (err) {
                  errorResponse('there was error in creating', callb);
                } else {
                  inserteddata[indicator] = response;
                  indicator = indicator + 1;
                  callb(null, inserteddata);
                }
              });
            }
          });
        }
        function failed(data) {
          var fs = require('fs');
          var csv = require('fast-csv');
          var ws = fs.createWriteStream('./commonValidation/notsaved.csv');
          csv
    .writeToPath('./commonValidation/notsaved.csv', data, {headers: true})
    .on('finish', function() {
    });
        }
        function createEvent(eventData, eventCallback) {
          var eventStudent = server.models.EventStudentList;
          var date = new Date();
          var input = {

            'studentId': eventData.unregStudentId,
            'companyId': stdata.companyId,
            'employerEventId': stdata.employerEventId,
            'campusId': 113914986,
            'campusEventId': 1,
            'candidateStatusValueId': 1,
            'campusComments': 'string',
            'employerComments': 'string',
            'createDatetime': date,
            'updateDatetime': date,
            'createUserId': stdata.createUserId,
            'updateUserId': stdata.updateUserId,
            'eligibilityInd': 'y',
            'studentSubscribeInd': 'y',
            'campusPublishInd': 'y',
            'offerStatusValueId': 1,
            'compPackageId': null,
          };
          eventStudent.create(input, function(err, res) {
            if (err) {
              errorResponse('there was an error', eventCallback);
            } else {
              eventCallback(null, res);
            }
          });
        }
      }
    });
  };
  UNREGISTER_CAMPUS_STUDENT.remoteMethod('unregisteredCampusStudentUpload', {
    description: 'Send Valid Data ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/unregisteredCampusStudentUpload',
      verb: 'POST',
    },
  });
  function checkexistense(data, existCallBack) {
    var obj = {};
    obj.firstName = data.firstName;
    obj.lastName = data.lastName;
    obj.middleName = data.middleName;
    obj.emailId = data.emailId;
    obj.phoneNo = data.phoneNo;
    UNREGISTER_CAMPUS_STUDENT.find({'where': {
      'and': [obj],
    },
    }, function(err, checkResponse) {
      if (err) {
        var status = 'error';
        existCallBack(null, status);
      } else if (checkResponse == 0) {
        var  status = 'true';
        existCallBack(null, checkResponse);
      } else {
        var status = 'false';
        existCallBack(null, status);
      }
    });
  }
  UNREGISTER_CAMPUS_STUDENT.observe('before save', function unregisterBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      if (ctx.instance.unregStudentId) {
        errorResponse('student id was auto generated', next);
      } else {
        ctx.instance.updateUserId = ctx.instance.createUserId;
        ctx.instance.createDatetime = new Date();
        ctx.instance.updateDatetime = new Date();
        next();
      }
    } else {
      next();
    }
  });
  UNREGISTER_CAMPUS_STUDENT.getunregstudentDetails = function(unregstudentid, callBc) {
    var inputObj = {};
    var unregister = server.models.UnregisterCampusStudent;
    var findEntity = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    if (unregstudentid) {
      inputObj['unregStudentId'] = unregstudentid;
      findEntity(inputObj, unregister, function(err, res) {
        if (err) {
          errorResponse(err, callBc);
        } else if (res == 0) {
          errorResponse('result not found Invalid Id', callBc);
        } else {
          callBc(null, res);
        }
      });
    } else {
      errorResponse('result not found', callBc);
    } };
  /**
   * remote method---
   * exe when ever unregstudentid was called
   * accpects-unregisterStudentId
   * gets-array
   */
  UNREGISTER_CAMPUS_STUDENT.remoteMethod('getunregstudentDetails', {
    description: 'To get student unreg student details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: {arg: 'unregstudentid ',
               type: 'number',
               required: true,
               http: {source: 'query'}},
    http: {
      path: '/getunregstudentDetails',
      verb: 'get',
    },
  });
  UNREGISTER_CAMPUS_STUDENT.deleteunregisterStudentDetails = function(unregstudentid, callBc) {
    if (unregstudentid) {
      var unregister = server.models.UnregisterCampusStudent;
      unregister.destroyById(unregstudentid, function(err, responseDelete) {
        if (err) {
          callBc(err, null);
        } else if (responseDelete.count == 0) {
          errorFunction('no student id with that was registered', callBc);
        } else {
          callBc(null, responseDelete);
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
    // remote method declaration to delete campus missing program details
  UNREGISTER_CAMPUS_STUDENT.remoteMethod('deleteunregisterStudentDetails', {
    description: 'To delete unregisterd student details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: {
      arg: 'unregstudentid',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    http: {
      path: '/deleteunregisterStudentDetails',
      verb: 'delete',
    },
  });
  UNREGISTER_CAMPUS_STUDENT.updateUnregisterStudentDetails = function(data, cb) {
    // console.log(data);
    var unregesterStudentUpdate = require('../../update/update-unregister-student');
    unregesterStudentUpdate.updateUnregStudentDetails(data, function(err, resp) {
      if (err) {
        errorResponse('there was an error', cb);
        cb(err, resp);
      } else if (resp == 0) {
        errorResponse('Invalid student id', cb);
      } else {
        cb(null, resp);
      }
    });
  };
  UNREGISTER_CAMPUS_STUDENT.remoteMethod('updateUnregisterStudentDetails', {
    description: 'update update Unregister Student Details details',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateUnregisterStudentDetails',
      verb: 'PUT',
    },
  });
};
