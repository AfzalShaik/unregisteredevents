'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var Joi = require('joi');
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
module.exports = function(DEPARTMENT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  DEPARTMENT.observe('before save', function departmentBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });

  // remote method definition to get campus department details based on department id and company id
    /**
 *getDepartmentDetails- To get campus department contact details by taking required fields
 *@constructor
 * @param {object} departmentId - Unique id for ecah and every department
 * @param {number} campusId - unique id for each and every campus
 * @param {function} callBc - deals with response
 */
  DEPARTMENT.getDepartmentDetails = function(departmentId, campusId, callBc) {
    var inputObj = {};
    var department = server.models.Department;
    var entityDetailsUsingIncludeFilter = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (departmentId && campusId) {
      inputObj['campusId'] = campusId;
      inputObj['departmentId'] = departmentId;
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, department, 'departmentCampusDetails', callBc);
    } else if (campusId) {
      inputObj['campusId'] = campusId;
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, department, 'departmentCampusDetails', callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus department details based on on department id and company id
  DEPARTMENT.remoteMethod('getDepartmentDetails', {
    description: 'To get campus department details based on department id and company id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'departmentId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getDepartmentDetails',
      verb: 'GET',
    },
  });
  //CampusUpdate custom method starts here
   /**
 *UpdateCampusContact- To update Campus Conatct Details
 *@constructor
 * @param {object} departmentData - contains all the data need to get updated
 * @param {function} callBack - deals with response
 */
  DEPARTMENT.updateDepartment = function(departmentData, cb) {
    var updateDept = require('../../commonCampusFiles/update-department-profile');
    updateDept.updateDepartmentProfile(departmentData, function(err, resp) {
      logger.info('upating Department Profile ');
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };
  //remoteMethod for campus update
  DEPARTMENT.remoteMethod('updateDepartment', {
    description: 'Send Valid campusId ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateDepartment',
      verb: 'PUT',
    },
  });
  DEPARTMENT.departmentProfile = function(departmentId, cb) {
    var inputObj = {};
    inputObj.departmentId = departmentId;
    var department = server.models.Department;
    var getEntityDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    getEntityDetails(inputObj, department, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var getProfile = require('../../commonCampusFiles/department-profile').getDepartmentProfile;
        getProfile(resp.data[0], function(err, res) {
          if (err) {
            errorFunction('an error', cb);
          } else {
            cb(null, res);
          }
        });
      }
    });
  };
  //remoteMethod for campus update
  DEPARTMENT.remoteMethod('departmentProfile', {
    description: 'Send Valid campusId ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'departmentId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/departmentProfile',
      verb: 'GET',
    },
  });
  DEPARTMENT.departmentGraphData = function(departmentId, cb) {
    var inputObj = {};
    inputObj.departmentId = departmentId;
    var department = server.models.Department;
    var getEntityDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    getEntityDetails(inputObj, department, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else if (resp == 0) {
        errorFunction('invalid department Id', cb);
      } else {
        var getGraphData = require('../../commonCampusFiles/department-profile').getGraphData;
        getGraphData(resp.data[0], function(err, res) {
          if (err) {
            errorFunction('an error', cb);
          } else {
            cb(null, res);
          }
        });
      }
    });
  };
  //remoteMethod for campus update
  DEPARTMENT.remoteMethod('departmentGraphData', {
    description: 'Send Valid campusId ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'departmentId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/departmentGraphData',
      verb: 'GET',
    },
  });
};
