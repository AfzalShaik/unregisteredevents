'use strict';
var server = require('../../server/server');
module.exports = function(DEPARTMENT_ADDRESS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;

  // remote method definition to create address for particular department
   /**
 *createDepartmentAddress- To Create department address details
 *@constructor
 * @param {object} data - contains all the data need to get created
 * @param {function} callBc - deals with response
 */
  DEPARTMENT_ADDRESS.createDepartmentAddress = function(data, callBc) {
    if (data.campusId && data.departmentId) {
      if (data.primaryInd == 'Y') {
        searchForInd(data, function(err, res) {
          if (err) {
            errorResponse('there was an error', callBc);
          } else {
            // console.log('sucess');
          }
        });
      }
      var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = data.campusId;
      inputFilterObject['departmentId'] = data.departmentId;
      var addressModel = server.models.Address;
      var departmentModel = server.models.Department;
      checkEntityExistence(departmentModel, inputFilterObject, function(checkStatus) {
        if (checkStatus) {
          addressModel.create(data, function(error, addressRes) {
            if (error) {
              callBc(error, null);
            } else if (addressRes) {
              DEPARTMENT_ADDRESS.create({
                'departmentId': Number(data.departmentId),
                'campusId': Number(data.campusId),
                'addressId': Number(addressRes.addressId),
                'primaryInd': String(data.primaryInd),
              }, function(err, resp) {
                if (err) {
                  errorResponse(callBc);
                } else {
                  // addressRes.requestStatus = true;
                  callBc(null, addressRes);
                }
              });
            } else {
              errorResponse(callBc);
            }
          });
        } else {
          errorResponse(callBc);
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    DEPARTMENT_ADDRESS.find({'where': {'departmentId': data.departmentId,
    'campusId': data.campusId, 'primaryInd': 'Y'}}, function(err, res) {
      if (err) {
        errorResponse('campusId or Education person id was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
      } else {
        var update = {};
        update.primaryInd = 'N';
        DEPARTMENT_ADDRESS.updateAll({'departmentId': data.departmentId,
        'campusId': data.campusId, 'primaryInd': 'Y'}, update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            // console.log('this is response of update', resp);
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create address for particular department
  DEPARTMENT_ADDRESS.remoteMethod('createDepartmentAddress', {
    description: 'Useful to create address for particular department',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createDepartmentAddress',
      verb: 'post',
    },
  });
  // remote method definition to get  campus department address details
  /**
 *getCampusDeptAddress- To get campus department contact details by taking required fields
 *@constructor
 * @param {number} departmentId - Unique id for ecah and every department
 * @param {number} campusId - unique id for each and every campus
 * @param {number} addressId - unique id for every address
 * @param {function} callBc - deals with response
 */
  DEPARTMENT_ADDRESS.getCampusDeptAddress = function(departmentId, campusId, addressId, callBc) {
    var inputObj = {};
    var departmentAddrs = server.models.DepartmentAddress;
    var campusDeptAddress = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').getCompleteAddress;
    if (departmentId && campusId && addressId) {
      inputObj['departmentId'] = departmentId;
      inputObj['campusId'] = campusId;
      inputObj['addressId'] = addressId;
      // below function will give address details for an entity based on loopback include filter
      campusDeptAddress(inputObj, departmentAddrs, 'departmentAddress', callBc);
    } else if (departmentId && campusId) {
      inputObj['departmentId'] = departmentId;
      inputObj['campusId'] = campusId;
      // below function will give address details for an entity based on loopback include filter
      campusDeptAddress(inputObj, departmentAddrs, 'departmentAddress', callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus department address details
  DEPARTMENT_ADDRESS.remoteMethod('getCampusDeptAddress', {
    description: 'To get campus department address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'departmentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getCampusDeptAddress',
      verb: 'get',
    },
  });
  // remote method definition to delete campus department address details
    /**
 *deleteCampusDeptAddress- To delete campus department address details by taking required fields
 *@constructor
 * @param {number} departmentId - Unique id for ecah and every department
 * @param {number} campusId - unique id for each and every campus
 * @param {number} addressId - unique id for every address
 * @param {function} callBc - deals with response
 */
  DEPARTMENT_ADDRESS.deleteCampusDeptAddress = function(departmentId, campusId, addressId, callBc) {
    if (departmentId && campusId && addressId) {
      var departmentAddressModel = server.models.DepartmentAddress;
      var addressModel = server.models.Address;
      var delCampusDeptAddress = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['addressId'] = addressId;
      var inputFilterObject = {};
      inputFilterObject['departmentId'] = departmentId;
      inputFilterObject['campusId'] = campusId;
      inputFilterObject['addressId'] = addressId;
      delCampusDeptAddress(primaryCheckInput, inputFilterObject, addressModel, departmentAddressModel, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to delete campus department address details
  DEPARTMENT_ADDRESS.remoteMethod('deleteCampusDeptAddress', {
    description: 'To delete campus department contact details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'departmentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteCampusDeptAddress',
      verb: 'delete',
    },
  });

  //update department address remoteMethod
   /**
 *updateDepartmentAddress- To update DEpartment address Details
 *@constructor
 * @param {object} deptData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  DEPARTMENT_ADDRESS.updateDepartmentAddress = function(deptData, cb) {
    var deptUpdate = require('../../commonCampusFiles/update-department-address.js');
    if (deptData.primaryInd == 'Y') {
      searchForInd(deptData, function(err, res) {
        if (err) {
          errorResponse('there was an error', cb);
        } else {
          // console.log('sucess');
        }
      });
    }
    deptUpdate.DepartmentUpdate(deptData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = deptData.primaryInd;
        DEPARTMENT_ADDRESS.updateAll({'campusId': deptData.campusId,
        'addressId': deptData.addressId, 'departmentId': deptData.departmentId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };

  //update department address remoteMethod
  DEPARTMENT_ADDRESS.remoteMethod('updateDepartmentAddress', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateDepartmentAddress',
      verb: 'PUT',
    },
  });
};
