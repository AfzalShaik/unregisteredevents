'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
module.exports = function(EMPLOYER_DRIVE_CAMPUSES) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  EMPLOYER_DRIVE_CAMPUSES.observe('before save', function employerDriveCampusesBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.empDriveCampusId == undefined) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      logger.info('EMPLOYER_DRIVE_CAMPUSES Creation Initiated');
      next();
    } else if (ctx.isNewInstance) {
      errorFunction('compPackageId is system generated value', next);
    } else {
      next();
    }
  });
};
