'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
// var final = [];
// var repeated = [];
var finalNonRepeat = [];
var notInsertedIntoDataBase = [];
var inserteddata1 = [];
var UnregisterCampusStudent = server.models.UnregisterCampusStudent;
module.exports = function(UNREGISTER_CAMPUS_STUDENT_WORK) {
  notInsertedIntoDataBase = [];
  var validateModel = require('../../commonValidation/unregistered-studentvalidation.js').validateJson;
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var async = require('async');
  var unsucess = 0;
  var sucess = 0;
  var empEvent = {};
  UNREGISTER_CAMPUS_STUDENT_WORK.unregisteredCampusStudentUpload = function(stdata, cb) {
    var name = stdata.fileDetails.name;
    var container = stdata.fileDetails.container;
    empEvent.empEventId = stdata.empEventId;
    var pathForm = require('path');
    var inputFile = './attachments/' + container + '/' + name;
    var fullPathForm = pathForm.join(__dirname, '../../../../', inputFile);
    // var errorFileLocation = './attachments/' + container + '/download/' + stdata.fileDetails.name;
    // var inputFile = './commonValidation/unregister-student-details.csv';
    var employerevent = server.models.EmployerEvent;
    employerevent.find({
      'where': {
        'empEventId': stdata.empEventId,
        'companyId': stdata.companyId,
      },
    }, function(err, res) {
      if (err) {
        errorResponse('An error occured', cb);
      } else if (res == 0) {
        errorResponse('Employer event id is invalid', cb);
      } else {
        var readCsvFile = require('../../commonValidation/common-mass-upload.js').readFile;
        var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
        // var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').validateInput;
        // var unregisteredCampusStudentJson = require('./UNREGISTER_CAMPUS_STUDENT.json');
        var logger = require('../../server/boot/lib/logger');
        readCsvFile(fullPathForm, function(err, fileResponse) {
          if (err) {
            cb(err, null);
          } else {
            var firstObj = fileResponse[0];
            if ((firstObj.firstName || firstObj.firstName == '') &&
              (firstObj.lastName || firstObj.lastName == '') && (firstObj.middleName || firstObj.middleName == '') &&
              (firstObj.gender || firstObj.gender == '') && (firstObj.dateOfBirth || firstObj.dateOfBirth == '') &&
              (firstObj.highlights || firstObj.highlights == '') && (firstObj.phoneNo || firstObj.phoneNo == '') &&
              (firstObj.emailId || firstObj.emailId == '') ||
              (firstObj.score || firstObj.score == '') ||
              (firstObj.programName || firstObj.programName == '') ||
              (firstObj.interests || firstObj.interests == '') ||
              (firstObj.skills || firstObj.skills == '')) {
              async.map(fileResponse, validateTheInput, function(error, response) {
                if (error) {
                  errorResponse(JSON.stringify(error), cb);
                } else {
                  var upload = {};
                  var companyFileUpload = server.models.CompanyUploadLog;
                  upload.compUploadTypeValueId = stdata.compUploadTypeValueId;
                  upload.companyId = stdata.companyId;
                  upload.createUserId = stdata.createUserId;
                  upload.createDatetime = new Date();
                  companyFileUpload.create(upload, function(err5, res5) {
                    if (err5) {
                      errorResponse(err5, cb);
                    } else {
                      // console.log(res5);
                      var final = cleanArray(response);
                      async.map(final, getNonRepeatData, function(err, res) {
                        if (err) {
                          throwError('an error', callBc);
                        } else {
                          async.map(final, getFinalData, function(err3, res3) {
                            if (err3) {

                            } else {
                              var unregisterWork = server.models.UnregisterCampusStudentWork;
                              unregisterWork.destroyAll({
                                'companyUploadLogId': res5.companyUploadLogId,
                              },
                                function(err11, res11) {
                                  if (err11) {
                                    errorResponse(err5, cb);
                                  } else {
                                    async.map(finalNonRepeat, createDetails, function(err, res) {
                                      if (err) {
                                        errorResponse(err, cb);
                                      } else {
                                        var async = require('async');
                                        // console.log('insssereted', inserteddata1);

                                        async.map(inserteddata1, createEvent,
                                          function(err, response1) {
                                            if (err) {
                                              cb(err, null);
                                            } else {
                                              var companyFileUpload = server.models.CompanyUploadLog;
                                              var upload = {};
                                              upload.companyId = stdata.companyId;
                                              upload.inputParameters = stdata.inputParameters;
                                              upload.createUserId = stdata.createUserId;
                                              upload.createDatetime = new Date();
                                              var pathForm = require('path');
                                              var csvFileLocation1 = './attachments/' + container + '/' + stdata.fileDetails.name + '/' + stdata.fileDetails.originalFileName;
                                              var errorFileLocation1 = './attachments/' + container + '/' + 'download' + '/' + stdata.fileDetails.name;
                                              upload.csvFileLocation = pathForm.join(__dirname, '../../../../', csvFileLocation1);
                                              upload.errorFileLocation = pathForm.join(__dirname, '../../../../', errorFileLocation1);
                                              upload.totalNoRecs = fileResponse.length;
                                              upload.noSuccessRecs = sucess;
                                              upload.noFailRecs = fileResponse.length - sucess;
                                              companyFileUpload.updateAll({
                                                'companyUploadLogId': res5.companyUploadLogId,
                                              }, upload, function(err, uploadRes) {
                                                if (err) {
                                                  errorResponse('cant create an event', cb);
                                                } else {
                                                  // console.log('not isnserted in database', notInsertedIntoDataBase);
                                                  failed(notInsertedIntoDataBase, function(err1, res1) {
                                                    if (err1) {
                                                      errorResponse('err', cb);
                                                    } else {
                                                      notInsertedIntoDataBase = [];
                                                      // console.log('noooooooooooottttttttttttttttttt', notInsertedIntoDataBase);
                                                      companyFileUpload.findOne({
                                                        'where': {
                                                          'companyUploadLogId': res5.companyUploadLogId,
                                                        },
                                                      }, function(err10, res10) {
                                                        sucess = 0;
                                                        finalNonRepeat = [];
                                                        inserteddata1 = [];
                                                        notInsertedIntoDataBase = [];
                                                        cb(null, res10);
                                                      });
                                                      sucess = 0;
                                                      unsucess = 0;
                                                      inserteddata1 = [];
                                                    }
                                                  });
                                                }
                                              });
                                            }
                                          });
                                      }
                                    });
                                  }
                                });
                            }
                          });
                        }
                      });

                      function getNonRepeatData(obj, cb) {
                        // console.log(obj);
                        obj.companyUploadLogId = res5.companyUploadLogId;
                        var unregisterWork = server.models.UnregisterCampusStudentWork;
                        unregisterWork.create(obj, function(err, res) {
                          if (err) {
                            // console.log(err);
                            var data1 = {};
                            data1.firstName = obj.firstName;
                            data1.lastName = obj.lastName;
                            data1.middleName = obj.middleName;
                            data1.gender = obj.gender;
                            data1.dateOfBirth = obj.dateOfBirth;
                            data1.highlights = obj.highlights;
                            data1.phoneNo = obj.phoneNo;
                            data1.emailId = obj.emailId;
                            data1.error = err.message;
                            notInsertedIntoDataBase.push(data1);
                            cb(null, null);
                          } else {
                            cb(null, res);
                          }
                        });
                      }
                    }
                  });
                }
              });

              function getFinalData(obj, cb) {
                var data = {};
                data['firstName'] = (obj.firstName);
                data['middleName'] = (obj.middleName) ? (obj.middleName) : undefined;
                data['lastName'] = (obj.lastName);
                data['phoneNo'] = (obj.phoneNo) ? (obj.phoneNo) : undefined;
                // data['dateOfBirth'] = (obj.dateOfBirth) ? (obj.dateOfBirth) : undefined;
                data['emailId'] = (obj.emailId) ? (obj.emailId) : undefined;
                // console.log(data);
                var unregisterWork = server.models.UnregisterCampusStudentWork;
                unregisterWork.find({
                  'where': {
                    'or': [{
                      'and': [{
                        'firstName': obj.firstName,
                      }, {
                          'lastName': obj.lastName,
                        }],
                    },
                    {
                      'or': [{
                        'phoneNo': data.phoneNo,
                      }, {
                          'emailId': data.emailId,
                        }],
                    },
                    ],
                  },
                }, function(err4, res4) {
                  // console.log('there are responses', res4);
                  if (res4.length === 1) {
                    finalNonRepeat.push(obj);
                    cb(null, 'done');
                  } else if (res4.length > 1) {
                    obj.error = 'repeated entry';
                    notInsertedIntoDataBase.push(obj);
                    cb(null, 'done');
                  } else {
                    cb(null, 'done');
                  }
                });
              }

              function validateTheInput(object, validCb) {
                var validateModel = require('../../commonValidation/unregistered-studentvalidation.js').validateJson;
                validateModel(object, function(validationErr, validationResponse) {
                  if (validationErr) {
                    // console.log(validationErr);
                    // var err = {};
                    // err.error = validationErr.error;
                    // err.data = validationErr.data;
                    // console.log('an err', err);
                    // console.log('in validation error', validationErr);
                    notInsertedIntoDataBase.push(validationErr);
                    validCb(null, 0);
                  } else {
                    validCb(null, validationResponse);
                  }
                });
              }
            } else {
              errorResponse('Invalid file Headers', cb);
            }
          }
        });
      }
    });

    function createEvent(eventData, eventCallback) {
      // console.log('cccccccccccccccccccccccc', eventData);
      var eventStudent = server.models.EventStudentList;
      var date = new Date();
      var input = {

        'studentId': eventData.unregStudentId,
        'companyId': stdata.companyId,
        'employerEventId': stdata.empEventId,
        //  'campusId': stdata.campusId,
        'campusEventId': null,
        'totalCompValue': null,
        'candidateStatusValueId': 376,
        'campusComments': 'string',
        'employerComments': 'string',
        'createDatetime': date,
        'updateDatetime': date,
        'createUserId': stdata.createUserId,
        'updateUserId': stdata.createUserId,
        'eligibilityInd': 'y',
        'studentSubscribeInd': 'y',
        'campusPublishInd': 'y',
        'offerStatusValueId': 1,
        'compPackageId': null,
        'registrationInd': 'N',
      };
      eventStudent.findOne({
        'where': {
          and: [{
            'studentId': eventData.unregStudentId,
          },
          {
            'employerEventId': stdata.empEventId,
          },
          ],
        },
      }, function(err, res) {
        // console.log(res);
        if (err) {

        } else if (res == null) {
          eventStudent.create(input, function(err, res) {
            // console.log('ressssssssssssssssssssssssssssssssssssssssssssssssssssss', err, input);
            if (err) {
              errorResponse(err, eventCallback);
            } else {
              var employerEvent = server.models.EmployerEvent;
              employerEvent.findOne({
                'where': {
                  'empEventId': stdata.empEventId,
                },
              }, function(errorEmp, successEmp) {
                var empObj = {};
                empObj = successEmp;
                empObj.eventStatusValueId = 317;
                successEmp.updateAttributes(empObj, function(empErr, empOut) {
                  sucess++;
                  eventCallback(null, res);
                });
              });
            }
          });
        } else {
          // console.log('getting hear');
          var data1 = {};
          data1.firstName = eventData.firstName;
          data1.lastName = eventData.lastName;
          data1.middleName = eventData.middleName;
          data1.gender = eventData.gender;
          data1.dateOfBirth = eventData.dateOfBirth;
          data1.highlights = eventData.highlights;
          data1.phoneNo = eventData.phoneNo;
          data1.emailId = eventData.emailId;
          data1.error = 'already part of the event';
          notInsertedIntoDataBase.push(data1);
          eventCallback(null, 0);
        }
      });
    }

    function createDetails(data, callb) {
      var skippedDetails = [];
      var inserteddata = [];
      var count = 0;
      var indicator = 0;
      var first = data.firstName.toUpperCase();
      var last = data.lastName.toUpperCase();
      data.searchName = first + ' ' + last;
      checkexistense(data, function(err, status) {
        // console.log('sssssssssssssssssssssssssss', err, status);
        if (status.data == 'error') {
          errorResponse('an error occured', cb);
        } else if (status.data == 'false') {
          // var data1 = {};
          // data1.empEventId = empEvent.empEventId;
          // data1.firstName = data.firstName;
          // data1.lastName = data.lastName;
          // data1.middelName = data.middleName;
          // data1.gender = data.gender;
          // data1.DateOfBrith = data.dateOfBirth;
          // // data1.highlights = data.highlights;
          // data1.highlights = data.highlights;
          // data1.phoneNo = data.phoneNo;
          // data1.emailId = data.emailId;
          // data1.error = 'DuplicateEntry';
          // notInsertedIntoDataBase.push(data1);
          // callb(null, 0);
          // console.log('status', status.res);
          inserteddata1.push(status.res);
          callb(null, 0);
        } else {
          data.createDatetime = new Date();
          data.updateDatetime = new Date();
          data.createUserId = stdata.createUserId;
          data.updateUserId = stdata.updateUserId;
          var UnregisterCampusStudent = server.models.UnregisterCampusStudent;
          data.empEventId = stdata.empEventId;
          // data.phoneNo = parseInt(data.phoneNo);
          // delete data.dateOfBirth;
          // console.log('this is data', stdata);
          UnregisterCampusStudent.create(data, function(err, response) {
            // console.log('eeeeeeeeeeeeeeeeeeeeeeeeeee', err);
            if (err) {
              errorResponse(err, callb);
            } else {
              // console.log(response);
              inserteddata[indicator] = response;
              indicator = indicator + 1;
              inserteddata1.push(response);
              callb(null, inserteddata);
            }
          });
        }
      });
    }

    function failed(data, cb) {
      console.log(data);
      unsucess++;
      var fs = require('fs');
      var csv = require('fast-csv');
      var ws = fs.createWriteStream(fullPathForm);
      csv
        .writeToPath(fullPathForm, data, {
          headers: true,
        })
        .on('finish', function() {});
      cb(null, 'done');
    }
  };
  UNREGISTER_CAMPUS_STUDENT_WORK.remoteMethod('unregisteredCampusStudentUpload', {
    description: 'Send Valid Data ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/unregisteredCampusStudentUpload',
      verb: 'POST',
    },
  });

  function checkexistense(data, existCallBack) {
    // console.log('this is data', data);
    var obj = {};
    obj.firstName = data.firstName;
    obj.lastName = data.lastName;
    obj.searchName = data.searchName;
    obj.middleName = (data.middleName) ? (data.middleName) : undefined;
    obj.emailId = (data.emailId) ? (data.emailId) : undefined;
    obj.phoneNo = (data.phoneNo) ? (data.phoneNo) : undefined;
    // console.log(obj);
    var UnregisterCampusStudent = server.models.UnregisterCampusStudent;
    UnregisterCampusStudent.findOne({
      'where': {
        'or': [{
          'and': [{
            'firstName': obj.firstName,
          }, {
              'lastName': obj.lastName,
            }],
        },
        {
          'or': [{
            'phoneNo': data.phoneNo,
          }, {
              'emailId': data.emailId,
            }],
        },
        ],
      },
    }, function(err, checkResponse) {
      // console.log('this is check response', checkResponse);
      if (err) {
        var status = {};
        status.data = 'error';
        existCallBack(null, status);
      } else if (checkResponse == null) {
        var status = {};
        status.data = 'true';
        existCallBack(null, status);
      } else {
        var status = {};
        // console.log('check response', checkResponse);
        status.data = 'false';
        status.res = checkResponse;
        existCallBack(null, status);
      }
    });
  }

  // to get unregistered student details
  UNREGISTER_CAMPUS_STUDENT_WORK.getUnregisteredStudents = function(employerEventId, companyId, cb) {
    var eventstudentList = server.models.EventStudentList;
    eventstudentList.find({
      'where': {
        'and': [{
          employerEventId: employerEventId,
        }, {
          'companyId': companyId,
        },
        {
          'registrationInd': 'N',
        },
        ],
      },
    }, function(err, resp) {
      if (resp) {
        async.map(resp, getSkills, function(skillsErr, skillsOut) {
          cb(null, skillsOut);
        });
      } else {
        cb(null, []);
      }
    });
  };

  function getSkills(obj, callBc) {
    var unregisterCampusStudent = server.models.UnregisterCampusStudent;
    unregisterCampusStudent.findOne({
      'where': {
        'unregStudentId': obj.studentId,
      },
    }, function(studentErr, studentOut) {
      if (studentOut) {
        callBc(null, studentOut);
      } else {
        callBc(null, studentOut);
      }
    });
  }
  UNREGISTER_CAMPUS_STUDENT_WORK.remoteMethod('getUnregisteredStudents', {
    description: 'To get Unregistered Students details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'employerEventId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getUnregisteredStudents',
      verb: 'get',
    },
  });
};
