
'use strict';
var nodemailer = require('nodemailer');
var config = require('../../server/config.json');
// var configuration = require('../../settings/configuration.json');
var path = require('path');
var server = require('../../server/server');
var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
var lookup = require('../../commonValidation/lookupMethods').lookupMethod;
var signUpService = require('../../commonValidation/signup-service.js').createSignUpService;
var globalPassword;
// var user = require('../../server/config.json');
var host = config.host;
var port = config.port;
var uiPort = config.uiPort.uiPort;
module.exports = function(SCORA_USER) {
  //Util function Required
  //Well i am defining path of util function in "script.js"

  //Definition of ScoraUser model

  //Create a newUser function
  /*
  •	A new user record is created with the following information:
  o	User_name (which is the email id of the user)
  o	user_id (system generated)
  o	user_status_code (P – Pending Confirmation)
  •	user_id is primary key. It should be auto generated and returned to calling unit after successful CREATE
  •	Refer to the standard rules for Create_datetime, create_user_id, update_datetime, update_userid columns
  •	On successful completion of the request, the requestStatus should be set to TRUE. In case of failure, the requestStatus should be FALSE.

   */

  SCORA_USER.observe('before save', function saveUser(ctx, next) {
    if (ctx.isNewInstance) {
      var randomstring = require('randomstring');
      globalPassword = randomstring.generate({
        length: 12,
        charset: 'alphanumeric',
      });
      // ctx.instance.updateUserId = ctx.instance.createUserId; // TODO: DEFAULT createUserId and updateUserId
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      ctx.instance.createUserId = 1;
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.password = (ctx.instance.password) ? ctx.instance.password : globalPassword;
      // console.log('ctx.instance..................................... ', ctx.instance);
      checkEntity(ctx.instance, function(uniqueErr, uniqueOut) {
        if (uniqueOut) {
          unauthorizedRes(next);
        } else {
          next();
        }
      });
    } else {
      next();
    }
  });
  //Create a getUser function
  /*
  •	User_id should be provided for the read
  •	Get user’s basic data from the table user
  •	On successful completion of the request, the requestStatus should be set to TRUE. In case of failure, the requestStatus should be FALSE.

   */
  SCORA_USER.getScoraExchangeUser = function(userId, cb) {
    if (userId) {
      var inputObj = {};
      inputObj['user_id'] = userId;
      //User = server.models.ScoraExchangeUser
      entityDetailsUsingIncl̥udeFilter(inputObj, User, 'userRole', cb);
    } else {
      errorResponse(cb);
    }
  };

  SCORA_USER.remoteMethod('getScoraExchangeUser', {
    description: 'To get user detail along with user_role',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'userId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getScoraExchangeUser',
      verb: 'get',
    },
  });
  /*
  Update
  •	user_id should be provided for the read
  •	Only user_status_code can be updated as follows:
  o	E (enabled) to D (Disabled) or D (Disabled) to E (Enabled).
  •	On successful completion of the request, the requestStatus should be set to TRUE.
  In case of failure, the requestStatus should be FALSE.
  */

  //remote method to send verification link to the regestered user
  SCORA_USER.afterRemote('create', function(context, userInstance, next) {
    // console.log(globalPassword);
    var options = {
      type: 'email',
      to: userInstance.email,
      from: 'noreply@loopback.com',
      subject: 'Thanks for registering.',
      template: path.resolve(__dirname, '../../server/views/verify.ejs'),
      redirect: '/verified',
      user: SCORA_USER,
      html: 'Use this Password to reset: ' + globalPassword,
    };
    // console.log('options', options);
    userInstance.verify(options, function(err, response, next) {
      // console.log('789999999999999999999999');
      // console.log(response);
      if (err) {
        //User.deleteById(user.id);
        return err;
      }
      // console.log('..................................... ', userInstance.id, err);
      signUpService(userInstance, function(campusErr, CampusResponse) {
        if (campusErr) {
          return campusErr;
        }
      });
    });
    next();
  });

  // Method to render
  SCORA_USER.afterRemote('prototype.verify', function(context, SCORA_USER, next) {
    // console.log('prototype.verify');
    context.res.render('response', {
      title: 'A Link to reverify your identity has been sent ' +
        'to your email successfully',
      content: 'Please check your email and click on the verification link ' +
        'before logging in',
      redirectTo: '/',
      redirectToLinkText: 'Log in',
    });
  });
  //send password reset link when requested
  SCORA_USER.on('resetPasswordRequest', function(info) {
    // console.log('resetPasswordRequest', info);
    var url = 'http://' + config.host + ':' + config.port + '/reset-password';
    var html = 'Click <a href="' + url + '?access_token=' +
      info.accessToken.id + '">here</a> to reset your password';

    SCORA_USER.app.models.Email.send({
      to: info.email,
      from: info.email,
      subject: 'Password reset',
      html: html,
    }, function(err) {
      if (err) return; // console.log('> error sending password reset email');
      // console.log('> sending password reset email to:', info.email);
    });
  });

  //render UI page after password change
  SCORA_USER.afterRemote('changePassword', function(context, user, next) {
    console.log('changePassword', host, uiPort);
    context.res.render('response', {
      title: 'Password changed successfully',
      content: 'Please login again with new password',
      redirectTo: 'http://' + host + ':' + uiPort,
      redirectToLinkText: 'Log in',
    });
  });

  //render UI page after password reset
  SCORA_USER.afterRemote('setPassword', function(context, user, next) {
    // console.log('setPassword');
    context.res.render('response', {
      title: 'Password reset success',
      content: 'Your password has been reset successfully',
      redirectTo: '/',
      redirectToLinkText: 'Log in',
    });
  });


  //Login Page
  SCORA_USER.afterRemote('login', function(context, user, next) {
    // console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    var userRole = server.models.ScoraUserRole;
    var student = server.models.Student;
    var educationPerson = server.models.EducationPerson;
    var employerPerson = server.models.EmployerPerson;
    // console.log(user);
    SCORA_USER.findOne({
      'where': {
        'id': user.userId,
      },
    }, function(err, userData) {
      if (err) {
        return err;
      } else {
        userRole.findOne({
          'where': {
            'and': [{
              'id': user.userId,
            }],
          },
        }, function(roleErr, roleResp) {
          // console.log(roleResp);
          if (roleResp.roleCode == 'STUDENT') {
            student.findOne({
              'where': {
                'id': user.userId,
              },
            }, function(studentErr, studentResp) {
              if (studentErr) {
                return studentErr;
              } else if (studentResp) {
                user.email = userData.email;
                user.role = roleResp.roleCode;
                user.studentId = studentResp.studentId;
                next();
              } else {
                user.email = userData.email;
                user.role = roleResp.roleCode;
                next();
              }
            });
          } else if (roleResp.roleCode == 'PLCDIR') {
            // console.log('roleresppppppppppppppppp ', roleResp, user.userId);
            educationPerson.findOne({
              'where': {
                'id': user.userId,
              },
            }, function(eduErr, eduResp) {
              // console.log(eduResp);
              user.email = userData.email;
              user.role = roleResp.roleCode;
              user.campusId = eduResp.campusId;
              user.educationPersonId = eduResp.educationPersonId;
              next();
            });
          } else if (roleResp.roleCode == 'RECDIR') {
            employerPerson.findOne({
              'where': {
                'id': user.userId,
              },
            }, function(empErr, empResp) {
              user.email = userData.email;
              user.role = roleResp.roleCode;
              user.companyId = empResp.companyId;
              user.employerPersonId = empResp.employerPersonId;
              next();
            });
          } else {
            // console.log('hiiiiiiiiiii');
            user.email = userData.email;
            user.role = 'SYSADMIN';
            next();
          }
        });
      }
    });
  });

  function checkEntity(input, callBc) {
    var lookup = require('../../commonValidation/lookupMethods').lookupMethod;
    lookup('ROLE_TYPE_CODE', function(err, response) {
      var campusIndicator = response.find(findCampus);
      var companyIndicator = response.find(findCompany);
      var studentIndicator = response.find(findStudent);

      function findCampus(campusVal) {
        return campusVal.lookupValue === 'Campus';
      }

      function findCompany(companyVal) {
        return companyVal.lookupValue === 'Employer';
      }

      function findStudent(studentVal) {
        return studentVal.lookupValue === 'Student';
      }
      if (input.roleTypeValueId == campusIndicator.lookupValueId) {
        var campus = server.models.Campus;
        campus.find({
          'where': {
            'name': input.entityName,
          },
        }, function(campusErr, campusOut) {
          if (campusOut.length > 0) {
            callBc(null, campusOut);
          } else {
            callBc(null, null);
          }
        });
      } else if (input.roleTypeValueId == companyIndicator.lookupValueId) {
        var company = server.models.Company;
        company.find({
          'where': {
            'name': input.entityName,
          },
        }, function(companyErr, companyOut) {
          if (companyOut.length > 0) {
            callBc(null, companyOut);
          } else {
            callBc(null, null);
          }
        });
      } else {
        callBc(null, null);
      }
    });
  }
  var unauthorizedRes = function(next) {
    var adminAuthorizationError = new Error('Entity Name instance is already present');
    adminAuthorizationError.statusCode = 401;
    // adminAuthorizationError.requestStatus = false;
    next(adminAuthorizationError, null);
  };
};
