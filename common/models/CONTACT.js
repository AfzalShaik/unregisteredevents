//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
// exporting function to use it in another modules if requires
module.exports = function(CONTACT) {
  var errorResponse = require("../../ErrorHandling/customizeErrorTypes.js").invalidInputError;
  // when request comes to contact model then we execute logic or change request object before saving into database
  CONTACT.observe('before save', function contactBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });
// remote method definition to get campus contact details
/**
 *getCampusContactDet- To get company contact details
 *@constructor
 * @param {number} campusId - unique id of campus
 * @param {number} contactId - unique id for each contact
 * @param {function} callBack - deals with response
 */
  CONTACT.getCampusContactDet = function (campusId,contactId, callBack) {
    var inputObj={};
    var campusContactModel=server.models.CampusContact;
    var campusContactsDet = require("../../ServicesImpl/CommonImpl/getEntityDetails.js").entityDetailsUsingIncludeFilter;
    if (campusId && contactId ) {
      inputObj['campusId']=campusId;
      inputObj['contactId']=contactId;
      // below function will give contact details for an entity based on loopback include filter
      campusContactsDet(inputObj,campusContactModel,"campusContact",callBack);
    } else if (campusId) {
      inputObj['campusId']=campusId;
      // below function will give contact details for an entity based on loopback include filter
      campusContactsDet(inputObj,campusContactModel,"campusContact",callBack);
    }
    else {
      errorResponse(callBack);
    }
  };
  // remote method declaration to get campus contact details
  CONTACT.remoteMethod('getCampusContactDet', {
    description: "To get campus contact details",
    returns: {
      type: 'array',
      root: true
    },
    accepts: [{arg: 'campusId', type: 'number', required: true, http: {source: 'query'}},
      {arg: 'contactId', type: 'number', http: {source: 'query'}}
    ],
    http: {
      path: '/getCampusContactDet',
      verb: 'get'
    }
  });
  // remote method definition to get company contact details
  /**
 *getCompanyContactDet- To get company conatct details
 *@constructor
 * @param {number} companyId - unique id of company
 * @param {number} contactId - unique id for each and every contact
 * @param {function} callBack - deals with response
 */
  CONTACT.getCompanyContactDet = function (companyId,contactId, callBack) {
    var inputObj={};
    var companyContactModel=server.models.CompanyContact;
    var companyContactsDet = require("../../ServicesImpl/CommonImpl/getEntityDetails.js").entityDetailsUsingIncludeFilter;
    if (companyId && contactId ) {
      inputObj['companyId']=companyId;
      inputObj['contactId']=contactId;
      // below function will give contact details for an entity based on loopback include filter
      companyContactsDet(inputObj,companyContactModel,"companyContact",callBack);
    } else if (companyId) {
      inputObj['companyId']=companyId;
      companyContactsDet(inputObj,companyContactModel,"companyContact",callBack);
    }
    else {
      errorResponse(callBack);
    }
  };
  // remote method declaration to get company contact details
  CONTACT.remoteMethod('getCompanyContactDet', {
    description: "To get company contact details",
    returns: {
      type: 'array',
      root: true
    },
    accepts: [{arg: 'companyId', type: 'number', required: true, http: {source: 'query'}},
      {arg: 'contactId', type: 'number', http: {source: 'query'}}
    ],
    http: {
      path: '/getCompanyContactDet',
      verb: 'get'
    }
  });
  // remote method definition to delete campus contact details
  /**
 *deleteCampusContact- To delete campus contact detals by taking required fields
 *@constructor
 * @param {number} campusId - unique id of campus
 * @param {number} contactId - unique id for each and every contact
 * @param {function} callBack - deals with response
 */
  CONTACT.deleteCampusContact = function (campusId,contactId, callBack) {
    if (campusId && contactId ) {
      var campusContactModel=server.models.CampusContact;
      var contactModel=server.models.Contact;
      var delCampusContact = require("../../ServicesImpl/CommonImpl/deleteEntityRecord.js").deleteEntityRecordExceptPrimary;
      var primaryCheckInput={};
      primaryCheckInput['contactId']=contactId;
      var inputFilterObject={};
      inputFilterObject['campusId']=campusId;
      inputFilterObject['contactId']=contactId;
      delCampusContact(primaryCheckInput, inputFilterObject,contactModel ,campusContactModel, callBack);
    } else {
      errorResponse(callBack);
    }
  };
  // remote method declaration to delete campus contact details
  CONTACT.remoteMethod('deleteCampusContact', {
    description: "To delete campus contact",
    returns: {
      type: 'object',
      root: true
    },
    accepts: [{arg: 'campusId', type: 'number', required: true, http: {source: 'query'}},
      {arg: 'contactId', type: 'number',required:true, http: {source: 'query'}}
    ],
    http: {
      path: '/deleteCampusContact',
      verb: 'delete'
    }
  });
  // remote method definition to delete company contact details
  /**
 *deleteCampusContact- To delete campus contact detals by taking required fields
 *@constructor
 * @param {number} companyId - unique id of company
 * @param {number} contactId - unique id for each and every contact
 * @param {function} callBack - deals with response
 */
  CONTACT.deleteCompanyContact = function (companyId,contactId, callBack) {
    if (companyId && contactId ) {
      var companyContactModel=server.models.CompanyContact;
      var contactModel=server.models.Contact;
      var delCompanyContact = require("../../ServicesImpl/CommonImpl/deleteEntityRecord.js").deleteEntityRecordExceptPrimary;
      var primaryCheckInput={};
      primaryCheckInput['contactId']=contactId;
      var inputFilterObject={};
      inputFilterObject['companyId']=companyId;
      inputFilterObject['contactId']=contactId;
      delCompanyContact(primaryCheckInput, inputFilterObject,contactModel ,companyContactModel, callBack);
    }
    else {
      errorResponse(callBack);
    }
  };
  // remote method declaration to delete company contact details
  CONTACT.remoteMethod('deleteCompanyContact', {
    description: "To delete company contact",
    returns: {
      type: 'object',
      root: true
    },
    accepts: [{arg: 'companyId', type: 'number', required: true, http: {source: 'query'}},
      {arg: 'contactId', type: 'number',required:true, http: {source: 'query'}}
    ],
    http: {
      path: '/deleteCompanyContact',
      verb: 'delete'
    }
  });
// function which is used to delete campus/company contact data
/**
 *deleteContactData- To delete contact Data
 *@constructor
 * @param {number} contactId - unique id for each and every contact
 * @param {function} callBack - deals with response
 */
var deleteContactData=function (contactId,callBack) {
    CONTACT.destroyAll({"contactId":contactId},function(error,contactRes){
      if(error){
        error.requestStatus = false;
        callBack(error, null);
      }else if(contactRes && contactRes.count>=1){
        var response = {};
        response.data = contactRes;
        response.requestStatus = true;
        callBack(null, response);
      }else{
        errorResponse(callBack);
      }
    });
  };
// function which is used to get campus contact details
/**
 *getCampusContactDetails- To get campus contact details by taking required fields
 *@constructor
 * @param {object} inputObj - contains all the required data for searching and getting
 * @param {function} callBack - deals with response
 */
  var getCampusContactDetails=function (inputObj,callBc) {
    var CampusContact=server.models.CampusContact;
    CampusContact.find({"where":{"and":[inputObj]},"include":["campusContact"]}
      , function (error, campusContactRes) {
        if (error) {
          error.requestStatus = false;
          callBc(error, null);
        }
        else if (campusContactRes.length>0) {
          var response = {};
          response.data = campusContactRes;
          response.requestStatus = true;
          callBc(null, response);
        }
        else if (campusContactRes.length==0) {
          var response = {};
          response.data = campusContactRes;
          response.requestStatus = false;
          callBc(null, response);
        }else {
          errorResponse(callBc);
        }
      });
  }
  // function which is used to get company contact details
/**
 *getCampusContactDetails- To get campus contact details by taking required fields
 *@constructor
 * @param {object} inputObj - contains all the required data for searching and getting
 * @param {function} callBack - deals with response
 */  
  var getCompanyContactDetails=function (inputObj,callBc) {
    var companyContact=server.models.CompanyContact;
    companyContact.find({"where":{"and":[inputObj]},"include":["companyContact"]}
      , function (error, companyContactRes) {
        if (error) {
          error.requestStatus = false;
          callBc(error, null);
        }
        else if (companyContactRes.length>0) {
          var response = {};
          response.data = companyContactRes;
          response.requestStatus = true;
          callBc(null, response);
        }
        else if (companyContactRes.length==0) {
          var response = {};
          response.data = companyContactRes;
          response.requestStatus = false;
          callBc(null, response);
        }else {
          errorResponse(callBc);
        }
      });
  }

  var Joi = require('joi');
 //UpdateCampusContact remote method starts here
 /**
 *UpdateCampusContact- To update Campus Conatct Details
 *@constructor
 * @param {object} contactData - contains all the data need to get updated
 * @param {function} callBack - deals with response
 */
CONTACT.UpdateCampusContact = function (contactData,cb) {
  var campusUpdate = require("../../update/campus-contact-update.js");
campusUpdate.updateCampusContact(contactData,function(err,resp){
    if(err){
      cb(err,resp);
    }else{
      cb(null,resp);
    }
  });
}

//UpdateCampusContact method to update both company and campus contacts
 CONTACT.remoteMethod('UpdateCampusContact', {
   description: "Send Valid Data ",
   returns: {
     arg: 'data',
     type: "object"
   },
   accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
   http: {
     path: '/UpdateCampusContact',
     verb: 'PUT'
   }
 });

 //UpdateCompanyContact remote method starts here
  /**
 *UpdateCampusContact- To update Campus Conatct Details
 *@constructor
 * @param {object} contactData - contains all the data need to get updated
 * @param {function} callBack - deals with response
 */
CONTACT.UpdateCompanyContact = function (contactData,cb) {
  var companyUpdate = require("../../update/company-contact-update.js");
companyUpdate.updateCompanyContact(contactData,function(err,resp){
    if(err){
      cb(err,resp);
    }else{
      cb(null,resp);
    }
  });
}

 //UpdateCompanyContact method to update both company and campus contacts
  CONTACT.remoteMethod('UpdateCompanyContact', {
    description: "Send Valid Data ",
    returns: {
      arg: 'data',
      type: "object"
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/UpdateCompanyContact',
      verb: 'PUT'
    }
  });
// };
//UpdateCompanyContact remote method starts here
 /**
 *UpdateDepartmentContact- To update Campus Department Conatct Details
 *@constructor
 * @param {object} contactData - contains all the data need to get updated
 * @param {function} callBack - deals with response
 */
CONTACT.UpdateDepartmentContact = function (contactData,cb) {
 var departmentContactUpdate = require("../../update/department-contact-update.js");
departmentContactUpdate.updateDepartmentContact(contactData,function(err,resp){
   if(err){
     cb(err,resp);
   }else{
     cb(null,resp);
   }
 });
}
//UpdateDepartmentContact method to update both company and campus contacts
 CONTACT.remoteMethod('UpdateDepartmentContact', {
   description: "Send Valid Data ",
   returns: {
     arg: 'data',
     type: "object"
   },
   accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
   http: {
     path: '/UpdateDepartmentContact',
     verb: 'PUT'
   }
 });

 //UpdateOrganizationContact remote method starts here
  /**
 *UpdateOrganizationContact- To Organization Conatct Details
 *@constructor
 * @param {object} contactData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
 CONTACT.UpdateOrganizationContact = function (contactData,cb) {
  var orgContactUpdate = require("../../update/organization-contact-update.js");
 orgContactUpdate.updateOrgContact(contactData,function(err,resp){
    if(err){
      cb(err,resp);
    }else{
      cb(null,resp);
    }
  });
 }

 //UpdateOrganizationContact method to update organization contact
  CONTACT.remoteMethod('UpdateOrganizationContact', {
    description: "Send Valid Data ",
    returns: {
      arg: 'data',
      type: "object"
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/UpdateOrganizationContact',
      verb: 'PUT'
    }
  });

  //UpdateOrganizationContact remote method starts here
   /**
 *updateContactService- To update Service in contact
 *@constructor
 * @param {object} contactData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  CONTACT.updateContactService = function (contactData,cb) {
   var contactUpdate = require("../../commonValidation/update-service-contact.js");
  contactUpdate.updateContactService(contactData,function(err,resp){
     if(err){
       cb(err,resp);
     }else{
       cb(null,resp);
     }
   });
  }

  //UpdateOrganizationContact method to update organization contact
   CONTACT.remoteMethod('updateContactService', {
     description: "Send Valid Data ",
     returns: {
       arg: 'data',
       type: "object"
     },
     accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
     http: {
       path: '/updateContactService',
       verb: 'PUT'
     }
   });
};
