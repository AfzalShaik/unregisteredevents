'use strict';
var server = require('../../server/server');
var campusDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;


module.exports = function(CAMPUS_PLACEMENT_AGGREGATES_VW) {
  CAMPUS_PLACEMENT_AGGREGATES_VW.searchCampus = function(campusId, universityId, regionFlag, stateCode, cityId, callBc) {
    if (campusId && !universityId) {
      var campusPlacementAggregatesVw = server.models.CampusPlacementAggregatesVw;
      var campusObj = {};
      campusObj['campusId'] = campusId;
      campusDetails(campusObj, campusPlacementAggregatesVw, callBc);
    } else if (!campusId && universityId) {
      var campusPlacementAggregatesVw = server.models.CampusPlacementAggregatesVw;
      var campusObj = {};
      campusObj['universityId'] = universityId;
      campusDetails(campusObj, campusPlacementAggregatesVw, callBc);
    } else if (campusId && universityId) {
      var campusPlacementAggregatesVw = server.models.CampusPlacementAggregatesVw;
      var campusObj = {};
      campusObj['universityId'] = universityId;
      campusObj['campusId'] = campusId;
      campusDetails(campusObj, campusPlacementAggregatesVw, callBc);
    }
  };
  // remote method declaration to get campusDetails details
  CAMPUS_PLACEMENT_AGGREGATES_VW.remoteMethod('searchCampus', {
    description: 'To get campus details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'universityId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'regionFlag',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'stateCode',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'cityId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/searchCampus',
      verb: 'get',
    },
  });
};
