'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var companyDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var async = require('async');
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
module.exports = function(COMPANY_SEARCH_VW) {
  function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
  // remote method definition to get companyDetails item details
  /**
   *searchCompany- To search campus based on given parameters
   *@constructor
   * @param {number} companyId - unique id of campus
   * @param {number} companySizeValueId - unique id of university
   * @param {number} regionFlag - contains region flag
   * @param {number} stateCode - contains state cose
   * @param {number} cityId - contains state id or a pin code
   * @param {function} callBc - deals with response
   */
  COMPANY_SEARCH_VW.searchCompany = function(companyId, searchName, companySizeValueId, companyTypeValueId, industryTypeValueId, stateCode, cityId, internshipInd, regionFlag, campusId, companyStatusValueId, callBc) {
    if (companyId || searchName || companySizeValueId || companyTypeValueId || industryTypeValueId || stateCode || cityId ||
      internshipInd || regionFlag || companyStatusValueId) {
      var companyObj = {};
      companyObj['companyId'] = (companyId) ? companyId : undefined;
      companyObj['companySizeValueId'] = (companySizeValueId) ? companySizeValueId : undefined;
      companyObj['companyTypeValueId'] = (companyTypeValueId) ? companyTypeValueId : undefined;
      companyObj['industryTypeValueId'] = (industryTypeValueId) ? industryTypeValueId : undefined;
      companyObj['cityId'] = (cityId) ? cityId : undefined;
      companyObj['stateCode'] = (stateCode) ? stateCode : undefined;
      companyObj['regionFlag'] = (regionFlag) ? regionFlag : undefined;
      companyObj['internshipInd'] = (internshipInd) ? internshipInd : undefined;
      companyObj['companyStatusValueId'] = (companyStatusValueId) ? companyStatusValueId : undefined;
      var date = new Date().getFullYear();
      // companyObj['calenderYear'] = date;
      // console.log(companyObj.companyStatus);
      // getCompanyStatusId(companyObj.companyStatus, function(err1, res1) {
      //   if (err1) {
      //     errorFunction(err1, callBc);
      //   } else {
      // companyObj.companyStatus = companyStatus;
          // console.log(companyObj);
      companyDetails(companyObj, COMPANY_SEARCH_VW, function(err, companySearch) {
            // console.log(err, companySearch);
        if (err) {
              errorFunction(err, callBc);
            } else if (companySearch.data.length > 0) {
              if (searchName) {
                searchCompanyName(searchName, companySearch.data, function(error, searchedData) {
                  callBc(null, searchedData);
                });
              } else {
                async.map(companySearch.data, companyDriveRecruitment, function(error, placementInfo) {
                  async.map(placementInfo, getFinalDetails, function(err2, res2) {
                    callBc(null, res2);
                  });
                });
              }
            } else {
              errorFunction('Records not found', callBc);
            }
      });
        // }
      // });
    } else {
      errorFunction('No Data Found', callBc);
    }

    function companyDriveRecruitment(obj, callBc) {
      var employerDrive = server.models.EmployerDrive;
      var lookup = require('../../commonValidation/lookupMethods').getLookupId;
      lookup('EMPLOYER_DRIVE_TYPE_CODE', 'Hiring', function(driveType) {
        lookup('EMPLOYER_DRIVE_STATUS_CODE', 'Initial', function(driveStatus) {
          var companyObj = {};
          companyObj['companyId'] = obj.companyId;
          companyObj['driveTypeValueId'] = driveType.lookupValueId;
          companyObj['driveStatusValueId'] = driveStatus.lookupValueId;
          employerDrive.find({
            'where': {
              'and': [companyObj],
            },
          }, function(err, companyDetails) {
            if (err) {
              errorFunction(err, callBc);
            } else {
              obj.activeRecruitmentDrive = (companyDetails.length > 0) ? 'Y' : 'N';
              companyInternshipRecruitment(obj, function(internErr, internResponse) {
                obj.activeIntershipDrive = internResponse;
                if (campusId) {
                  visitedCompany(campusId, obj, function(err, visitedOurCampusInPast) {
                    obj.visitedOurCampusInPast = visitedOurCampusInPast;
                    callBc(null, obj);
                  });
                } else {
                  callBc(null, obj);
                }
              });
            }
          });
        });
      });
    }

    function companyInternshipRecruitment(obj, callBc) {
      var employerDrive = server.models.EmployerDrive;
      var lookup = require('../../commonValidation/lookupMethods').getLookupId;
      lookup('EMPLOYER_DRIVE_TYPE_CODE', 'Internship', function(driveType) {
        lookup('EMPLOYER_DRIVE_STATUS_CODE', 'Initial', function(driveStatus) {
          var companyObj = {};
          companyObj['companyId'] = obj.companyId;
          companyObj['driveTypeValueId'] = driveType.lookupValueId;
          companyObj['driveStatusValueId'] = driveStatus.lookupValueId;
          employerDrive.find({
            'where': {
              'and': [companyObj],
            },
          }, function(err, companyDetails) {
            if (err) {
              errorFunction(err, callBc);
            } else {
              var activeIntershipDrive = (companyDetails.length > 0) ? 'Y' : 'N';
              callBc(null, activeIntershipDrive);
            }
          });
        });
      });
    }

    function visitedCompany(campusId, obj, callBc) {
      var employerDriveCampuses = server.models.EmployerDriveCampuses;
      var companyObj = {};
      companyObj['campusId'] = (campusId) ? campusId : undefined;
      companyObj['companyId'] = obj.companyId;
      employerDriveCampuses.find({
        'where': {
          'and': [companyObj],
        },
      }, function(err, companyDetails) {
        if (err) {
          errorFunction(err, callBc);
        } else {
          var visitedOurCampusInPast = (companyDetails) ? 'Y' : 'N';
          callBc(null, visitedOurCampusInPast);
        }
      });
    }

    function searchCompanyName(searchName, myArray, callBc) {
      async.map(myArray, searchWithName, function(error, response) {
        var output = [];
        output = cleanArray(response);
        async.map(output, companyDriveRecruitment, function(error, placementInfo) {
          async.map(placementInfo, getFinalDetails, function(err2, res2) {
            callBc(null, res2);
          });
        });
      });

      function searchWithName(object, cb) {
        // console.log(object);
        searchName = searchName.toUpperCase();
        var searchShort = (object.searchShortName) ? object.searchShortName.indexOf(searchName) >= 0 : false;
        var search = (object.searchName) ? object.searchName.indexOf(searchName) >= 0 : false;
        var date = new Date().getFullYear();
        if ((search || searchShort)) {
          cb(null, object);
        } else {
          cb(null, null);
        }
      }
    }

    function getFinalDetails(object, cb2) {
      // console.log(object);
      var data = {
        name: object.name,
        shortName: object.shortName,
        companyId: object.companyId,
        cityId: object.cityId,
        stateCode: object.stateCode,
        regionFlag: object.regionFlag,
        avgSalary: object.avgSalary,
        cityName: object.cityName,
        companySizeValueId: object.companySizeValueId,
        companyTypeValueId: object.companyTypeValueId,
        industryTypeValueId: object.industryTypeValueId,
        totalNoOfOffers: object.totalNoOfOffers,
        internshipInd: object.internshipInd,
        companyType: object.companyType,
        companySize: object.companySize,
        industryType: object.industryType,
        searchName: object.searchName,
        searchShortName: object.searchShortName,
        companyStatus: object.companyStatus,
        companyStatusValueId: object.companyStatusValueId,
        activeRecruitmentDrive: object.activeRecruitmentDrive,
        activeIntershipDrive: object.activeIntershipDrive,
      };
      if (campusId) {
        data.visitedOurCampusInPast = object.visitedOurCampusInPast;
      }
      cb2(null, data);
    }

    function getCompanyStatusId(obj, cb1) {
      if (obj == undefined) {
        cb1(null, undefined);
      } else {
        var lookup = require('../../commonValidation/lookupMethods').lookupMethod;
        lookup('COMPANY_STATUS_CODE', function(err, status) {
          // console.log('this is status', status);
          var data = status.find(getDetails);
          cb1(null, data.lookupValue);
        });
      }
    }

    function getDetails(obj) {
      return obj.lookupValueId === companyStatusValueId;
    }
  };
  // remote method declaration to get companyDetails details
  COMPANY_SEARCH_VW.remoteMethod('searchCompany', {
    description: 'To get campus details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'companyId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'searchName',
      type: 'string',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companySizeValueId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyTypeValueId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'industryTypeValueId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'stateCode',
      type: 'string',
        //   required: true,
      http: {
          source: 'query',
        },
    }, {
        arg: 'cityId',
        type: 'number',
        //   required: true,
        http: {
          source: 'query',
        },
      }, {
        arg: 'internshipInd',
        type: 'string',
        //   required: true,
        http: {
          source: 'query',
        },
      }, {
        arg: 'regionFlag',
        type: 'string',
        //   required: true,
        http: {
          source: 'query',
        },
      },
    {
      arg: 'campusId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyStatusValueId',
      type: 'number',
      // required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/searchCompany',
      verb: 'get',
    },
  });
};
