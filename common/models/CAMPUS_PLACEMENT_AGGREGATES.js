'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(CAMPUS_PLACEMENT_AGGREGATES_WORK) {
  //mass upload remote method starts here
  var async = require('async');
  CAMPUS_PLACEMENT_AGGREGATES_WORK.campusPlacementAggregateUpload = function(test, cb) {
    var inputFile = './commonValidation/campus_placement_aggregates.csv';
    loadAggregateData(inputFile, function(err, fileResponse) {
      if (err) {
        cb(err, null);
      } else {
        async.map(fileResponse, getInfo, function(e, r) {
          if (e) {
            cb(e.errors, null);
          } else {
            var output = [];
            output = cleanArray(r);
            var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
            campusPlacementAggregatesWork.create(output, function(massError, testData) {
              if (massError) {
                cb(massError, null);
              } else {
                campusPlacementAggregatesWork.find({
                  'where': {
                    'error': 'false',
                  },
                }, function(error, campusOut) {
                  if (error) {
                    cb(error, null);
                  } else {
                    var createTest = require('../../commonValidation/create-campus-aggregate.js');
                    createTest.createCampusAggregate(campusOut, function(err, successCampus) {
                      if (err) {
                        cb(err, null);
                      } else {
                        var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
                        campusPlacementAggregatesWork.find({
                          'where': {
                            'error': 'true',
                          },
                        }, function(error, failureCampus) {
                          campusPlacementAggregatesWork.destroyAll({}, function(error, destroy) {
                            var finalArray = {};
                            finalArray['successCampus'] = successCampus;
                            finalArray['failureCampus'] = failureCampus;
                            cb(err, finalArray);
                          });
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  };
  //event test upload method creation
  CAMPUS_PLACEMENT_AGGREGATES_WORK.remoteMethod('campusPlacementAggregateUpload', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/campusPlacementAggregateUpload',
      verb: 'POST',
    },
  });

  function loadAggregateData(inputFile, cb) {
    var fs = require('fs');
    var parse = require('csv-parse');
    // var async = require('async');
    var parser = parse({
      delimiter: ',',
      columns: true,
    }, function(err, data) {
      //   async.eachSeries(data, function(line, callBc) {  });
      if (err) {
        cb(err, null);
      } else {
        cb(null, data);
      }
    });

    fs.createReadStream(inputFile).pipe(parser);
  }

  function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
      if (actual[i]) {
        newArray.push(actual[i]);
      }
    }
    return newArray;
  }

  function getInfo(obj, callback) {
    var student = server.models.Student;
    student.find({
      'where': {
        'studentId': obj.studentId,
      },
    }, function(error, findResp) {
      if (error) {
        callback(error, null);
      } else if (findResp.length > 0) {
        obj['error'] = 'false';
        callback(null, obj);
      } else {
        obj['error'] = 'true';
        callback(null, obj);
      }
    });
  }
};
