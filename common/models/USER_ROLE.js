'use strict';
var server = require('../../server/server');
module.exports = function(USER_ROLE) {
  USER_ROLE.sendEmail = function(data, callBc) {
    var email = server.models.Email;
    email.send({
      to: data.emailId,
      from: 'afzal.iiit@gmail.com',
      subject: 'Contact Us',
      text: data.message,
      html: data.message,
    }, function(err, mail) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, data);
      }
    });
  };
  USER_ROLE.remoteMethod('sendEmail', {
    description: 'Send Email',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/sendEmail',
      verb: 'post',
    },
  });
  //........................
  var globalPassword;
  var nodemailer = require('nodemailer');
  var config = require('../../server/config.json');
  var path = require('path');
  var server = require('../../server/server');
  var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
  var lookup = require('../../commonValidation/lookupMethods').lookupMethod;
  var signUpService = require('../../commonValidation/signup-service.js').createSignUpService;

  USER_ROLE.hello = function(input, cb) {
    var user = server.models.ScoraUser;
    var randomstring = require('randomstring');
    globalPassword = randomstring.generate({
      length: 12,
      charset: 'alphanumeric',
    });
    input.createDatetime = new Date();
    input.updateDatetime = new Date();
    input.createUserId = 1;
    input.updateUserId = input.createUserId;
    input.password = globalPassword;
    console.log('inputttttttttttttttttttt ', input);
    user.create(input, function(err, reso) {
      var options = {
        type: 'email',
        to: reso.email,
        from: 'noreply@loopback.com',
        subject: 'Thanks for registering.',
        template: path.resolve(__dirname, '../../server/views/verify.ejs'),
        redirect: '/verified',
        user: user,
        html: 'Use this Password to reset: ' + globalPassword,
      };
      // console.log('options', options);
      reso.verify(options, function(err, response, next) {
        // console.log('789999999999999999999999');
        console.log(response);
        if (err) {
          //User.deleteById(user.id);
          return err;
        }
        console.log('..................................... ', reso.id, err);
        signUpService(reso, function(campusErr, CampusResponse) {
          console.log('/////////////');
          if (campusErr) {
            cb(campusErr, null);
          }
          cb(null, CampusResponse);
        });
      });
      // next();
    });
  };
  USER_ROLE.remoteMethod('hello', {
    description: 'Send Email',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/hello',
      verb: 'post',
    },
  });
};
