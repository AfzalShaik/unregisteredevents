'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var Joi = require('joi');
/**
 * deals with the operations of the StudentAdditionalInfos
 * @constructor
 * @param {function} STUDENT_ADDITIONAL_INFO-contais couple of functions
 */
module.exports = function(STUDENT_ADDITIONAL_INFO) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  // CreateOperation
  // var lookUp = require('../../commonValidation/lookup');
  /**
   * studentBeforeSave-function for create operation
   * @constructor
   * @param {object} ctx-contains all the data user inserted
   * @param {function} next-function used for skiping when not required
   */
  STUDENT_ADDITIONAL_INFO.observe('before save', function studentBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      var catcode = 'ATTACH_CAT_CODE';
      var catTypeObj = {};
      catTypeObj['lookupValue'] = ctx.instance['programName'];
      // calling function deals with catcode
      findLookUp(catTypeObj, catcode, function(err, resp) {
        if (err) {
          errorResponse(cb);
        } else {
          ctx.instance.updateUserId = ctx.instance.createUserId;
          ctx.instance.createDatetime = new Date();
          ctx.instance.updateDatetime = new Date();
          ctx.instance.catcode = resp[0].lookupTypeId;
          logger.info('cat code was find Creation Initiated'); // TODO: CODEREVIEW - VARA – 09SEP17 – Correct the message it is not clear
          next();
        }
      });
    } else {
      next();
    }
  }); // TODO: CODEREVIEW - VARA – 09SEP17 – Add space (empty line) after methods for readability
  /**
   * findLookUp-function which deals with finding the lookup code from the database
   * @constructor
   * @param {object} Value-contains object which value we need to find
   * @param {number} catcode1-contains cat code
   * @param {function} callback-callback function
   */
  var findLookUp = function(Value, catcode1, callback) {
    var LookupTry = server.models.LookupType;
    var LookupTy = server.models.LookupValue;
    // finds the Value in the lookupValue table in database
    LookupTy.find({
      'where': {
        lookupValue: Value['lookupValue'],
      },
    }, function(err, resp) {
      if (err) {} else {
        var type = resp[0].lookupTypeId;
        // Finds that the Id Matches with the id of lookUpValue
        LookupTry.find({
          'where': {
            lookupTypeId: type,
          },
        }, function(err, response) {
          if (err) { // TODO: CODEREVIEW - VARA – 09SEP17 -- Use logger error() method
          } else {
            if (response[0].lookupCode == catcode1) {
              callback(null, response);
            } else {
              callback(null, null);
            }
          }
        });
      }
    });
  };
  /**
   * getStudentAcademics
   *@constructor
   * @param {function} getStudentAcademics-for getting student STUDENTADDITIONALINFO
   * @param {number} -academicsId as a primary key for getting details from the database
   * @param {function} -callback function for getting response
   */
  STUDENT_ADDITIONAL_INFO.getStudentAcademics = function(academicsId, callBc) {
    var inputObj = {};
    var studentInfo = server.models.StudentAdditionalInfo;
    var findEntity = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    if (academicsId) {
      inputObj['academicsId'] = academicsId;
      findEntity(inputObj, studentInfo, function(err, res) {
        if (err) {
          errorResponse(err, callBc);
        } else {
          callBc(null, res);
        }
      });
    } else {
      errorResponse('result not found', callBc);
    }
  };
  /**
   * getstudentAcademics for getting details from database
   */
  STUDENT_ADDITIONAL_INFO.remoteMethod('getstudentAcademics', {
    description: 'To get student academics details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: {
      arg: 'academicsId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    http: {
      path: '/getstudentAcademics',
      verb: 'get',
    },
  });
  /**
   * updateStudentInfo - for updating info of stugents
   * *constructor
   * @param {object} academicsData-contains data which need to get updated
   * @param {function} cb-an function deals with response
   */
  // UPDATE details in Student Academics
  STUDENT_ADDITIONAL_INFO.updateStudentInfo = function(academicsData, cb) {
    var studentUpdate = require('../../update/student-additionalinfo-update.js');
    studentUpdate.updateAcademics(academicsData,
      function(err, resp) {
        if (err) {
          cb(err, resp);
        } else if (resp == 0) {
          errorResponse(cb);
        } else {
          cb(null, resp);
        }
      });
  };

  // update Student Academics remoteMethod
  STUDENT_ADDITIONAL_INFO.remoteMethod('updateStudentInfo', {
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    description: 'For updating Academics Record (Input requires must is academicsId)',
    returns: {
      arg: 'data',
      type: 'object',
    },
    http: {
      path: '/updateStudentInfo',
      verb: 'PUT',
    },
  });

  // subscribe and un subscribe method

  // update Student Academics remoteMethod
  STUDENT_ADDITIONAL_INFO.subscribeOrUnsubscribe = function(input, callBC) {
    var eventStudentList = server.models.EventStudentList;
    eventStudentList.findOne({
      'where': {
        'and': [{
          'studentId': input.studentId,
        }, {
          'campusEventId': input.campusEventId,
        }],
      },
    }, function(eventErr, eventOut) {
      if (eventOut) {
        var object = {};
        object = eventOut;
        object.studentSubscribeInd = input.studentSubscribeInd;
        eventOut.updateAttributes(object, function(updateErr, updateOut) {
          if (updateOut) {
            var notificationObj = {
              'userId': input.userId,
              'role': 'STUDENT',
              'campusId': input.campusId,
              'campusEventId': eventOut.campusEventId,
              'empEventId': updateOut.employerEventId,
              'notificationName': 9,
              'companyList': [{
                'companyId': updateOut.companyId,
              },
              ],
              'campusList': [{
                'campusId': input.campusId,
              },
              ],
            };
            // console.log(eventOut, updateOut);
            var notificationTemplate = server.models.NotificationMessageTemplates;
            var pushNotification = notificationTemplate.pushNotification;
            pushNotification(notificationObj, function(notificationErr, notificationOut) {
              updateOut.notification = 'Notification Sent';
              callBC(null, updateOut);
            });
          } else {
            callBC(null, null);
          }
        });
      } else {
        callBC(null, null);
      }
    });
  };

  STUDENT_ADDITIONAL_INFO.remoteMethod('subscribeOrUnsubscribe', {
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    description: 'For updating student status',
    returns: {
      arg: 'data',
      type: 'object',
    },
    http: {
      path: '/subscribeOrUnsubscribe',
      verb: 'PUT',
    },
  });
};
