'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
module.exports = function(EMPLOYER_CAMPUS_LIST_COMP_PKG) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  EMPLOYER_CAMPUS_LIST_COMP_PKG.observe('before save', function employerCampusListCompPkgBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.listCompPkgId == undefined) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      logger.info('EmployerCampusListCompPkg Creation Initiated');
      next();
    } else if (ctx.isNewInstance) {
      errorFunction('EmployerCampusListCompPkg listCompPkgId is system generated value', next);
    } else {
      next();
    }
  });

  // remote method definition to get employer campus list details
    /**
 *getEmployerCampusListCompPkg- To get Campus List Comp Pkg by taking required fields
 *@constructor
 * @param {number} listCompPkgId - Unique id for ecah and every pakage
 * @param {function} callBc - deals with response
 */
  EMPLOYER_CAMPUS_LIST_COMP_PKG.getEmployerCampusListCompPkg = function(listId, listCompPkgId, callBc) {
    var inputObj = {};
    var employerCampusListDtl = server.models.EmployerCampusListCompPkg;
    var employerCampusListCompPkgDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (listId || listCompPkgId) {
      inputObj['listId'] = listId;
      inputObj['listCompPkgId'] = (listCompPkgId) ? listCompPkgId : undefined;
      var includeModels = ['employerCampusListCompPkgCompany', 'employerCampusListCompPkgEmployerCampusListHdr',
        'employerCampusListCompPkgCompensationPkg',
      ];
      // below function will give details for an entity based on loopback include filter
      employerCampusListCompPkgDetails(inputObj, employerCampusListDtl, includeModels, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method definition to get employer campus list campus package details
  EMPLOYER_CAMPUS_LIST_COMP_PKG.remoteMethod('getEmployerCampusListCompPkg', {
    description: 'To  get employer campus list campus package details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'listId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'listCompPkgId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getEmployerCampusListCompPkg',
      verb: 'GET',
    },
  });
  //UpdateEmployerCampusListCompPkg remote method starts here
   /**
 *updateEmployerCampusListCompPkg- To update Employer Comp pakage Details
 *@constructor
 * @param {object} empCampusPkgData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  EMPLOYER_CAMPUS_LIST_COMP_PKG.updateEmployerCampusListCompPkg = function(empCampusPkgData, cb) {
    var compPkgUpdate = require('../../commonCompanyFiles/update-emp-campus-list-compPkg');
    compPkgUpdate.updateCompPkgService(empCampusPkgData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //UpdateEmployerCampusListCompPkg method creation
  EMPLOYER_CAMPUS_LIST_COMP_PKG.remoteMethod('updateEmployerCampusListCompPkg', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEmployerCampusListCompPkg',
      verb: 'PUT',
    },
  });
};
