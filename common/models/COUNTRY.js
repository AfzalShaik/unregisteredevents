'use strict';
var server = require('../../server/server');
var throwerror = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
module.exports = function(COUNTRY) {
  COUNTRY.getCountries = function(input, callBc) {
    COUNTRY.find({}, function(err, countries) {
      if (err) {
        throwerror(err, callBc);
      } else {
        callBc(null, countries);
      }
    });
  };
  COUNTRY.remoteMethod('getCountries', {
    description: 'Send Valid campusId ',
    returns: {
      root: 'true',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getCountries',
      verb: 'POST',
    },
  });
};
