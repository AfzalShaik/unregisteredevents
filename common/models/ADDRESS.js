//requiring server path so that we can use exported functions or methods when ever we need
'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
// exporting function to use it in another modules if requires
module.exports = function(ADDRESS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var Joi = require('joi');
  // when request comes to address model then we execute logic or change request object before saving into database
  ADDRESS.observe('before save', function addressBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });
  /*  // Once request saved/updated/deleted in database then we perform additional operations based on request completion
    ADDRESS.observe('after save', function addressAfterSave(ctx, next) {
      if (ctx.instance) {
        if( ctx.instance.campusId && !isNaN(ctx.instance.campusId) && ctx.instance.companyId==undefined && !ctx.instance.departmentId){
          var CampusAddress = server.models.CampusAddress;
          CampusAddress.create({
            "campusId": Number(ctx.instance.campusId),
            "addressId": Number(ctx.instance.addressId)
          },function(err,resp){
            console.log(" CampusAddress resp: "+resp);
            next();
          });
        }
        else if( ctx.instance.companyId && !isNaN(ctx.instance.companyId) && ctx.instance.campusId==undefined){
          var companyAddress = server.models.CompanyAddress;
          companyAddress.create({
            "companyId": Number(ctx.instance.companyId),
            "addressId": Number(ctx.instance.addressId)
          },function(err,resp){
            console.log("companyAddress resp: "+resp);
            next();
          });
        } else if( ctx.instance.departmentId && !isNaN(ctx.instance.departmentId) && ctx.instance.campusId && !isNaN(ctx.instance.campusId)){
          var departmentAddress = server.models.DepartmentAddress;

          departmentAddress.create({
            "campusId": Number(ctx.instance.campusId),
            "departmentId": Number(ctx.instance.departmentId),
            "addressId": Number(ctx.instance.addressId)
          },function(err,campDepartmentAddressRes){
            console.log("campDepartmentAddressRes: "+ JSON.stringify(campDepartmentAddressRes));
            next();
          });
        }
        else{
          // invalidInputError(next);
          next();
        }
      }else{
        next();
      }
    });*/

  // remote method definition to get company address details
/**
 *getCompanyAddress- To get company address details
 *@constructor
 * @param {number} companyId - Unique id of company
 * @param {number} addressId - unique id of address
 * @param {function} callBc - Deals with response
 */
  ADDRESS.getCompanyAddress = function(companyId, addressId, callBc) {
    var inputObj = {};
    var companyAddrs = server.models.CompanyAddress;
    var companyAddressDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').getCompleteAddress;
    if (companyId && addressId) {
      inputObj['companyId'] = companyId;
      inputObj['addressId'] = addressId;
      // below function will give address details for an entity based on loopback include filter
      companyAddressDet(inputObj, companyAddrs, 'companyAddress', callBc);
    } else if (companyId) {
      inputObj['companyId'] = companyId;
      // below function will give address details for an entity based on loopback include filter
      companyAddressDet(inputObj, companyAddrs, 'companyAddress', callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus address details
  ADDRESS.remoteMethod('getCompanyAddress', {
    description: 'To get company address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{arg: 'companyId',
               type: 'number',
               required: true,
               http: {source: 'query'}},
      {arg: 'addressId', type: 'number', http: {source: 'query'}},
    ],
    http: {
      path: '/getCompanyAddress',
      verb: 'get',
    },
  });

// remote method definition to delete company address details
/**
 *deleteCompanyAddress- To Delete company address details
 *@constructor
 * @param {number} companyId - Unique id of comapny
 * @param {number} addressId - unique id of address
 * @param {function} callBc - deals with response
 */
  ADDRESS.deleteCompanyAddress = function(companyId, addressId, callBc) {
    if (companyId && addressId) {
      var companyAddressModel = server.models.CompanyAddress;
      var addressModel = server.models.Address;
      var deleteCompanyAddressDet = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['addressId'] = addressId;
      var inputFilterobject = {};
      inputFilterobject['companyId'] = companyId;
      inputFilterobject['addressId'] = addressId;
      deleteCompanyAddressDet(primaryCheckInput, inputFilterobject,
                               addressModel, companyAddressModel, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to delete campus address details
  ADDRESS.remoteMethod('deleteCompanyAddress', {
    description: 'To delete company address',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{arg: 'companyId',
              type: 'number',
              required: true,
              http: {source: 'query'}},
      {arg: 'addressId',
      type: 'number',
      required: true,
      http: {source: 'query'}},
    ],
    http: {
      path: '/deleteCompanyAddress',
      verb: 'delete',
    },
  });

// Update campus Address remoteMethod starts here
/**
 *UpdateCampusAddress- To update canpus address details
 *@constructor
 * @param {object} addressData - contains all the data need to be updated
 * @param {function} cb - deals with response
 */
  ADDRESS.UpdateCampusAddress = function(addressData, cb) {
    var campusUpdate = require('../../update/campus-address-update.js');
    campusUpdate.updateCampus(addressData,
      function(err, resp) {
        if (err) {
          cb(err, resp);
        } else {
          cb(null, resp);
        }
      });
  };
//update department address remoteMethod
  ADDRESS.remoteMethod('UpdateCampusAddress', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/UpdateCampusAddress',
      verb: 'PUT',
    },
  });
  //update department address remoteMethod
  /**
 *UpdateDepartmentAddress- To update department address details
 *@constructor
 * @param {object} deptData - contains all the data need to be updated
 * @param {function} cb - deals with response
 */
  ADDRESS.UpdateDepartmentAddress = function(deptData, cb) {
    var deptUpdate = require('../../update/department-address-update.js');
    deptUpdate.DepartmentUpdate(deptData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

    //update department address remoteMethod
  ADDRESS.remoteMethod('UpdateDepartmentAddress', {
    description: 'Send Valid Data ',
    returns: {
      			         arg: 'data',
      			         type: 'object',
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      			         path: '/UpdateDepartmentAddress',
      			         verb: 'PUT',
    },
  });

        //update company address remoteMethod
        /**
 *UpdateCompanyAddress- To update company address details
 *@constructor
 * @param {object} companyData - contains all the data need to be updated
 * @param {function} cb - deals with response
 */
  ADDRESS.UpdateCompanyAddress = function(companyData, cb) {
    var companyUpdate = require('../../update/company-address-update.js');
    companyUpdate.updateCompany(companyData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

        //update company address remoteMethod
  ADDRESS.remoteMethod('UpdateCompanyAddress', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/UpdateCompanyAddress', // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Use camel casing for methods and URL paths.
      verb: 'PUT',
    },
  });

  // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Use camel casing for methods and URL paths everywhere.
  // This should have been /updateOrganizationAddress
            //  UpdateOrganizationAddress remoteMethod
/**
 *UpdateOrganizationAddress- To update Organization address details
 *@constructor
 * @param {object} orgData - contains all the data need to be updated
 * @param {function} cb - deals with response
 */
  ADDRESS.UpdateOrganizationAddress = function(orgData, cb) {
    var orgUpdate = require('../../update/organization-address-update.js');
    orgUpdate.OrganizationAddressUpdate(orgData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

                    //update Organization address remoteMethod
  ADDRESS.remoteMethod('UpdateOrganizationAddress', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object', // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Fix this typo.
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/UpdateCompanyAddress',
      verb: 'PUT',
    },
  });

  //  UpdateOrganizationAddress remoteMethod
  ADDRESS.UpdateOrganizationAddress = function(orgData, cb) {
    var orgUpdate = require('../../update/organization-address-update.js');
    orgUpdate.OrganizationAddressUpdate(orgData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //update Organization address remoteMethod
  ADDRESS.remoteMethod('UpdateOrganizationAddress', {
    description: 'Send Valid Data ', // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Need better descriptions
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object', // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Typo
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/UpdateOrganizationAddress',
      verb: 'PUT',
    },
  });

  //update method to update individual address service
/**
 *updateAddress- To update address details
 *@constructor
 * @param {object} addressData - contains all the data need to be updated
 * @param {function} cb - deals with response
 */
  ADDRESS.updateAddress = function(addressData, cb) {
    var addressUpdate = require('../../commonValidation/update-service-address.js');
    addressUpdate.updateAddresService(addressData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };
            //update  address remoteMethod
  ADDRESS.remoteMethod('updateAddress', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object', // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Fix this typo.
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateAddress',
      verb: 'PUT',
    },
  });
};
