'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);
describe(' Drive Create Accpect etc ;', function() {
  /**
 Acpect event test Case
 */
  it('Drive Create returns 200', function(done) {
    var input = {
      'campusDriveId': 897979,
      'Action': 'Accepted',
      'createUserId': 2,
      'employerEventId': 75675,
    };

    var url = 'http://localhost:3000/api/CampusEvents/CampusEvent';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
/**
 Published event test Case
 */
  it('Drive Create returns 200', function(done) {
    var input = {
      'campusEventId': 271,
      'Action': 'Published',
    };

    var url = 'http://localhost:3000/api/CampusEvents/CampusEvent';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  /**
 Students shortlisted event test Case
 */
  it('Drive Create returns 200', function(done) {
    var input = {
      'campusEventId': 271,
      'Action': 'Students shortlisted',
    };

    var url = 'http://localhost:3000/api/CampusEvents/CampusEvent';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  /**
 Scheduled event test Case
 */
  it('Drive Create returns 200', function(done) {
    var input = {
      'campusEventId': 271,
      'Action': 'Scheduled',
      'scheduledDate': '2017*12-12',
      'scheduledStartTime': '11:45',
    };

    var url = 'http://localhost:3000/api/CampusEvents/CampusEvent';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  /**
 Closed event test Case
 */
  it('Drive Create returns 200', function(done) {
    var input = {
      'campusEventId': 271,
      'Action': 'Closed',
    };

    var url = 'http://localhost:3000/api/CampusEvents/CampusEvent';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
}

);
