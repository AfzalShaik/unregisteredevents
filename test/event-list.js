'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);
//Program
describe(' Eventlist ;', function() {
//program post method with valid data
  it('Event list POST returns 200', function(done) {
    var input = {
      'studentId': 3,
      'companyId': 1000,
      'employerEventId': 75675,
      'campusId': 113914986,
      'campusEventId': 1,
      'candidateStatusValueId': 1,
      'campusComments': 'string',
      'employerComments': 'string',
      'createDatetime': '2017-09-07T11:23:00.797Z',
      'updateDatetime': '2017-09-07T11:23:00.797Z',
      'createUserId': 1,
      'updateUserId': 1,
      'eligibilityInd': 'y',
      'studentSubscribeInd': 'y',
      'campusPublishInd': 'y',
      'offerStatusValueId': 1,
    };

    var url = 'http://localhost:3000/api/EventStudentLists';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
// trying to upbate by providing academicsId which was auto generated
  it('event list  POST returns 200', function(done) {
    var input = {
      'studentListId': 4,
      'studentId': 3,
      'companyId': 1000,
      'employerEventId': 75675,
      'campusId': 113914986,
      'campusEventId': 1,
      'candidateStatusValueId': 1,
      'campusComments': 'string',
      'employerComments': 'string',
      'createDatetime': '2017-09-07T11:23:00.797Z',
      'updateDatetime': '2017-09-07T11:23:00.797Z',
      'createUserId': 1,
      'updateUserId': 1,
      'eligibilityInd': 'y',
      'studentSubscribeInd': 'y',
      'campusPublishInd': 'y',
      'offerStatusValueId': 1,
    };

    var url = 'http://localhost:3000/api/EventStudentLists';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }

      expect(response.statusCode).to.equal(500);

      done();
    });
  });
// get method
  it('Event list  GET returns 200', function(done) {
    var input =
      {

        'studentListId': 2,
      };

    var url = 'http://localhost:3000/api/EventStudentLists';
    var request = require('request');

    request({
      url: url,
      method: 'GET',
      json: true,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
  //update method with valid data
  it('Event list  PUT returns 200', function(done) {
    var input =
      {
        'studentListId': 2,
        'campusComments': 'strg',
        'employerComments': 'string',
      };

    var url = 'http://localhost:3000/api/EventStudentLists/updateEventtDet';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
  // update method test with in valid academics id
  it('Event list  PUT returns 200', function(done) {
    var input =
      {
        'studentListId': 14,
        'campusComments': 'strg',
        'employerComments': 'string',
      };
    var url = 'http://localhost:3000/api/EventStudentLists/updateEventtDet';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(422);
      done();
    });
  });
  it('Event list  PUT returns 200', function(done) {
    var input =
      {
        'studentListId': 'pushpak',
        'campusComments': 'strg',
        'employerComments': 'string',
      };
    var url = 'http://localhost:3000/api/EventStudentLists/updateEventtDet';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(422);
      done();
    });
  });
}

);

