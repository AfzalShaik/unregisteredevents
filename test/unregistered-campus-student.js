'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);
describe(' Unregister Campus Student Upload ', function() {
  it('Event list POST returns 200', function(done) {
    var input =
      {
        'companyId': 1007,
        'createUserId': 1,
        'updateUserId': 1,
        'employerEventId': 75676,
      };
    var url = 'http://localhost:3000/api/UnregisterCampusStudents/unregisteredCampusStudentUpload';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input, employerEventId: 75675,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  it('Event list POST returns 200', function(done) {
    var input =
      {
        'companyId': 1007,
        'createUserId': 1,
        'updateUserId': 1,
        'employerEventId': 75676,
      };
    var url = 'http://localhost:3000/api/UnregisterCampusStudents/unregisteredCampusStudentUpload';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(400);

      done();
    });
  });
});
