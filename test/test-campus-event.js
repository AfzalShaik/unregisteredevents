'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);
//Program
describe(' Campus Event Cases ;', function() {
//program post method with valid data
  it('Campus Event POST returns 200', function(done) {
    var input = {
      'campusDriveId': 897979,
      'campusId': 113914986,
      'employerEventId': 75675,
      'companyId': 1007,
      'eventName': 'starboys',
      'eventTypeValueId': 1,
      'eventStatusValueId': 1,
      'scheduledDate': '2017-09-04T00:00:00.000Z',
      'scheduledStartTime': '10:45:00',
      'duration': 4,
      'createDatetime': '2017-09-04T10:18:11.000Z',
      'createUserId': 1,
      'updateDatetime': '2017-09-04T10:18:11.000Z',
      'updateUserId': 1,
    };

    var url = 'http://localhost:3000/api/CampusEvents';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  // trying to post with invalid campusdriveid
  it('Campus Event POST returns 200', function(done) {
    var input = {
      'campusDriveId': 89779,
      'campusId': 113914986,
      'employerEventId': 75675,
      'companyId': 1007,
      'eventName': 'starboys',
      'eventTypeValueId': 1,
      'eventStatusValueId': 1,
      'scheduledDate': '2017-09-04T00:00:00.000Z',
      'scheduledStartTime': '10:45:00',
      'duration': 4,
      'createDatetime': '2017-09-04T10:18:11.000Z',
      'createUserId': 1,
      'updateDatetime': '2017-09-04T10:18:11.000Z',
      'updateUserId': 1,
    };

    var url = 'http://localhost:3000/api/CampusEvents';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }

      expect(response.statusCode).to.equal(500);

      done();
    });
  });
// trying to post with in valid campusid
  it('Campus Event POST returns 200', function(done) {
    var input = {
      'campusDriveId': 897979,
      'campusId': 1114986,
      'employerEventId': 75675,
      'companyId': 1007,
      'eventName': 'starboys',
      'eventTypeValueId': 1,
      'eventStatusValueId': 1,
      'scheduledDate': '2017-09-04T00:00:00.000Z',
      'scheduledStartTime': '10:45:00',
      'duration': 4,
      'createDatetime': '2017-09-04T10:18:11.000Z',
      'createUserId': 1,
      'updateDatetime': '2017-09-04T10:18:11.000Z',
      'updateUserId': 1,
    };

    var url = 'http://localhost:3000/api/CampusEvents';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }

      expect(response.statusCode).to.equal(500);

      done();
    });
  });
// get method
  it('Campus Event GET returns 200', function(done) {
    var input =
      {

        'campusEventId': 9,
      };

    var url = 'http://localhost:3000/api/CampusEvents';
    var request = require('request');

    request({
      url: url,
      method: 'GET',
      json: true,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
  //update method with valid data
  it('Campus Event PUT returns 200', function(done) {
    var input =
      {
        'campusEventId': 1,
        'eventName': 'string',
        'scheduledDate': '2017-09-05T06:18:43.523Z',
        'scheduledStartTime': '10:30',
        'duration': 2,
      };

    var url = 'http://localhost:3000/api/CampusEvents/updateCampusEvent';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
// update method test with invalid campusEventId
  it('Campus EventPUT returns 200', function(done) {
    var input =
      {

        'campusEventId': 11,
        'eventName': 'string',
        'scheduledDate': '2017-09-05T06:18:43.523Z',
        'scheduledStartTime': '10:30',
        'duration': 2,

      };

    var url = 'http://localhost:3000/api/CampusEvents/updateCampusEvent';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(422);
      done();
    });
  });
}
);

