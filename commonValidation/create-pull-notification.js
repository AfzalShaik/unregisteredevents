'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var throwError = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var async = require('async');
var loginRole;
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;

function createPullNotification(input, callBack) {
  loginRole = input.role;
  var notificationDetails = server.models.NotificationDetails;
  notificationDetails.find({
    'where': {
      'and': [{
        'recipientId': input.userId,
      }, {
        'notificationDismissedInd': 'N',
      }],
    },
  }, function(err, output) {
    if (err) {
      throwError(err, callBack);
    } else if (input.role.toUpperCase() == 'STUDENT') {
      // console.log('outputtt', output);
      async.map(output, getStudentNotification, function(notificationError, notification) {
        // console.log('............................. ', notification, notificationError);
        if (notificationError) {
          throwError(notificationError, callBack);
        } else {
          callBack(null, notification);
        }
      });
    } else {
      async.map(output, getNotification, function(notificationError, notification) {
        if (notificationError) {
          throwError(notificationError, callBack);
        } else {
          // console.log('notificationArray::::: ', notification);
          // async.map(notification, getTemplate, function(templateErr, template) {
          //   if (templateErr) {
          //     throwError(templateErr, null);
          //   } else {
          //     var finalOutput = {};
          //     finalOutput.notificationDetails = output;
          //     finalOutput.notificationEvent = notification;
          //     finalOutput.notificationTemplate = template;
          var finalOut = [];
          finalOut = cleanArray(notification);
          callBack(null, finalOut);
          //   }
          // });
        }
      });
    }
  });
}

function getNotification(obj, callBC) {
  // console.log('objjjjjjjjjjjjjjjjjjjjjjjj ', obj);
  var inputObj = {};
  var notificationEvents = server.models.NotificationEvents;
  if (obj.notificationEventId != undefined || obj.notificationEventId != null) {
    inputObj['notificationEventId'] = obj.notificationEventId;
    notificationEvents.findOne({
      'where': {
        'and': [inputObj],
      },
    }, function(notificationError, notificationObj) {
      if (notificationError) {
        callBC(notificationError, null);
      } else {
        // console.log('......', obj, notificationObj);
        // callBC(null, notification);

        // function getTemplate(obj, callBC) {
        // console.log('objjjjjjjjjjjjjjjjjjjjjjj ', JSON.stringify(obj));
        var notificationMessageTemplates = server.models.NotificationMessageTemplates;
        var inputObj = {};
        inputObj['notificationTemplateId'] = notificationObj.notificationTemplateId;

        notificationMessageTemplates.findOne({
          'where': {
            'and': [inputObj],
          },
        }, function(templateError, template) {
          if (templateError) {
            callBC(templateError, null);
          } else {
            var employerEvent = server.models.EmployerEvent;
            var campusEvent = server.models.CampusEvent;
            var eventModel = (loginRole == 'PLCDIR') ? campusEvent : employerEvent;
            var empObj = {};
            var eduObj = {};
            var inputObject = {};
            empObj['empEventId'] = notificationObj.transactionId;
            eduObj['campusEventId'] = notificationObj.transactionId;
            inputObject = (loginRole == 'PLCDIR') ? empObj : empObj;
            
            // console.log('////////////////// ', inputObject);
            // console.log('......................................... ', notificationObj.transactionId);
            //Campus Event Get Details......//
            campusEvent.findOne({
              'where': {
                'campusEventId': notificationObj.transactionId,
              },
            }, function(campusEventErr, campusEvent) {
              var campusEventName = (campusEvent) ? campusEvent.eventName : null;
              employerEvent.findOne({
                'where': {
                  'empEventId': notificationObj.transactionId,
                },
              }, function(companyErr, companyEvent) {
                var companyEventName = (companyEvent) ? companyEvent.eventName : null;
                var campusEventModel = server.models.CampusEvent;
                campusEventModel.findOne({
                  'where': {
                    'employerEventId': notificationObj.transactionId,
                  },
                }, function(campusEventsErr, campussEvent) {
                  var campusEventNames = (campussEvent) ? campussEvent.eventName : null;
                  var campusEventNamee = (campusEventName) ? campusEventName : campusEventNames;
                  // });
                  // console.log('--------------------------------------- ',  campusEventName, companyEventName);
                  // eventModel.findOne({
                  //   'where': {
                  //     'and': [inputObject],
                  //   },
                  // }, function (eventErr, eventOutput) {
                  // if (eventErr) {
                  //   callBC(null, eventErr);
                  // } else {
                  // console.log('------------------------------ ', companyEvent);
                  if (companyEvent) {
                    var scheduleDate = (companyEvent.scheduledDate != null) ? companyEvent.scheduledDate : null;
                    var scheduledStartTime = (companyEvent.scheduledStartTime != null) ? companyEvent.scheduledStartTime : null;
                  }
                  var campus = server.models.Campus;
                  var company = server.models.Company;
                  var modelName = (loginRole == 'PLCDIR') ? company : campus;
                  var employerObj = {};
                  var educationObj = {};
                  var object = {};
                  // employerObj['companyId'] = obj.parentId;
                  employerObj.companyId = notificationObj.parentId;
                  educationObj.campusId = notificationObj.parentId;
                  object = (loginRole == 'PLCDIR') ? employerObj : educationObj;
                  // console.log(object);employerObj
                  if (notificationObj.roleTypeValueId == 28) {
                    object = educationObj;
                    modelName = campus;
                  }
                  modelName.findOne({
                    'where': object,
                  }, function(campusErr, campusOut) {
                    // console.log('----------------------------------- ', campusErr, campusOut);
                    if (campusErr) {
                      callBC(campusErr, null);
                    } else {
                      var notificationName = template.notificationName;
                      var notificationId = template.notificationTemplateId;
                      var receiverRoleCode = template.receiverRoleCode;
                      // var eventName = eventOutput.eventName;
                      var entityName = campusOut.name;
                      var validator = (loginRole === 'PLCDIR') ? 'campus' : 'employer';
                      getMessage(notificationId, receiverRoleCode, campusEventNamee, companyEventName, entityName, validator, scheduleDate, scheduledStartTime, function(messageErr, messageOut) {
                        if (messageErr) {
                          callBC(messageErr, null);
                        } else {
                          var flagVal;
                          var lookup = require('./lookupMethods').lookupMethod;
                          lookup('EMPLOYER_EVENT_STATUS_CODE', function(err, empResponse) {
                            var publishIndicator = empResponse.find(findPublish);

                            function findPublish(companyVal) {
                              return companyVal.lookupValue === 'Published';
                            }
                            // });
                            if (companyEvent) {
                              flagVal = (companyEvent.eventStatusValueId == publishIndicator.lookupValueId) ? true : false;
                            } else {
                              flagVal = false;
                            }
                            template.messageText = messageOut;
                            var finalOutput = {};
                            finalOutput.notificationDetails = obj;
                            finalOutput.notificationEvent = notificationObj;
                            finalOutput.notificationTemplate = template;
                            finalOutput.notificationTemplate.flag = flagVal;
                            var sender = {};
                            lookup('ROLE_TYPE_CODE', function(err, response) {
                              var employerIndicator = response.find(findCompany);
                              var campusIndicator = response.find(findCampus);
                              var studentIndicator = response.find(findStudent);
                              // console.log('responseeeeeeeeeeeeeeeeeeeeee ', response);
                              function findCompany(companyVal) {
                                return companyVal.lookupValue === 'Employer';
                              }

                              function findCampus(campusVal) {
                                return campusVal.lookupValue === 'Campus';
                              }

                              function findStudent(studentVal) {
                                return studentVal.lookupValue === 'Student';
                              }
                              if (notificationObj.roleTypeValueId == employerIndicator.lookupValueId) {
                                var company = server.models.Company;
                                company.findOne({
                                  'where': {
                                    'companyId': notificationObj.parentId,
                                  },
                                }, function(empErr, empSender) {
                                  finalOutput.senderDetails = empSender;
                                  callBC(null, finalOutput);
                                });
                              } else if (notificationObj.roleTypeValueId == campusIndicator.lookupValueId) {
                                var campus = server.models.Campus;
                                campus.findOne({
                                  'where': {
                                    'campusId': notificationObj.parentId,
                                  },
                                }, function(campusErr, campSender) {
                                  finalOutput.senderDetails = campSender;
                                  callBC(null, finalOutput);
                                });
                              } else {
                                var student = server.models.Student;
                                student.findOne({
                                  'where': {
                                    'id': notificationObj.senderId,
                                  },
                                }, function(studentErr, stSender) {
                                  finalOutput.senderDetails = stSender;
                                  callBC(null, finalOutput);
                                });
                              }
                              // console.log('000000000000000000000000 ', notificationObj.senderId, sender);
                              // callBC(null, finalOutput);
                            });
                          });
                        }
                      });
                    }
                  });
                });
                // }
                // });
              });
            });
          }
        });
        // }
      }
    });
  } else {
    callBC(null, null);
  }
}

function getMessage(notificationId, receiverRoleCode, campusEventName, companyEventName, entityName, validator, scheduleDate, scheduledStartTime, messageCB) {
  // console.log(notificationId);
  if (notificationId == 2) {
    var messageText = 'Your event' + ' ' + companyEventName + ' ' + 'has been rejected by' + ' ' + entityName;
    messageCB(null, messageText);
  } else if (notificationId == 3) {
    var messageText = 'Your event' + ' ' + companyEventName + ' ' + 'has been accepted by' + ' ' + entityName;
    messageCB(null, messageText);
  } else if (notificationId == 5) {
    var messageText = entityName + ' ' + 'has shared the student list for your event' + ' ' + companyEventName;
    messageCB(null, messageText);
  } else if (notificationId == 7) {
    var messageText = entityName + ' ' + 'has scheduled your event' + ' ' + companyEventName + ' ' + 'on' + ' ' + scheduleDate + ' at ' + scheduledStartTime;
    messageCB(null, messageText);
  } else if (notificationId == 10 && validator === 'employer') {
    var messageText = 'One or more students have unsubscribed from' + ' ' + companyEventName + '.' + ' ' + 'Please review the updated list.';
    messageCB(null, messageText);
  } else if (notificationId == 1) {
    var messageText = entityName + ' ' + 'is intereseted in conducting a campus drive at your campus. Please view ' + ' ' + companyEventName + ' and take appropriate action.';
    messageCB(null, messageText);
  } else if (notificationId == 6) {
    var messageText = entityName + ' ' + 'has shared the shortlisted students for your event' + ' ' + campusEventName;
    messageCB(null, messageText);
  } else if (notificationId == 9 && validator === 'campus') {
    // console.log('11111111111111111111111111111111111111111111111111111111111111111111');
    var messageText = 'One or more students have unsubscribed from' + ' ' + campusEventName + ' ' + 'Please review the updated list.';
    messageCB(null, messageText);
  } else if (notificationId == 15) {
    var messageText = 'Reminder: ' + ' ' + entityName + ' ' + 'has requested to share the stuent list for your event' + ' ' + campusEventName + '.';
    messageCB(null, messageText);
  } else if (notificationId == 16) {
    var messageText = 'Reminder: ' + ' ' + entityName + ' ' + 'has requested to schedule the event' + ' ' + campusEventName + '.';
    messageCB(null, messageText);
  } else if (notificationId == 17) {
    var messageText = 'Reminder: ' + ' ' + entityName + ' ' + 'has requested to share the shortlisted students for the event' + ' ' + companyEventName + '.';
    messageCB(null, messageText);
  } else if (notificationId == 20) {
    var messageText = 'Reminder: ' + ' ' + entityName + ' ' + 'is intereseted in conducting a campus drive at your campus. Please view ' + ' ' + companyEventName + ' and take appropriate action.';
    messageCB(null, messageText);
  } else if (notificationId == 1) {
    var messageText = entityName + ' ' + 'is intereseted in conducting a campus drive at your campus. Please view ' + ' ' + companyEventName + ' and take appropriate action.';
    messageCB(null, messageText);
  } else if (notificationId == 8) {
    var messageText = campusEventName + ' ' + 'for' + ' ' + entityName + ' ' + 'has been scheduled for' + ' ' + scheduleDate + ' at ' + scheduledStartTime;
    messageCB(null, messageText);
  } else if (notificationId == 18) {
    var messageText = entityName + ' ' + 'would like to connect with you. Please review' + ' ' + entityName + ' ' + 'profie.';
    messageCB(null, messageText);
  } else {
    messageCB(null, null);
  }
}

function getStudentNotification(obj, cb) {
  // console.log('objjjjjjjjjjjjjjj', obj);
  var inputObj = {};
  var notificationEvents = server.models.NotificationEvents;
  if (obj.notificationEventId != undefined || obj.notificationEventId != null) {
    inputObj['notificationEventId'] = obj.notificationEventId;
    notificationEvents.findOne({
      'where': {
        'and': [inputObj],
      },
    }, function(notificationError, notificationObj) {
      if (notificationError) {
        cb(err, null);
      } else {
        var notificationMessageTemplates = server.models.NotificationMessageTemplates;
        var inputObj = {};
        inputObj['notificationTemplateId'] = notificationObj.notificationTemplateId;
        notificationMessageTemplates.findOne({
          'where': {
            'and': [inputObj],
          },
        }, function(err, notificationMessageTemplateResponse) {
          if (err) {
            cb(err, null);
          } else {
            if (notificationMessageTemplateResponse.notificationTemplateId == 4 ||
              notificationMessageTemplateResponse.notificationTemplateId == (8 || 7) ||
              notificationMessageTemplateResponse.notificationTemplateId == 11 ||
              notificationMessageTemplateResponse.notificationTemplateId == 12) {
              var campusEvent = server.models.CampusEvent;
              campusEvent.findOne({'where': {'campusEventId': notificationObj.transactionId}},
            function(err, campusEventDetails) {
              if (err) {
                cb(err, null);
              } else {
                var empEvent = server.models.EmployerEvent;
                empEvent.findOne({'where': {'empEventId': campusEventDetails.employerEventId}},
                function(err, empEventDetails) {
                  if (err) {
                    cb(err, null);
                  } else {
                    var empDrive = server.models.EmployerDrive;
                    empDrive.findOne({'where': {'empDriveId': empEventDetails.empDriveId}},
                    function(err, driveDetails) {
                      if (err) {
                        cb(err, null);
                      } else {
                        var org = server.models.Organization;
                        org.findOne({'where': {'organizationId': driveDetails.organizationId}},
                      function(err, orgDetails) {
                        if (err) {
                          cb(err, null);
                        } else {
                          var company = server.models.Company;
                          company.findOne({'where': {'companyId': orgDetails.companyId}},
                          function(err, companyResponse) {
                            if (err) {
                              cb(err, null);
                            } else {
                              if (notificationMessageTemplateResponse.notificationTemplateId == 12) {
                                var company = server.models.Company;
                                company.findOne({'where': {'companyId': obj.parentId}},
                              function(err, senderResponse) {
                                if (err) {
                                  cb(err, null);
                                } else {
                                  createStudentNotification(obj, notificationObj,
                                    notificationMessageTemplateResponse, campusEventDetails,
                                    empEventDetails, driveDetails, orgDetails, companyResponse, senderResponse,
                                    function(err, notificationObject) {
                                      if (err) {
                                        cb(err, null);
                                      } else {
                                        // console.log('384444444444 ', notificationObject);
                                        cb(null, notificationObject);
                                      }
                                    });
                                }
                              });
                              } else {
                                var campus = server.models.Campus;
                                campus.findOne({'where': {'campusId': obj.parentId}},
                              function(err, senderResponse) {
                                if (err) {
                                  cb(err, null);
                                } else {
                                  createStudentNotification(obj, notificationObj,
                                    notificationMessageTemplateResponse, campusEventDetails,
                                    empEventDetails, driveDetails, orgDetails, companyResponse, senderResponse,
                                    function(err, notificationObject) {
                                      if (err) {
                                        cb(err, null);
                                      } else {
                                        // console.log(notificationObj);
                                        cb(null, notificationObject);
                                      }
                                    });
                                }
                              });
                              }
                            }
                          });
                        }
                      });
                      }
                    });
                  }
                });
              }
            });
            } else if (notificationMessageTemplateResponse.notificationTemplateId == 13 ||
              notificationMessageTemplateResponse.notificationTemplateId == 14) {
              var campEvent = server.models.CampusEvent;
              campEvent.findOne({'where': {'campusEventId': notificationObj.transactionId},
            'include': 'campusEventIbfk5rel'},
              function(err, eventDetails) {
                if (err) {
                  cb(err, null);
                } else {
                  var company = server.models.Company;
                  company.findOne({'where': {'companyId': eventDetails.companyId}}, function(err, companyData) {
                    if (err) {
                      cb(err, null);
                    } else {
                      createStudentNotification(obj, notificationObj,
                        notificationMessageTemplateResponse, null,
                        eventDetails, null, null, companyData, companyData,
                        function(err, notificationObject) {
                          if (err) {
                            cb(err, null);
                          } else {
                            cb(null, notificationObject);
                          }
                        });
                    }
                  });
                }
              });
            }
          }
        });
      }
    });
  }
}
function createStudentNotification(notificationDetails, notificationEvents,
  notificationMessageTemplates, campusEventData, employerEventData,
  empDriveData, orgDetails, companyData, senderData, cb) {
  if (notificationMessageTemplates.notificationTemplateId == 4) {
    var message = 'You are subscribed to the event ' + campusEventData.eventName + ' for Employer: ' + companyData.name + ', Organization: ' + orgDetails.name + '';
    var data = {};
    notificationMessageTemplates.messageText = message;
    data.notificationDetails = notificationDetails;
    data.notificationEvents = notificationEvents;
    data.notificationTemplate = notificationMessageTemplates;
    data.senderDetails = senderData;
    cb(null, data);
  }
  if (notificationMessageTemplates.notificationTemplateId == (8 || 7)) {
    var message = '' + campusEventData.eventName + ' for ' + companyData.name + ' has been scheduled for ' + campusEventData.scheduledDate + '';
    var data = {};
    notificationMessageTemplates.messageText = message;
    data.notificationDetails = notificationDetails;
    data.notificationEvents = notificationEvents;
    data.notificationTemplate = notificationMessageTemplates;
    data.senderDetails = senderData;
    cb(null, data);
  }
  if (notificationMessageTemplates.notificationTemplateId == 11) {
    var message = 'Your status on the event ' + campusEventData.eventName + ' has been changed by the placement director. Please check the event for detail.';
    var data = {};
    notificationMessageTemplates.messageText = message;
    data.notificationDetails = notificationDetails;
    data.notificationEvents = notificationEvents;
    data.notificationTemplate = notificationMessageTemplates;
    data.senderDetails = senderData;
    cb(null, data);
  }
  if (notificationMessageTemplates.notificationTemplateId == 12) {
    var message = 'Your status on the event ' + campusEventData.eventName + ' has been changed by the recruitment director. Please check the event for detail.';
    var data = {};
    notificationMessageTemplates.messageText = message;
    data.notificationDetails = notificationDetails;
    data.notificationEvents = notificationEvents;
    data.notificationTemplate = notificationMessageTemplates;
    data.senderDetails = senderData;
    cb(null, data);
  }
  if (notificationMessageTemplates.notificationTemplateId == 13) {
    var message = 'Congratulations! You have been offered by ' + companyData.name + '. Please review the offfer.';
    var data = {};
    notificationMessageTemplates.messageText = message;
    data.notificationDetails = notificationDetails;
    data.notificationEvents = notificationEvents;
    data.notificationTemplate = notificationMessageTemplates;
    data.senderDetails = senderData;
    cb(null, data);
  }
  if (notificationMessageTemplates.notificationTemplateId == 14) {
    var employerEvent = employerEventData.toJSON();
    var message = 'Congratulations! You have been elected by ' + companyData.name + ' for ' + employerEvent.campusEventIbfk5rel.lookupValue + '';
    var data = {};
    notificationMessageTemplates.messageText = message;
    data.notificationDetails = notificationDetails;
    data.notificationEvents = notificationEvents;
    data.notificationTemplate = notificationMessageTemplates;
    data.senderDetails = senderData;
    cb(null, data);
  }
}
exports.createPullNotification = createPullNotification;
