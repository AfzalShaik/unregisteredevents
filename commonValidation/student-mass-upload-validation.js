'use strict';
var Joi = require('joi');
var validVal;
  /**
   *validateJson-function deals with validating the correct json format
   *@constructor
   * @param {object} massuploadData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function validateJson(massuploadData, cb) {
  //validating input json fields
  var schema = Joi.object().keys({
    firstName: Joi.string().max(50).required(),
    lastName: Joi.string().max(50).required(),
    email: Joi.string().max(254).required(),
    admissionNo: Joi.string().max(50).required(),
    startDate: Joi.date().iso().required(),
    plannedCompletionDate: Joi.date().iso().required(),
    skipUserInd: [Joi.string().max(1), Joi.allow(null)],
    error: [Joi.string().max(500), Joi.allow(null)],
    rowNumber: Joi.number().integer().max(999999999999999),
    campusId: Joi.number().integer().max(999999999999999),
    programId: Joi.number().integer().max(999999999999999),
  });

  Joi.validate(massuploadData, schema, function(err, value) {
    if (err) {
      var error = new Error('');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err.details[0].message;
      validVal = false;

      cb(error, null);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateJson = validateJson;
