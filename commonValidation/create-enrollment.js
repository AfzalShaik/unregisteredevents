'use strict';
var server = require('../server/server');
var async = require('async');
/**
   *createEnrollmentService-function deals creating an enrollment service
   *@constructor
   * @param {object} stData - contains student id
   * @param {object} input - conatins a value that contains file name
   * @param {function} cb - deals with the response
   */
function createEnrollmentService(stData, input, cb) {
  async.map(stData, enrollmentMethod, function(e, r) {
    if (e) {
      cb(e.errors, null);
    } else {
      cb(null, r);
    }
  });
/*
enrollmentMethod - function called by asyns.map()
*/
  function enrollmentMethod(obj, callback) {
    var enrollmentModal = server.models.Enrollment;
    enrollmentInput(obj, stData, input, function(enrollmentData) {
      enrollmentModal.create(enrollmentData, function(err, enrollmentResp) {
        if (err) {
          callback(err, null);
        } else {
          callback(null, enrollmentResp);
        }
      });
    });
  }
}

function enrollmentInput(obj, stData, input, callBc) {
  var studentUserId = obj.userId;
  var massUser = server.models.ScoraUser;
  massUser.findOne({
    'where': {
      'id': studentUserId,
    },
  }, function(err, studentUser) {
    if (err) {
      callBc(err, null);
    } else {
      var massData = [];
      massData = input.find(findEmail);

      function findEmail(mass) {
        return mass.email === studentUser.email;
      }
      var lookup = require('./lookupMethods').lookupMethod;
      lookup('STUDENT_STATUS_CODE', function(err, response) {
        var studentStatusValueId = response.find(findStatus);

        function findStatus(statusVal) {
          return statusVal.lookupValue === 'Confirmed';
        }
        var enrollmentData = {
          'studentId': obj.studentId,
          'programId': massData.programId,
          'admissionNo': massData.admissionNo,
          'dataVerifiedInd': 'Y',
          'createDatetime': new Date(),
          'updateDatetime': new Date(),
          'createUserId': 1,
          'updateUserId': 1,
          'startDate': massData.startDate,
          'planedCompletionDate': massData.plannedCompletionDate,
        };
        callBc(enrollmentData);
      });
    }
  });
}
exports.createEnrollmentService = createEnrollmentService;
