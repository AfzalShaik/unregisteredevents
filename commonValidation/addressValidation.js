'use strict';
var Joi = require('joi');
var validVal;
  /**
   *validateJson-function deals with validating the correct json format
   *@constructor
   * @param {object} addressData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function validateJson(addressData, cb) {
  //validating input json fields
  var schema = Joi.object().keys({
    campusId: Joi.number().max(999999999999999),
    companyId: Joi.number().max(999999999999999),
    studentId: Joi.number().max(999999999999999),
    departmentId: Joi.number().max(999999999999999),
    organizationId: Joi.number().max(999999999999999),
    educationPersonId: Joi.number().max(999999999999999),
    employerPersonId: Joi.number().max(999999999999999),
    addressId: Joi.number().max(999999999999999).required(),
    addressTypeValueId: Joi.number().max(999999999999999),
    addressLine1: [Joi.string().max(100), Joi.allow(null)],
    addressLine2: [Joi.string().max(100), Joi.allow(null)],
    addressLine3: [Joi.string().max(100), Joi.allow(null)],
    postalId: [Joi.number().max(999999999999999), Joi.allow(null)],
    primaryInd: [Joi.string().max(1), Joi.allow(null)],
    stateCode: Joi.string().max(16),
    cityId: Joi.number().max(999999999999999),
    countryCode: Joi.string().max(16),
    createDatetime: Joi.date().iso(),
    updateDatetime: Joi.date().iso(),
    createUserId: Joi.number().max(999999999999999),
    updateUserId: Joi.number().max(999999999999999),
  });

  Joi.validate(addressData, schema, function(err, value) {
    if (err) {
      var error = new Error('Validation Error');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err;
      validVal = false;

      cb(error, validVal);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateJson = validateJson;
