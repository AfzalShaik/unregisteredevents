'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var updateContact = require('../commonValidation/updateContact');
var Contact = server.models.Contact;
var lookupMethods = require('./lookupMethods');
var contactValidation = require('./contactValidation');
/**
   *updateContactService-function deals with updating the contact service
   *@constructor
   * @param {object} contactData - contains the id need to get updated
   * @param {function} cb - deals with the response
   */
function updateContactService(contactData, cb) {
  if (contactData) {
    if (contactData.contactId) {
      //validating input json fields

      contactValidation.validateContactJson(contactData, function(err, validVal) {
        if (err) {
          cb(err, contactData);
        } else if (validVal == true) {
          var contactTypeVal;
          var contactTypeCode = 'CONTACT_TYPE_CODE';
          var contactTypeObj = {};
          contactTypeObj['lookupValueId'] = contactData.contactTypeValueId;
          lookupMethods.typeCodeFunction(contactTypeObj, contactTypeCode, function(typeCodeCheck) {
            contactTypeVal = typeCodeCheck;
            if (contactTypeVal == false) {
              var error = new Error('invalid contactTypeValueId');
              error.statusCode = 422;
              error.requestStatus = false;
              cb(error, contactData);
            } else {
              Contact.findOne({
                'where': {
                  'contactId': contactData.contactId,
                },
              }, function(err, contactList) {
                if (err) {
                  //throws error
                  cb(err, contactData);
                } else if (contactList) {
                  //updating record in contact table with contactId
                  updateContact.updateContact(contactData, function(error, info) {
                    if (error) {
                      cb(error, contactData);
                    } else {
                      cb(null, info);
                    }
                  });
                } else {
                  //throws error incase of invalid employerPersonId or companyId
                  var error = new Error('Invalid contactId ');
                  logger.error('Invalid contactId');
                  error.statusCode = 422;
                  error.requestStatus = false;
                  cb(error, contactData);
                }
              });
            }
          });
        } else {
          var error = new Error('You cannot perform update operation: ');
          error.statusCode = 422;
          error.requestStatus = false;
          cb(error, contactData);
        }
      });
    } else {
      var error = new Error('required contactId');
      error.statusCode = 422;
      error.requestStatus = false;
      cb(error, contactData);
    }
  } else {
    logger.error("Input can't be Null");
    var error = new Error('Input is Null: ');
    error.statusCode = 422;
    error.requestStatus = false;
    cb(error, contactData);
  }
}
exports.updateContactService = updateContactService;
