'use strict';
var Joi = require('joi');
var validVal;
 /**
   *validateCompanyJson-function deals with validating company json data
   *@constructor
   * @param {object} comanyData - contains all the data need to get valiadated
   * @param {function} cb - deals with the response
   */
function validateCompanyJson(companyData, cb) {
  //validating input json fields
  var schema = Joi.object().keys({
    companyId: Joi.number().integer().max(999999999999999).required(),
    name: Joi.string().max(100),
    description: [Joi.string().max(200), Joi.allow(null)],
    companySizeValueId: Joi.number().max(999999999999999),
    companyTypeValueId: Joi.number().max(999999999999999),
    industryTypeValueId: Joi.number().max(999999999999999),
    companyStatusValueId: Joi.number().max(999999999999999),
    logo: [Joi.string().max(200), Joi.allow(null)],
    websiteAddress: [Joi.string().max(200), Joi.allow(null)],
    objective: [Joi.string().max(2000), Joi.allow(null)],
    brandingImage: [Joi.string().max(200), Joi.allow(null)],
    missionStatement: [Joi.string().max(2000), Joi.allow(null)],
    shortName: [Joi.string().max(50), Joi.allow(null)],
    facebook: [Joi.string().max(200), Joi.allow(null)],
    twitter: [Joi.string().max(200), Joi.allow(null)],
    youtube: [Joi.string().max(200), Joi.allow(null)],
    linkedin: [Joi.string().max(200), Joi.allow(null)],
    internshipInd: Joi.string().max(1),
    createDatetime: Joi.date().iso(),
    updateDatetime: Joi.date().iso(),
    createUserId: Joi.number().max(999999999999999),
    updateUserId: Joi.number().max(999999999999999),
    searchName: [Joi.string().max(100), Joi.allow(null)],
    searchShortName: [Joi.string().max(50), Joi.allow(null)],
  });

  Joi.validate(companyData, schema, function(err, value) {
    if (err) {
      var error = new Error('Validation Error');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err;
      validVal = false;

      cb(error, validVal);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateCompanyJson = validateCompanyJson;
