'use strict';
var server = require('../server/server');
var async = require('async');

function createCompanyHiringService(testData, callBc) {
  async.map(testData, testMethod, function(err, resp) {
    if (err) {
      callBc(err.errors, null);
    } else {
      callBc(null, resp);
    }
  });

  function testMethod(obj, callback) {
    var companyHiringAggregatesModel = server.models.CompanyHiringAggregates;
    testInput(obj, testData, function(eventTestData) {
      companyHiringAggregatesModel.create(eventTestData, function(err, eventTestResp) {
        if (err) {
          callback(err, null);
        } else {
          callback(null, eventTestResp);
        }
      });
    });
  }
}

function testInput(obj, testData, callBc) {
  var organizationModel = server.models.Organization;
  organizationModel.findOne({'where': {'organizationId': obj.organizationId}}, function(error, findResp) {
    if (error) {
      callBc(error);
    } else {
      var companyHireData = {
        'companyId': findResp.companyId,
        'organizationId': obj.organizationId,
        'campusId': obj.campusId,
        'noOfOffers': obj.noOfOffers,
        'sumOfOffers': obj.sumOfOffers,
        'minimumOffer  ': obj.minimumOffer,
        'maximumOffer': obj.maximumOffer,
        'calendarYear': 2017,
        'jobRoleId': obj.jobRoleId,
      };
      callBc(companyHireData);
    }
  });
}
exports.createCompanyHiringService = createCompanyHiringService;
