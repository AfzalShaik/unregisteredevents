'use strict';
var server = require('../server/server');
//validating addressTypeValueId in addressTypeCodeFunction
// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Break down to smaller functions.
// Function name can follow the "validate*" pattern - Will help improve readability.
// Always use === and !== as it'll validate typeof too.
// Generic API to search/validate a Lookup Type.
  /**
   *addressTypeCodeFunction-function deals with finding lookup value and id
   *@constructor
   * @param {object} addressData - contains the data need to get search
   * @param {any} callbackAddressFunction - deals with the response
   */
function addressTypeCodeFunction(addressData, callbackAddressFunction) {
  var addressVal = false;
  if (addressData.addressTypeValueId) {
    var LookupValue = server.models.LookupValue;
    var LookupType = server.models.LookupType;
    LookupValue.find({
      'where': {
        'lookupValueId': addressData.addressTypeValueId,
      },
    }, function(err, lookupValueId) {
      if (err) {
        addressVal = false;

        callbackAddressFunction(addressVal);
      } else if (lookupValueId.length > 0) {
        var typeId = lookupValueId[0].lookupTypeId;
        LookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            addressVal = false;
            callbackAddressFunction(addressVal);
          } else if (lookupTypeId.length > 0) {
            var lookupCode = lookupTypeId[0].lookupCode;
            if (lookupCode == 'ADDRESS_TYPE_CODE') {
              addressVal = true;
              callbackAddressFunction(addressVal);
            } else {
              addressVal = false;
              callbackAddressFunction(addressVal);
            }
          } else {
            addressVal = false;

            callbackAddressFunction(addressVal);
          }
        });
      } else {
        addressVal = false;

        callbackAddressFunction(addressVal);
      }
    });
  } else {
    addressVal = true;
    callbackAddressFunction(addressVal);
  }
}

//addressFunction for validating cityId,countryId and StateID
  /**
   *addressFunction-for validating city state and id
   *@constructor
   * @param {object} addData - contains address data
   * @param {function} callbackAddFunction - deals with the response
   */
function addressFunction(addData, callbackAddFunction) {
  var addressValue = false;
  if (addData.cityId || addData.stateCode || addData.countryCode) {
    if (addData.cityId && addData.stateCode && addData.countryCode) {
      var City = server.models.City;
      var PostalCodeModel = server.models.PostalCode;

      City.findOne({
        'where': {
          'and': [{
            'cityId': addData.cityId,
          }, {
            'stateCode': addData.stateCode,
          }, {
            'countryCode': addData.countryCode,
          }],
        },
      }, function(err, CityID) {
        if (err) {
          addressValue = false;
          callbackAddFunction(addressValue);
        } else if (CityID) {
          addressValue = true;
          callbackAddFunction(addressValue);
        } else {
          addressValue = false;
          callbackAddFunction(addressValue);
        }
      });
    } else {
      addressValue = false;
      callbackAddFunction(addressValue);
    }
  } else {
    addressValue = true;
    callbackAddFunction(addressValue);
  }
}

//validating contactTypeValueId in contactTypeCodeFunction
  /**
   *contactTypeCodeFunction-function deals with finding the type value id
   *@constructor
   * @param {object} contactData - contains all the contact information need to get searched
   * @param {function} callbackContactFunction - deals with the response
   */
function contactTypeCodeFunction(contactData, callbackContactFunction) {
  var contactVal = false;
  if (contactData.contactTypeValueId) {
    var LookupValue = server.models.LookupValue;
    var LookupType = server.models.LookupType;
    LookupValue.find({
      'where': {
        'lookupValueId': contactData.contactTypeValueId,
      },
    }, function(err, lookupValueId) {
      if (err) {
        contactVal = false;

        callbackContactFunction(contactVal);
      } else if (lookupValueId.length > 0) {
        var typeId = lookupValueId[0].lookupTypeId;
        LookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            contactVal = false;
            callbackContactFunction(contactVal);
          } else if (lookupTypeId.length > 0) {
            var lookupCode = lookupTypeId[0].lookupCode;
            if (lookupCode == 'CONTACT_TYPE_CODE') {
              contactVal = true;
              callbackContactFunction(contactVal);
            } else {
              contactVal = false;
              callbackContactFunction(contactVal);
            }
          } else {
            contactVal = false;

            callbackContactFunction(contactVal);
          }
        });
      } else {
        contactVal = false;

        callbackContactFunction(contactVal);
      }
    });
  } else {
    contactVal = true;
    callbackContactFunction(contactVal);
  }
}

//validating companySizeValueId in companySizeFunction
 /**
   *companySizeFunction-function deals with finding the lookup value id
   *@constructor
   * @param {object} companySizeData - contains company size data for
   * @param {function} callbackSizeFunction - deals with the response
   */
function companySizeFunction(companySizeData, callbackSizeFunction) {
  var sizeVal = false;
  if (companySizeData.companySizeValueId) {
    var LookupValue = server.models.LookupValue;
    var LookupType = server.models.LookupType;
    LookupValue.find({
      'where': {
        'lookupValueId': companySizeData.companySizeValueId,
      },
    }, function(err, lookupValueId) {
      if (err) {
        sizeVal = false;

        callbackSizeFunction(sizeVal);
      } else if (lookupValueId.length > 0) {
        var typeId = lookupValueId[0].lookupTypeId;
        LookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            sizeVal = false;
            callbackSizeFunction(sizeVal);
          } else if (lookupTypeId.length > 0) {
            var lookupCode = lookupTypeId[0].lookupCode;
            if (lookupCode == 'COMPANY_SIZE_CODE') {
              sizeVal = true;
              callbackSizeFunction(sizeVal);
            } else {
              sizeVal = false;
              callbackSizeFunction(sizeVal);
            }
          } else {
            sizeVal = false;

            callbackSizeFunction(sizeVal);
          }
        });
      } else {
        sizeVal = false;

        callbackSizeFunction(sizeVal);
      }
    });
  } else {
    sizeVal = true;
    callbackSizeFunction(sizeVal);
  }
}

//validating companyTypeValueId in companyTypeFunction
 /**
   *companyTypeFunction-function deals with finding which type of company it was
   *@constructor
   * @param {object} companyTypeData - contains all the which need to get find in look up table
   * @param {function} callbackTypeFunction - deals with the response
   */
function companyTypeFunction(companyTypeData, callbackTypeFunction) {
  var typeVal = false;
  if (companyTypeData.companyTypeValueId) {
    var LookupValue = server.models.LookupValue;
    var LookupType = server.models.LookupType;
    LookupValue.find({
      'where': {
        'lookupValueId': companyTypeData.companyTypeValueId,
      },
    }, function(err, lookupValueId) {
      if (err) {
        typeVal = false;

        callbackTypeFunction(typeVal);
      } else if (lookupValueId.length > 0) {
        var typeId = lookupValueId[0].lookupTypeId;
        LookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            typeVal = false;
            callbackTypeFunction(typeVal);
          } else if (lookupTypeId.length > 0) {
            var lookupCode = lookupTypeId[0].lookupCode;
            if (lookupCode == 'COMPANY_TYPE_CODE') {
              typeVal = true;
              callbackTypeFunction(typeVal);
            } else {
              typeVal = false;
              callbackTypeFunction(typeVal);
            }
          } else {
            typeVal = false;

            callbackTypeFunction(typeVal);
          }
        });
      } else {
        typeVal = false;

        callbackTypeFunction(typeVal);
      }
    });
  } else {
    typeVal = true;
    callbackTypeFunction(typeVal);
  }
}

//validating industryTypeValueId in industryTypeFunction
 /**
   *industryTypeFunction-function deals with finding which type of industry it belongs to in lookup table
   *@constructor
   * @param {object} industryTypeData - contains all the data need to get find
   * @param {function} callbackIndFunction - deals with the response
   */
function industryTypeFunction(industryTypeData, callbackIndFunction) {
  var industryTypeVal = false;
  if (industryTypeData.industryTypeValueId) {
    var LookupValue = server.models.LookupValue;
    var LookupType = server.models.LookupType;
    LookupValue.find({
      'where': {
        'lookupValueId': industryTypeData.industryTypeValueId,
      },
    }, function(err, lookupValueId) {
      if (err) {
        industryTypeVal = false;

        callbackIndFunction(industryTypeVal);
      } else if (lookupValueId.length > 0) {
        var typeId = lookupValueId[0].lookupTypeId;
        LookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            industryTypeVal = false;
            callbackIndFunction(industryTypeVal);
          } else if (lookupTypeId.length > 0) {
            var lookupCode = lookupTypeId[0].lookupCode;
            if (lookupCode == 'INDUSTRY_TYPE_CODE') {
              industryTypeVal = true;
              callbackIndFunction(industryTypeVal);
            } else {
              industryTypeVal = false;
              callbackIndFunction(industryTypeVal);
            }
          } else {
            industryTypeVal = false;

            callbackIndFunction(industryTypeVal);
          }
        });
      } else {
        industryTypeVal = false;

        callbackIndFunction(industryTypeVal);
      }
    });
  } else {
    industryTypeVal = true;
    callbackIndFunction(industryTypeVal);
  }
}

//validating postalCode
 /**
   *postalCodeFunction-function deals with finding the postal code and validating it in the lookup table
   *@constructor
   * @param {object} postalData - contains all the data need to get found in lookuptable
   * @param {function} callbackPostalFunction - deals with the response
   */
function postalCodeFunction(postalData, callbackPostalFunction) {
  var postalVal = false;
  if (postalData.postalCode && postalData.countryCode && postalData.cityId) {
    var postalCode = server.models.PostalCode;
    postalCode.find({
      'where': {
        'and': [{
          'postalCode': postalData.postalCode,
        }, {
          'cityId': postalData.cityId,
        }, {
          'countryCode': postalData.countryCode,
        }],
      },
    }, function(err, postalCodeList) {
      if (err) {
        postalVal = false;
        callbackPostalFunction(postalVal);
      } else if (postalCodeList.length > 0) {
        postalVal = true;
        callbackPostalFunction(postalVal);
      } else {
        postalVal = false;

        callbackPostalFunction(postalVal);
      }
    });
  } else {
    postalVal = true;
    callbackPostalFunction(postalVal);
  }
}
exports.contactTypeCodeFunction = contactTypeCodeFunction;
exports.addressTypeCodeFunction = addressTypeCodeFunction;
exports.addressFunction = addressFunction;
exports.companySizeFunction = companySizeFunction;
exports.companyTypeFunction = companyTypeFunction;
exports.industryTypeFunction = industryTypeFunction;
exports.postalCodeFunction = postalCodeFunction;
