'use strict';
var server = require('../server/server');
var campusEvenetStudentSearch = server.models.CampusEventStudentSearchVw;
var skills = [];
var errorResponse = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var intrests = [];
var address = [];
var strength = [];
var coverPicture = {};
var picturesCode = [];
var pictures = {};
var total = 0;
var Contact = [];
function getProfile(studentDetails, cb) {
  console.log(studentDetails);
  var studentAdditionalInfo = server.models.StudentAdditionalInfo;
  studentAdditionalInfo.find({'where': {'studentId':
  studentDetails.data[0].studentId}},
  function(err, additionalInfo) {
    if (err) {
      errorResponse(err, cb);
    } else {
      getStudentContact(studentDetails.data[0].studentId, function(err, contactAndAddress) {
        if (err) {
          errorResponse(err, cb);
        } else {
          var studentAddributes = server.models.StudentAttributes;
          studentAddributes.find({'where': {'studentId': studentDetails.data[0].studentId}},
          function(err, res) {
            if (err) {
              errorResponse('errin addribures');
            } else {
              lookup('ATTACH_CAT_CODE', 'Profile Picture', function(err4, profileRes) {
                if (err4) {
                  errorResponse('errin profile');
                } else {
                  console.log('profile ressssssssss', profileRes);
                  picturesCode[0] = profileRes[0].lookupValueId;
                  lookup('ATTACH_CAT_CODE', 'Cover Photo', function(err5, coverRes) {
                    if (err5) {
                      errorResponse('errin photo');
                    } else {
                      picturesCode[1] = coverRes[0].lookupValueId;
                      lookup('ATTACH_TYPE_CODE', 'Picture', function(err6, bothRes) {
                        if (err6) {
                          errorResponse('errin pic');
                        } else {
                          picturesCode[2] = bothRes[0].lookupValueId;
                          var async = require('async');
                          async.map(res, getProfileAndCover, function(err, resFinal) {
                            if (err) {
                              errorResponse('errin ', cb);
                            } else {
                              var final = {};
                              final.StudentDetails = studentDetails;
                              final.StudentAcademics = additionalInfo;
                              final.AdditionalData = contactAndAddress;
                              final.profileDetails = pictures;
                              getTheStrength(final, function(err, FinalRes) {
                                if (err) {
                                  errorResponse('err', cb);
                                } else {
                                  getTotal(strength, function(err, finalResponse) {
                                    if (err) {
                                      errorResponse('an err', cb);
                                    } else {
                                      final.Strength = total * 2;
                                      var data = {};
                                      // console.log(final);
                                      data.Details = final.StudentDetails.data[0];
                                      data.Address = address[0];
                                      data.Contact = Contact[0];
                                      data.Strength = final.Strength;
                                      data.ProfilePIc = pictures;
                                      data.CoverPic = coverPicture;
                                      // console.log(data);
                                      total = 0;
                                      cb(null, data);
                                      pictures = [];
                                      coverPicture = [];
                                      Contact = [];
                                      address = [];
                                    }
                                  });
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}
function getProfileAndCover(ob, cb) {
  console.log(picturesCode);
  if ((ob.attachCatValueId == picturesCode[0]) &&
ob.attachTypeValueId == picturesCode[2]) {
    pictures = ob;
    cb(null, 'done');
  } else if (ob.attachCatValueId == picturesCode[1] && ob.attachTypeValueId == picturesCode[2]) {
    var studentObj = {};
    coverPicture = ob;
    cb(null, 'done');
  } else {
    cb(null, 'done');
  }
}
function getTotal(array, cb1) {
  console.log(array);
  for (var i = 0; i < array.length; i++) {
    total = total + array[i];
    if (i == array.length - 1) {
      cb1(null, 'done');
    }
  }
}
function getStudentContact(studentId, contactCb) {
  var studentContact = server.models.StudentContact;
  studentContact.findOne({'where': {'studentId': studentId,
  'primaryInd': 'Y'}, 'include': 'studentContactIbfk2rel'}, function(err1, res1) {
    if (err1) {
      errorResponse('err', contactCb);
    } else {
      getStudentAddress(studentId, function(err2, res2) {
        if (err2) {
          errorResponse('an error', contactCb);
        } else {
          var contactAndAddress = {};
          Contact.push(res1);
          contactAndAddress.Contact = res1;
          contactAndAddress.Address = address;
          contactAndAddress.skillsAndIntrests = res2;
          contactCb(err2, res2);
        }
      });
    }
  });
}
function getStudentAddress(studentId, addressCb) {
  var studentAddress = server.models.StudentAddress;
  studentAddress.findOne({'where': {'studentId': studentId, 'primaryInd': 'Y'}, 'include': 'studentAddressIbfk2rel'},
  function(err3, res3) {
    if (err3) {
      errorResponse('an error', addressCb);
    } else {
      console.log('resssssssssssssssssssssssssssss', res3);
      if (res3 == null) {
        campusEvenetStudentSearch.getSkills(studentId, function(err, res4) {
          if (err) {
            errorResponse(err, addressCb);
          } else {
            addressCb(null, res4);
          }
        });
      } else {
        var addressModel = server.models.Address;
        addressModel.findOne({'where': {'addressId': res3.addressId},
        'include': ['cityDetails', 'addressIbfk4rel', 'addressIbfk6rel', 'addressIbfk5rel']},
        function(err4, addressDetails) {
          address.push(addressDetails);
          campusEvenetStudentSearch.getSkills(studentId, function(err, res4) {
            if (err) {
              errorResponse(err, addressCb);
            } else {
              addressCb(null, res4);
            }
          });
        });
      }
    //   getStudentIntrestsAndSkills(studentId, function(err, skillsAndIntrests) {
    //     if (err) {
    //       errorResponse('err', addressCb);
    //     } else {
    //       addressCb(null, res3);
    //     }
    //   });
    }
  });
}
function getTheStrength(data, strengthCb) {
  console.log('addributes', data.profileDetails);
  console.log('true or false', data.profileDetails.length > 0);
  var async = require('async');
  async.map(data.StudentAcademics, getStrength, function(err, res) {
    if (err) {
      errorResponse('an error', strengthCb);
    } else {
      if (data.StudentDetails.data[0].aboutMe != 0) { strength[0] = 5; } else { strength[0] = 0; }
      if (data.StudentDetails.data[0].hobbies != 0) { strength[2] = 5; } else { strength[2] = 0; }
      if (data.AdditionalData.skills.length >= 2) { strength[3] = 5; } else { strength[3] = 0; }
      if (data.AdditionalData.intrests.length >= 2) { strength[4] = 5; } else { strength[4] = 0; }
      if (data.AdditionalData.studentInfo != 0) { strength[1] = 5; } else { strength[1] = 0; }
      if (data.profileDetails.length > 0) { strength[9] = 5; } else { strength[9] = 0; }
      if (strength[5] == null) { strength[5] = 0; }
      if (strength[6] == null) { strength[6] = 0; }
      if (strength[7] == null) { strength[7] = 0; }
      if (strength[8] == null) { strength[8] = 0; }
      strengthCb(null, res);
    }
  });
}
function getStrength(ob, cb) {
  if (ob.programName == 'Project') {
    strength[5] = 5;
    cb(null, 'done');
  } else if (ob.programName == '10th') {
    strength[6] = 5;
    cb(null, 'done');
  } else if (ob.programName == '12th') {
    strength[7] = 5;
    cb(null, 'done');
  } else if (ob.programName == 'Certificate') {
    strength[8] = 5;
    cb(null, 'done');
  } else {
    cb(null, 'done');
  }
}
// function getStudentIntrestsAndSkills(studentId, callBack) {
//   var studentSkills = server.models.StudentSkills;
//   var studentInterests = server.models.StudentInterests;
//   studentSkills.find({'where': {'studentId': studentId}}, function(er, skillResponse) {
//     if (er) {
//       errorResponse('an error', callBack);
//     } else {
//       var async = require('async');
//       async.map(skillResponse, getSkillValue, function(err, skillsOfStudent) {
//         if (err) {
//           errorResponse('err', callBack);
//         } else {
//           studentInterests.find({'where': {'studentId': studentId}}, function(error, intrests) {
//             if (err) {
//               errorResponse('err', callBack);
//             } else {
//               async.map(intrests, getIntrestsValue, function(err, intrestOfStudents) {
//                 if (err) {
//                   errorResponse('err', callBack);
//                 } else {
//                   getObject(skillsOfStudent, function(err, res) {
//                     if (err) {
//                       errorResponse('err', callBack);
//                     } else {
//                       getObject1(intrestOfStudents, function(err, res1) {
//                         if (err) {
//                           errorResponse('err', callBack);
//                         } else {
//                           callBack(null, 'done');
//                         }
//                       });
//                     }
//                   });
//                 }
//               });
//             }
//           });
//         }
//       });
//     }
//   });
// }
// function getObject(array, skillintcb) {
//   skills = [];
//   for (var i = 0; i < array.length; i++) {
//     for (var j = 0; j < array[i].length; j++) {
//       skills.push(array[i][j]);
//     }
//   }
//   skillintcb(null, 'done');
// }
// function getObject1(array, Intcb) {
//   intrests = [];
//   for (var i = 0; i < array.length; i++) {
//     for (var j = 0; j < array[i].length; j++) {
//       intrests.push(array[i][j]);
//     }
//   }
//   Intcb(null, 'done');
// }
// function getSkillValue(obj, callBc) {
//   lookup('SKILL_TYPE_CODE', obj.skillTypeValueId, function(err, skillResponse) {
//     if (err) {
//       errorResponse('err', callBc);
//     } else {
//       callBc(null, skillResponse);
//     }
//   });
// }
// function getIntrestsValue(obj1, callBack) {
//   lookup('INTEREST_TYPE_CODE', obj1.interestTypeValueId, function(err, intrestResponse) {
//     if (err) {
//       errorResponse('err', callBack);
//     } else {
//       callBack(null, intrestResponse);
//     }
//   });
// }
function lookup(type, ob, lookupCallback) {
  var lookup = server.models.LookupType;
  var lookupvalue = server.models.LookupValue;
      //var type = 'SKILL_TYPE_CODE';
  lookup.find({
    'where': {
      lookupCode: type,
    },
  }, function(err, re) {
    if (err) {
      lookupCallback(err, null);
    } else {
      lookupvalue.find({
        'where': {
          lookupValue: ob,
          lookupTypeId: re[0].lookupTypeId,
        },
      }, function(err, re1) {
        if (err) {
          lookupCallback('error', re1);
        } else {
          var data = {};
          data.id = re1[0].lookupValueId;
          lookupCallback(null, re1);
        };
      });
    }
  });
}
exports.getProfile = getProfile;
