'use strict';
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').inputValidation;
var compensationItemJson = require('../common/models/COMPENSATION_PACKAGE_ITEM.json').properties;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var server = require('../server/server');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var compensationPackageItem = server.models.CompensationPackageItem;
var count = 0;
var compArray = [];
  /**
   *updateItem-function deals with updating the iteam ina an compensation pakage
   *@constructor
   * @param {object} iteamData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateItem(itemData, cb) {
  compArray = [];
  // validateModel(itemData, compensationItemJson, function(err, resp) {
  //   if (resp.valid == false) {
  //     cb(resp.errors, resp);
  //   } else {
      itemData.map(function(itemObj) {
        if (
            itemObj.companyId &&
            itemObj.compPackageItemId) {
          var compLookups = [{
            'lookupValueId': itemObj.compPackageTypeValueId,
            'lookupTypeId': 'COMP_PACKAGE_TYPE_CODE',
          }];
          lookupValidation(compLookups, function(lookupValue) {
            if (lookupValue == true) {
              var inputFilterObject = {};
              inputFilterObject['compPackageId'] = itemObj.compPackageId;
              inputFilterObject['companyId'] = itemObj.companyId;
              inputFilterObject['compPackageItemId'] = itemObj.compPackageItemId;
              findEntity(inputFilterObject, compensationPackageItem, function(error, response) {
                if (error) {
                  throwError(error, cb);
                  logger.error('Error while fetching compensation item package record');
                } else if (response) {
                  response.data[0].updateAttributes(itemObj, function(err, info) {
                    if (err) {
                      throwError('error while updating compensation item package', cb);
                    } else {
                      logger.info('compensation package item record Updated Successfully');
                      info['requestStatus'] = true;
                      info['updateDatetime'] = new Date();
                      count++;
                      compArray.push(info);
                      compArray['compensationPkgItem'] = compArray;
                      if (itemData.length == count) {
                        count = 0;
                        cb(null, compArray);
                      }
                    }
                  });
                } else {
                  throwError('Invalid compPackageItemId, compPackageId and CompanyId', cb);
                  logger.error('Invalid compPackageItemId, compPackageId and CompanyId');
                }
              });
            } else {
              throwError('Invalid compPackageTypeValueId', cb);
              logger.error('Invalid compPackageTypeValueId');
            }
          });
        } else {
          throwError(' compPackageItemId and CompanyId are required', cb);
          logger.error(' compPackageItemId and CompanyId are required');
        }
      });
    }
  // });
}
exports.updateItem = updateItem;
