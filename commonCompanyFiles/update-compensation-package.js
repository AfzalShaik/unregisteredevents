'use strict';
var compensationValidation = require('../commonValidation/compensation-package-validation');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js');
var server = require('../server/server');
var compensationPackage = server.models.CompensationPackage;
  /**
   *updatePackageService-function deals with updating the compensation pakage
   *@constructor
   * @param {object} campusData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updatePackageService(packageData, cb) {
  if (packageData) {
    if (packageData.compPackageId && packageData.companyId) {
      var validValue;
      compensationValidation.validateCompensationPackageJson(packageData, function(err, validValue) {
        validValue = validValue;
        if (validValue == false) {
          throwError(err, cb);
          logger.error('validation error');
        } else {
          var inputFilterObject = {};
          inputFilterObject['compPackageId'] = packageData.compPackageId;
          inputFilterObject['companyId'] = packageData.companyId;
          findEntity.entityDetailsById(inputFilterObject, compensationPackage, function(error, response) {
            if (error) {
              throwError(error, cb);
              logger.error('Error while fetching compensation package record');
            } else if (response) {
              compensationValidation.validateCompensationPackageLookup(packageData, function(lookupErr, lookupValue) {
                if (lookupErr) {
                  throwError(lookupErr, cb);
                  logger.error('error while validating package lookups');
                } else if (lookupValue == true) {
                  response.data.updateAttributes(packageData, function(err, info) {
                    if (err) {
                      throwError('error while updating compensation package', cb);
                    } else {
                      logger.info('compensation package record Updated Successfully');
                      info['requestStatus'] = true;
                      info['updateDatetime'] = new Date();
                      cb(null, info);
                    }
                  });
                } else {
                  throwError('invalid  approvalStatusValueId', cb);
                }
              });
            } else {
              throwError('Invalid companyId or compPackageId', cb);
              logger.error('Invalid companyId or compPackageId');
            }
          });
        }
      });
    } else {
      throwError('compPackageId and companyId are required', cb);
      logger.error('compPackageId and companyId are required');
    }
  } else {
    throwError('Input cannot be blank', cb);
    logger.error('Input cannot be blank');
  }
}
exports.updatePackageService = updatePackageService;
