'use strict';
var server = require('../server/server');

function createCompanyLog(input, callBc) {
  var companyUploadLog = server.models.CompanyUploadLog;
  var pathForm = require('path');
  var csvFileLocation = './attachments/' + input.container + '/' + input.name + '/' + input.originalFileName;
  csvFileLocation = pathForm.join(__dirname, '../../../', csvFileLocation);
  var errorFileLocation = './attachments/' + input.container + '/' + 'download' + '/' + input.name;
  errorFileLocation = pathForm.join(__dirname, '../../../', errorFileLocation);
  var logInput = {
    'compUploadTypeValueId': input.compUploadTypeValueId,
    'companyId': input.companyId,
    'inputParameters': 'string',
    'createUserId': 1,
    'createDatetime': new Date(),
    'csvFileLocation': csvFileLocation,
    'errorFileLocation': errorFileLocation,
    'totalNoRecs': input.totalNoRecs,
    'noFailRecs': input.noFailRecs,
    'noSuccessRecs': input.noSuccessRecs,
  };
  companyUploadLog.create(logInput, function(logErr, logOutput) {
    callBc(null, logOutput);
  });
}
exports.createCompanyLog = createCompanyLog;
