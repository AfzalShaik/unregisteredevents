'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var validation = require('../commonValidation/personValidation');
var updateAddress = require('../commonValidation/updateAddress');
var allmethods = require('../commonValidation/allmethods');
var EmployerPerson = server.models.EmployerPerson;
var lookupMethods = require('../commonValidation/lookupMethods');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *employeePersonUpdate-function deals with updating the employee person
   *@constructor
   * @param {object} personData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function employeePersonUpdate(personData, cb) {
  if (personData) {
    if (personData.companyId && personData.employerPersonId) {
      //validating input json fields
      validation.validateContactJson(personData, function(err, validVal) {
        if (err) {
          cb(err, personData);
        } else if (validVal == true) {
          var prefixVal, genderVal, personTypeVal;
          var lookupValueObj = {};
          var lookupValueObj1 = {};
          var lookupValueObj2 = {};
          var prefixCode = 'PREFIX_CODE';
          var genderCode = 'GENDER_CODE';
          var personTypeCode = 'PERSON_TYPE_CODE';
          lookupValueObj['lookupValueId'] = personData.prefixValueId;
          lookupValueObj1['lookupValueId'] = personData.genderValueId;
          lookupValueObj2['lookupValueId'] = personData.personTypeValueId;
          lookupMethods.typeCodeFunction(lookupValueObj, prefixCode, function(prefixVal) {
            prefixVal = prefixVal;

            //addressTypeCodeFunction to verify addressTypeValueId
            lookupMethods.typeCodeFunction(lookupValueObj1, genderCode, function(genderVal) {
              genderVal = genderVal;

              //addressFunction function verifies city id , countryCode and stateCode
              lookupMethods.typeCodeFunction(lookupValueObj2, personTypeCode, function(personTypeVal) {
                personTypeVal = personTypeVal;

                if (prefixVal == false || genderVal == false || personTypeVal == false) {
                  throwError(' Invalid prefixVal or genderVal or personTypeVal', cb);
                } else {
                  EmployerPerson.findOne({
                    'where': {
                      'and': [{
                        'companyId': personData.companyId,
                      }, {
                        'employerPersonId': personData.employerPersonId,
                      }],
                    },
                  }, function(err, personList) {
                    if (err) {
                      //throws error

                      cb(err, personData);
                    } else if (personList) {
                      logger.info('record found for given input');

                      personList.updateAttributes(personData, function(err, info) {
                        if (err) {
                          cb(err, info);
                        } else {
                          //making requestStatus is true;
                          logger.info('employee person record Updated Successfully');
                          info['requestStatus'] = true;
                          info['updateDatetime'] = new Date();
                          cb(null, info);
                        }
                      });
                    } else {
                      //throws error incase of invalid employerPersonId or companyId
                      throwError('Invalid employerPersonId or companyId ', cb);
                      logger.error('Invalid employerPersonId or companyId');
                    }
                  });
                }
              });
            });
          });
        } else {
          throwError('You cannot perform update operation: ', cb);
          logger.error('You cannot perform update operation');
        }
      });
    } else {
      throwError('companyId and  employerPersonId are required', cb);
      logger.error('companyId and  employerPersonId are required');
    }
  } else {
    logger.error("Input can't be Null");
    throwError('Input is Null: ', cb);
  }
}
exports.employeePersonUpdate = employeePersonUpdate;
