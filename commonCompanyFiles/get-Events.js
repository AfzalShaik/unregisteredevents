'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var errorResponse = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var events = [];
var sheduledEvents = [];
var sheduledd = [];
var getSheduledEvents = function(companyId, callback) {
  events = [];
  typeCodeFunction('Scheduled', 'EMPLOYER_EVENT_STATUS_CODE', function(err, sheduled) {
    sheduledd.push(sheduled.lookupValueId);
    typeCodeFunction('Initial', 'EMPLOYER_DRIVE_STATUS_CODE', function(err, lookupRes1) {
      if (err) {
        errorResponse('error in finding initial one', callback);
      } else {
        typeCodeFunction('Approved', 'EMPLOYER_DRIVE_STATUS_CODE', function(err, lookupRes) {
          if (err) {
            errorResponse('error occured', callback);
          } else {
            var empDrive = server.models.EmployerDrive;
            empDrive.find({'where': {and: [{'companyId': companyId}, {
              or: [
            {'driveStatusValueId': lookupRes1.lookupValueId},
            {'driveStatusValueId': lookupRes.lookupValueId},
              ],
            }],
    }}, function(err, res) {
              if (err) {
                errorResponse('error occured', callback);
              } else {
                var async = require('async');
                async.map(res, getTheDetails, function(err, resp) {
                  if (err) {
                    errorResponse('error occured', callback);
                  } else {
                    for (var i = 0; i < events.length; i++) {
                      async.map(events[i], getSheduledOne);
                    }
                    var data = {};
                    data.totalDrives = res;
                    data.thisYearEvents = sheduledEvents;
                    callback(null, data);
                  }
                });
              }
            });
          }
        });
      }
    });
  });
};
var getTheDetails = function(obj, callbc) {
  var empEvent = server.models.EmployerEvent;
  empEvent.find({'where': {'empDriveId': obj.empDriveId}}, function(err, eventRes) {
    if (err) {
      errorResponse('error occured in getting details function', callbc);
    } else {
      if (eventRes != 0) { events.push(eventRes); }
      callbc(null, 'done');
    }
  });
};
var getSheduledOne = function(ob) {
  var date = new Date().getFullYear();
  var final = ob.scheduledDate;
  var parse = JSON.stringify(final);
  var p = parse.substr(1, 4);
 // console.log(sheduledd);
  if (ob.eventStatusValueId == sheduledd[0]) {
   // console.log('getting in');
    if (p >= date && p < date + 1) {
     // console.log('these are sheduled');
      sheduledEvents.push(ob);
    }
  }
};

function typeCodeFunction(value, type, lookupCallback) {
  var lookup = server.models.LookupType;
  var lookupvalue = server.models.LookupValue;
  lookup.find({
    'where': {
      lookupCode: type,
    },
  }, function(err, re) {
    if (err) {
      lookupCallback(err, null);
    } else {
      lookupvalue.findOne({
        'where': {
          lookupValue: value,
          lookupTypeId: re[0].lookupTypeId,
        },
      }, function(err, re1) {
        if (err) {
          lookupCallback('error', null);
        } else {
          lookupCallback(null, re1);
          return re1;
        };
      });
    }
  });
}
exports.getSheduledEvents = getSheduledEvents;
