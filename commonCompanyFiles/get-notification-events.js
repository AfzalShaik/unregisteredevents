'use strict';
var server = require('../server/server');
var employerEvent = server.models.EmployerEvent;
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js');
var employerDriveCampuses = server.models.EmployerDriveCampuses;
function notificationEvents(empEventId, cb) {
  getEployerEventDetails(empEventId, function(err, eventDetails) {
    var companyId = eventDetails.data.companyId;
    getCampusId(empEventId, function(campusErr, campusId) {
      notificationMsgTempateCreate(empEventId, function(tempErr, templateResp) {
        notificationEventCreate(templateResp, function(eventErr, eventResp) {
          broadcastRoleCreate(eventResp, companyId, function(roleErr, roleResp) {
            cb(null, roleResp);
          });
        });
      });
    });
  });
}
exports.notificationEvents = notificationEvents;

function getEployerEventDetails(empEventId, callBc) {
  var inputFilterObject = {};
  inputFilterObject['empEventId'] = empEventId;
  findEntity.entityDetailsById(inputFilterObject, employerEvent, function(error, response) {
    callBc(null, response);
  });
}

function getCampusId(empEventId, callBc) {
  var inputFilterObject = {};
  inputFilterObject['empEventId'] = empEventId;
  findEntity.entityDetailsById(inputFilterObject, employerDriveCampuses, function(error, response) {
    callBc(null, response);
  });
}

function notificationEventCreate(input, callBc) {
  var lookup = require('../commonValidation/lookupMethods').lookupMethod;
  lookup('NOTIFICATION_TYPE', function(err, response) {
    var notificationType = response.find(findStatus);

    function findStatus(statusVal) {
      return statusVal.lookupValue === 'User Management';
    }
    var notificationEventInput = {
      'notificationType': notificationType.lookupValueId,
      'broadcastMode': 'GRP',
      'notificationMessageTemplateId': input.messageTemplateId,
      'createDatetime': new Date(),
      'updateDatetime': new Date(),
      'createUserId': 1,
      'updateUserId': 1,
    };
    var notificationEvents = server.models.NotificationEvents;
    notificationEvents.create(notificationEventInput, function(err, eventResp) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, eventResp);
      }
    });
  });
}

//notification template
function notificationMsgTempateCreate(input, callBc) {
  var lookup = require('../commonValidation/lookupMethods').lookupMethod;
  lookup('LANGUAGE_TYPE_CODE', function(err, response) {
    var language = response.find(findStatus);

    function findStatus(statusVal) {
      return statusVal.lookupValue === 'English';
    }
    var templateInput = {
      'language': language.lookupValueId,
      'messageTemplate': 'SCHEDULE_CAMPUS_EVENT',
      'notificationName': 'SHARE_COMPANY_EVENT',
      'createDatetime': new Date(),
      'updateDatetime': new Date(),
      'createUserId': 1,
      'updateUserId': 1,
    };
    var notificationMessageTemplates = server.models.NotificationMessageTemplates;
    notificationMessageTemplates.create(templateInput, function(err, templateResp) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, templateResp);
      }
    });
  });
}

function broadcastRoleCreate(eventResp, parentId, callBc) {
  var roleInput = {
    'notificationEventId': eventResp.notificationEventId,
    'parentId': parentId,
    'broadcastRoleCode': 'PLCDIR',
    'createDatetime': new Date(),
    'updateDatetime': new Date(),
    'createUserId': 1,
    'updateUserId': 1,
  };
  var notificationEventBroadcastRoles = server.models.NotificationEventBroadcastRoles;
  notificationEventBroadcastRoles.create(roleInput, function(err, roleResp) {
    if (err) {
      callBc(err, null);
    } else {
      callBc(null, roleResp);
    }
  });
}

function notificationDetaisCreate() {

}
