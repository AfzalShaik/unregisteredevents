/**
 * Created by admin on 17-07-2017.
 */
// var server1 = require('../../server/server');
// This file will handle request before entering into execution of business logic available at common/models folder
// Onnce transaction done then we change response before sending to client
/**
 * Created by admin on 17-07-2017.
 */
// var server1 = require('../../server/server');
// This file will handle request before entering into execution of business logic available at common/models folder
// Onnce transaction done then we change response before sending to client

// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Remove once Vijay's authorization comes into place.
'use strict';
module.exports = function(server) {
  var remotes = server.remotes();
  //Before remote method for authentication
  // remotes.before('**', function(ctx, next) {
  //   var url = ctx.req.originalUrl.toString();
  //   var method = ctx.req.method.toString();
  //   var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
  //   var createUserId = ctx.req.body.createUserId;
  //   var updateUserId = (ctx.req.body.updateUserId) ? ctx.req.updateUserId : createUserId;
  //   var scoraServices = server.models.ScoraServices;
  //   var scoraUserRole = server.models.ScoraUserRole;
  //   var scoraRolePrivileges = server.models.ScoraRolePrivileges;
  //   var scoraPrivilegeService = server.models.ScoraPrivilegeService;
  //   var accessToken = server.models.AccessToken;
  //   if (url.indexOf('/api/ScoraUsers') > -1) {
  //     loginService(method, next);
  //   } else {
  //     var access = url.split('?')[1];
  //     var token = access.split('=')[1];
  //     if (url.split('?')[1]) {
  //       var serviceName = url.split('?');
  //       accessToken.find({}, function(err, serv) {
  //         var accessToken = serv.find(findStatus);
  //         function findStatus(statusVal) {
  //           return statusVal.id === token;
  //         }
  //         if (accessToken) {
  //           next();
  //         } else {
  //           unauthorizedRes(next);
  //         }
  //       });
  //     } else {
  //       unauthorizedRes(next);
  //     }
  //   }
  // });
  // Modify response before sending response to requested user/client
  remotes.after('**', function(ctx, next) {
    if (ctx.req.method.toString() == 'POST') {
      ctx.result = {
        data: ctx.result,
        requestStatus: true,
      };
      next();
    } else {
      next();
    }
  });
  // error message if any unauthorized request comes to server
  var unauthorizedRes = function(next) {
    var adminAuthorizationError = new Error('AUTHORIZATION_REQUIRED');
    adminAuthorizationError.statusCode = 401;
    // adminAuthorizationError.requestStatus = false;
    next(adminAuthorizationError, null);
  };
  var loginService = function(method, next) {
    if (method == 'POST' || method == 'GET') {
      next();
    } else {
      unauthorizedRes(next);
    }
  };
};
