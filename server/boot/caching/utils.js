var nodeCache = require('node-cache');
/**
 *The function run a find on an input Model
 *The function will accept an error function, a Modelname and a callback function.
 * @param {string} modelname
 * @param {function} callback
 */
function findEntityDetailsAsIs(modelname, callback) {
  modelname.find({}, function(error, result) {
      if (error)
        callback(error, null);
      else {
        //console.log(result);
        callback(null, modelname, result);
      }
    });
};

/**
 * The function will create a Cachen named modelName+Cache string.
 * The function use Node-Cache for Cache creation.
 * @param {string} modelname
 * @returns NewCache
 */
function createCache(callback, key, error, modelname, entityDetails) {
  var modelCache = new nodeCache();
  return callback(null, modelCache, entityDetails);
};



/**
 * The function use Node-Cache set property to define a value inside Node-Cache
 *
 * @param {String} cacheName
 * @param {String} key
 * @param {Object} valueObject
 * @param {Function} callback
 */
function setCache(callback, key, error, cacheObject, valueObject) {
  //console.log('this is value object', valueObject);
  cacheObject.set(key, valueObject);
    // console.log('Done');
  callback(cacheObject);
}

/**
 * The getCache function will return a Value associted with the given Key
 * Input take cacheName : String, key : String and a callback function.
 * @param {String} cacheName
 * @param {String} key
 * @param {Function} callback
 * @returns
 */
function getCache(callback, key, cacheObject) {
    // console.log(cacheObject.get(key));
  callback(cacheObject.get(key));
}

function printEntitydetails(error, entitydetails) {
    // console.log(entitydetails);
  }

exports.findEntityDetailsAsIs = findEntityDetailsAsIs;
exports.createCache = createCache;
exports.setCache = setCache;
exports.getCache = getCache;
exports.printEntitydetails = printEntitydetails;
