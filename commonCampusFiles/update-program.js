'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var validation = require('../commonValidation/programValidation');
var updateAddress = require('../commonValidation/updateAddress');
var allmethods = require('../commonValidation/allmethods');
var Program = server.models.Program;
var lookupMethods = require('../commonValidation/lookupMethods');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *programUpdate-function deals with updating the program update
   *@constructor
   * @param {object} programData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function programUpdate(programData, cb) {
  if (programData) {
    if (programData.campusId && programData.programId && programData.departmentId) {
      //validating input json fields
      validation.validateJson(programData, function(err, validVal) {
        if (err) {
          cb(err, programData);
        } else if (validVal == true) {
          var programTypeVal, programClassVal, programCatVal, programMajorVal;
          var lookupValueObj = {};
          var lookupValueObj1 = {};
          var lookupValueObj2 = {};
          var lookupValueObj2 = {};
          var lookupValueObj3 = {};
          var programTypeCode = 'PROGRAM_TYPE_CODE';
          var programClassCode = 'PROGRAM_CLASSIFICATION_CODE';
          var programCatCode = 'PROGRAM_CATEGORY_CODE';
          var programMajorCode = 'PROGRAM_MAJOR';
          lookupValueObj['lookupValueId'] = programData.programTypeValueId;
          lookupValueObj1['lookupValueId'] = programData.programClassValueId;
          lookupValueObj2['lookupValueId'] = programData.programCatValueId;
          lookupValueObj3['lookupValueId'] = programData.programMajorValueId;
          lookupMethods.typeCodeFunction(lookupValueObj, programTypeCode, function(programTypeVal) {
            programTypeVal = programTypeVal;

            //checking program class
            lookupMethods.typeCodeFunction(lookupValueObj1, programClassCode, function(programClassVal) {
              programClassVal = programClassVal;

              //checking program cat
              lookupMethods.typeCodeFunction(lookupValueObj2, programCatCode, function(programCatVal) {
                programCatVal = programCatVal;
                //checking program major type code
                lookupMethods.typeCodeFunction(lookupValueObj3, programMajorCode, function(programMajorVal) {
                  programMajorVal = programMajorVal;

                  if (programTypeVal == false || programClassVal == false || programCatVal == false || programMajorVal == false) {
                    throwError(' Invalid programTypeVal or programClassVal or programCatVal or programMajorVal', cb);
                    logger.error('Invalid programTypeVal or programClassVal or programCatVal or programMajorVal');
                  } else {
                    Program.findOne({
                      'where': {
                        'and': [{
                          'campusId': programData.campusId,
                        }, {
                          'programId': programData.programId,
                        }],
                      },
                    }, function(err, programList) {
                      if (err) {
                        //throws error

                        cb(err, programData);
                      } else if (programList) {
                        programList.updateAttributes(programData, function(err, info) {
                          if (err) {
                            logger.error('error while updating program');
                            cb(err, info);
                          } else {
                            //making requestStatus is true;
                            logger.info('Program record Updated Successfully');
                            info['requestStatus'] = true;
                            info['updateDatetime'] = new Date();
                            cb(null, info);
                          }
                        });
                      } else {
                        //throws error incase of invalid programId or campusId
                        throwError('Invalid programId or campusId or departmentId ', cb);
                        logger.error('Invalid programId or campusId or departmentId');
                      }
                    });
                  }
                });
              });
            });
          });
        } else {
          throwError('You cannot perform update operation: ', cb);
          logger.error('You cannot perform update operation');
        }
      });
    } else {
      throwError('campusId,departmentId and  programId are required', cb);
      logger.error('campusId,departmentid and programId are required');
    }
  } else {
    logger.error("Input can't be Null");
    throwError('Input is Null: ', cb);
  }
}
exports.programUpdate = programUpdate;
