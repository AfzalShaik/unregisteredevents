'use strict';
var server = require('../server/server');
var enrollment = server.models.Enrollment;
var eventStudentList = server.models.EventStudentList;
var campusEvent = server.models.CampusEvent;
var async = require('async');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;

function getCampus(inputObj, callBc) {
  getNoOfStudents(inputObj, function(campusErr, campusResult) {
    if (campusErr) {
      throwError(campusErr, null);
    } else {
      var dashBoardObj = {};
      dashBoardObj.noofStudents = campusResult.numberOfStudents;
      dashBoardObj.campusId = campusResult.campusId;
      getStudentStatus(dashBoardObj, 'CANDIDATE_STATUS_TYPE', 'Offered', function(placedErr, placedStudents) {
        if (placedErr) {
          throwError(placedErr, callBc);
        } else {
          var dashBoardObj = {};
          dashBoardObj = placedStudents;
          dashBoardObj.studentsPlaced = placedStudents.students;
          delete placedStudents.students;
          getStudentStatus(dashBoardObj, 'CANDIDATE_STATUS_TYPE', 'Shortlisted', function(shortErr, shortListed) {
            shortListed.studentsShortListed = shortListed.students;
            delete shortListed.students;
            getStudentStatus(shortListed, 'EDUCATION_EVENT_STATUS_CODE', 'Closed', function(visitedErr, visited) {
              visited.noOfCompaniesVisited = visited.students;
              delete visited.students;
              getStudentStatus(visited, 'EDUCATION_EVENT_STATUS_CODE', 'Scheduled', function(scheduledErr, scheduled) {
                scheduled.noOfEventsScheduled = scheduled.students;
                delete scheduled.students;
                callBc(null, scheduled);
                console.log(scheduled);
              });
            });
          });
        }
      });
    }
  });
}

function getNoOfStudents(inputObj, noCB) {
  var campus = server.models.Campus;
  var enrollemntObj = {};
  enrollemntObj['campusId'] = inputObj.campusId;
  campus.findOne({
    'where': {
      'and': [enrollemntObj],
    },
  }, function(error, result) {
    if (error) {
      noCB(error, null);
    } else {
      noCB(null, result);
    }
  });
}

function getModelData(modelName, input, callback) {
  modelName.find({
    'where': {
      'and': [input],
    },
  }, function(error, result) {
    if (error) {
      callback(error, null);
    } else {
      callback(null, result);
    }
  });
}

function getStudentStatus(dashBoardObj, code, value, statusCB) {
  getLookups(code, value,
    function(candidateStatusOffered) {
      var eventPlacedObj = {};
      eventPlacedObj['campusId'] = dashBoardObj.campusId;
      if (code == 'CANDIDATE_STATUS_TYPE') {
        eventPlacedObj['candidateStatusValueId'] = candidateStatusOffered.lookupValueId;
      } else {
        eventPlacedObj['eventStatusValueId'] = candidateStatusOffered.lookupValueId;
      }
      // eventPlacedObj['candidateStatusValueId'] = candidateStatusOffered.lookupValueId;
      var eventStudentList = server.models.EventStudentList;
      var campusEvent = server.models.CampusEvent;
      var modelName = (code == 'CANDIDATE_STATUS_TYPE') ? eventStudentList : campusEvent;
      modelName.find({
        'where': {
          'and': [eventPlacedObj],
        },
      }, function(placedErr, placedStudents) {
        if (placedErr) {
          throwError(placedErr, statusCB);
        } else {
          var dashBoardObject = {};
          dashBoardObject = dashBoardObj;
          dashBoardObject.students = placedStudents.length;
          statusCB(null, dashBoardObject);
        }
      });
    });
}

function getLookups(code, value, callBc) {
  var lookup = require('../commonValidation/lookupMethods').lookupMethod;
  lookup(code, function(err, lookUpResponse) {
    var lookupValueId = lookUpResponse.find(findStatus);

    function findStatus(statusVal) {
      return statusVal.lookupValue === value;
    }
    callBc(lookupValueId);
  });
  // }
}
exports.getLookups = getLookups;
exports.getCampus = getCampus;
exports.getStudentStatus = getStudentStatus;
