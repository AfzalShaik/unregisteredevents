'use strict';
var campusDriveValidation = require('../commonValidation/all-models-validation');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js');
var server = require('../server/server');
var campusDriveJson = require('../commonValidation/all-models-json').campusDrive;
var campusDriveLookups = require('../commonValidation/lookupMethods').typeCodeFunction;
var campusDrive = server.models.CampusDrive;
  /**
   *updateCampusDriveService-function deals with updating the campus drive details
   *@constructor
   * @param {object} driveData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateCampusDriveService(driveData, cb) {
  if (driveData) {
    if (driveData.campusDriveId && driveData.campusId) {
      var validValue, driveType, driveStatus;
      var driveTypeObj = {};
      var driveStatusObj = {};
      var driveTypeCode = 'EDUCATION_DRIVE_TYPE_CODE';
      var driveStatusCode = 'EDUCATION_DRIVE_STATUS_CODE';
      driveTypeObj['lookupValueId'] = driveData.driveTypeValueId;
      driveStatusObj['lookupValueId'] = driveData.driveStatusValueId;
      campusDriveLookups(driveTypeObj, driveTypeCode, function(typeVal) {
        if (typeVal == true) {
          campusDriveLookups(driveStatusObj, driveStatusCode, function(statusVal) {
            if (statusVal == true) {
              campusDriveValidation.validateModelsJson(driveData, campusDriveJson, function(err, validValue) {
                validValue = validValue;
                if (validValue == false) {
                  throwError(err, cb);
                  logger.error('validation error');
                } else {
                  var inputFilterObject = {};
                  inputFilterObject['campusId'] = driveData.campusId;
                  inputFilterObject['campusDriveId'] = driveData.campusDriveId;
                  findEntity.entityDetailsById(inputFilterObject, campusDrive, function(error, response) {
                    if (error) {
                      throwError(error, cb);
                      logger.error('Error while fetching employer campus list Program record');
                    } else if (response) {
                      response.data[0].updateAttributes(driveData, function(err, info) {
                        if (err) {
                          throwError('error while updating emp campus Program', cb);
                        } else {
                          logger.info('campus drive record Updated Successfully');
                          info['requestStatus'] = true;
                          info['updateDatetime'] = new Date();
                          cb(null, info);
                        }
                      });
                    } else {
                      throwError('Invalid  campusId or campusDriveId', cb);
                      logger.error('Invalid campusDriveId or campusId ');
                    }
                  });
                }
              });
            } else {
              throwError('Invalid Education_Drive_Status_code', cb);
            }
          });
        } else {
          throwError('Invalid Education_Drive_Type_code', cb);
        }
      });
    } else {
      throwError('campusDriveId, campusId are required', cb);
      logger.error('campusDriveId, campusId are required');
    }
  } else {
    throwError('Input cannot be blank', cb);
    logger.error('Input cannot be blank');
  }
}
exports.updateCampusDriveService = updateCampusDriveService;
