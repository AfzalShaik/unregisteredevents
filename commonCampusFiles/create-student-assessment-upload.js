'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var async = require('async');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var errorArray = [];
var repeated = [];
var final = [];
var finalRepete = [];
var finalNonRepeat = [];
var cgpaArray = [];
var initialScore;
var scoreCount = 0;

function createAssessment(assessmentData, assessmentInput, cb) {
  console.log(assessmentData);
  async.map(assessmentData, validateInput, function(error, response) {
    if (error) {
      throwError('Invalid DataTypes', callBc);
    } else {
      var clean = cleanArray(response);
      getNonRepeatData(response, function(err, res) {
        if (err) {
          throwError('err', cb);
        } else {
          async.map(response, getFinalData, function(err1, res1) {
            if (err1) {
              cb(err1, null);
            } else {
              var final = cleanArray(response);
              var studentsAssessmentUploadWork = server.models.StudentsAssessmentUploadWork;
              studentsAssessmentUploadWork.destroyAll({}, function(err2, res3) {
                async.map(repeated, insertIntoTemp, function(err2, res2) {
                  if (err2) {
                    errorFunction('err', cb);
                  } else {
                    var assessmentArray = cleanArray(response);
                    if (finalNonRepeat.length > 0) {
                      // console.log('final non repeat', finalNonRepeat);
                      async.map(finalNonRepeat, getInfo, function(scoreErr, scoreResp) {
                        if (scoreErr) {
                          throwError(scoreErr, cb);
                        } else {
                          var scoreResponse = cleanArray(scoreResp);
                          if (scoreResponse.length > 0) {
                            async.map(scoreResponse, checkScore, function(err, scoreOutput) {
                              var scoreArray = cleanArray(scoreOutput);
                              if (scoreArray.length > 0) {
                                initialScore = scoreArray[0].score.toString().length;
                                // async.map(scoreArray, validateScore, function(validateErr, validatedScore) {
                                validateScore(scoreArray, function(validateErr, validatedScore) {
                                  // console.log('iiiiiiiiiiiiiiiiiiiiiiii....iiiiiiiiii ', validatedScore);
                                  if (validatedScore) {
                                    var validatedScores = cleanArray(validatedScore);
                                    checkScoreOrCgpa(validatedScores, function(checkErr, chekedScores) {
                                      // console.log('000000000000000000000000 ', chekedScores);
                                      if (chekedScores) {
                                        var cgpasArray = cleanArray(chekedScores);
                                        // console.log('000000000000000000000000 ', cgpasArray);
                                        async.map(cgpasArray, createOrUpdate, function(createErr, responseCreated) {
                                          if (createErr) {
                                            throwError(createErr, cb);
                                          } else {
                                            commonErrorRead(responseCreated, function(err, output) {
                                              cb(null, output);
                                            });
                                          }
                                        });
                                      } else {
                                        commonErrorRead(undefined, function(err, output) {
                                          cb(null, output);
                                        });
                                      }
                                    });
                                  } else {
                                    commonErrorRead(undefined, function(err, output) {
                                      cb(null, output);
                                    });
                                  }
                                });
                              } else {
                                commonErrorRead(undefined, function(err, output) {
                                  cb(null, output);
                                });
                              }
                            });
                          } else {
                            commonErrorRead(undefined, function(err, output) {
                              cb(null, output);
                            });
                          }
                        }
                      });
                    } else {
                      commonErrorRead(undefined, function(err, output) {
                        cb(null, output);
                      });
                    }
                  }
                });
              });
            }
          });
        }
      });
    }
  });

  function validateScore(scoreArrayInput, scoresCB) {
    async.map(scoreArrayInput, checkAllScores, function(error, allScores) {
      if (scoreCount >= 1) {
        var studentsAssessmentUploadWork = server.models.StudentsAssessmentUploadWork;
        studentsAssessmentUploadWork.create(scoreArrayInput, function(err, res) {
          scoresCB(null, []);
        });
      } else {
        scoresCB(null, allScores);
      }
    });

    function checkAllScores(obj, cb) {
      // console.log('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii ', obj);
      if (obj.score.toString().length == initialScore) {
        obj.error = 'Score/ CGPA should be in the same format for a campus';
        cb(null, obj);
      } else {
        scoreCount = scoreCount + 1;
        // createAssessmentUpload(obj, 'Score/ CGPA should be in the same format for a campus', function(err, errOutput) {
        cb(null, obj);
        // });
      }
    }
  }

  function checkScoreOrCgpa(inputArray, clback) {
    var assessment = server.models.Assessment;
    assessment.findOne({
      'where': {
        'campusId': assessmentInput.campusId,
      },
    }, function(assessmentErr, assessmentOut) {
      if (assessmentOut) {
        async.map(inputArray, assessmentValidation, function(assErr, assessments) {
          // console.log('ifffffffffffff ', assessment);
          clback(null, assessments);
        });

        function assessmentValidation(object, assCB) {
          // console.log('ifffffffffffff ', object);
          var assessmentScore = assessmentOut.score;
          var assessmentScoreLength = assessmentScore.toString().length;
          var scoreLength = object.score.toString().length;
          if (assessmentScoreLength == scoreLength) {
            // console.log('ifffffffffffff ', inputArray[i]);
            // goodScoreArray.push(inputArray[i]);
            assCB(null, object);
          } else {
            // console.log('iffffffrlseeeeeeefffffff ', inputArray[i]);
            createAssessmentUpload(object, 'Score/ CGPA should be in the same format for a campus', function(err, errOutput) {
              assCB(null, null);
            });
          }
        }
      } else if (assessmentErr) {
        createAssessmentUpload(inputArray, 'Score/ CGPA should be in the same format for a campus', function(err, errOutput) {
          clback(null, null);
        });
      } else {
        // console.log('inputArrauuuuuuuuuuu ', inputArray);
        clback(null, inputArray);
      }
    });
  }

  function getInfo(obj, callback) {
    var enrollmentModel = server.models.Enrollment;
    enrollmentModel.findOne({
      'where': {
        'and': [{
          'admissionNo': obj.admissionNo,
        },
        {
          'campusId': assessmentInput.campusId,
        },
        {
          'programId': assessmentInput.programId,
        },
        ],
      },
    }, function(error, findResp) {
      if (error) {
        createAssessmentUpload(obj, error, function(err, errOutput) {
          callback(null, null);
        });
      } else if (findResp) {
        obj.enrollmentId = findResp.enrollmentId;
        callback(null, obj);
      } else {
        createAssessmentUpload(obj, 'Invalid Admission Number', function(err, errOutput) {
          callback(null, null);
        });
      }
    });
  }

  function createAssessmentUpload(object, error, callBc) {
    var studentsAssessmentUploadWork = server.models.StudentsAssessmentUploadWork;
    var input = {
      'rowNumber': object.rowNumber,
      'admissionNo': object.admissionNo,
      'score': object.score,
      'programUnitTypeValueId': assessmentInput.programUnitTypeValueId,
      'departmentId': assessmentInput.departmentId,
      'programId': assessmentInput.programId,
      'error': error,
    };
    studentsAssessmentUploadWork.create(input, function(err, out) {
      if (err) {
        throwError(err, callBc);
      } else {
        // console.log('111111111111111111111111111111111111 ', err, out, JSON.stringify(input));
        callBc(null, out);
      }
    });
  }

  function checkScore(obj, callBack) {
    if (obj.score >= 0) {
      callBack(null, obj);
    } else {
      createAssessmentUpload(obj, 'Invalid Score', function(err, errOutput) {
        callBack(null, null);
      });
    }
  }

  function createOrUpdate(object, createCB) {
    var assessment = server.models.Assessment;
    assessment.findOne({
      'where': {
        'and': [{
          'enrollmentId': object.enrollmentId,
        }, {
          'programUnitTypeValueId': assessmentInput.programUnitTypeValueId,
        }],
      },
    }, function(error, response) {
      if (response) {
        var updateInput = response;
        updateInput.score = object.score;
        updateInput.updateDatetime = new Date();
        updateInput.campusId = assessmentInput.campusId;
        response.updateAttributes(updateInput, function(updateErr, updateResponse) {
          createCB(null, updateResponse);
        });
      } else {
        var createInput = {
          'enrollmentId': object.enrollmentId,
          'programUnitTypeValueId': assessmentInput.programUnitTypeValueId,
          'subjectDetails': null,
          'score': object.score,
          'highlights': null,
          'dataVerifiedInd': 'Y',
          'createDatetime': new Date(),
          'updateDatetime': new Date(),
          'createUserId': 1,
          'updateUserId': 1,
          'campusId': assessmentInput.campusId,
        };
        // console.log('inputtttttttttttttttttttttttttt ', createInput);
        assessment.create(createInput, function(createError, createResponse) {
          if (createError) {
            createCB(null, null);
          } else {
            createCB(null, createResponse);
          }
        });
      }
    });
  }

  function getfailureMass(iterateArray, cb) {
    var failedArray = [];
    var finalArray = [];
    failedArray = iterateArray;
    if (errorArray.length > 0) {
      for (var j = 0; j < errorArray.length; j++) {
        // console.log();
        failedArray.push(errorArray[j]);
        // console.log(failedArray);
      }
    } else {
      failedArray = iterateArray;
    }
    for (var i = 0; i < failedArray.length; i++) {
      var obj = {
        'rowNumber': failedArray[i].rowNumber,
        'admissionNo': failedArray[i].admissionNo,
        'score': failedArray[i].score,
        // 'departmentId': failedArray[i].departmentId,
        // 'programUnitTypeValueId': failedArray[i].programUnitTypeValueId,
        'error': (failedArray[i].error) ? failedArray[i].error : 'Score/ CGPA should be in the same format for a campus',
        // 'programId': failedArray[i].programId,
      };
      finalArray.push(obj);
    }
    var name = assessmentInput.fileDetails.name;
    var container = assessmentInput.fileDetails.container;
    var pathForm = require('path');
    var errorCsv = './attachments/' + container + '/' + name;
    errorCsv = pathForm.join(__dirname, '../../../', errorCsv);
    var fs = require('fs');
    var csv = require('fast-csv');
    var ws = fs.createWriteStream(errorCsv);
    csv
      .writeToPath(errorCsv, finalArray, {
        headers: true,
      })
      .on('finish', function() {
        // console.log('done!');
      });
    cb(null, finalArray);
    errorArray = [];
    repeated = [];
    final = [];
    finalNonRepeat = [];
    failedArray = [];
    scoreCount = 0;
  }

  function validateInput(validateObj, validateCB) {
    var validateMassUpload = require('../commonValidation/student-assessment-upload-validation').validateJson;
    validateMassUpload(validateObj, function(validationErr, validationResponse) {
      if (validationErr) {
        validateObj.error = validationErr.err;
        errorArray.push(validateObj);
        // createAssessmentUpload(validateObj, validateObj.error, function(postErr, postResp) {
        //   if (postErr) {
        //     validateObj.error = validationErr.err;
        //     errorArray.push(validateObj);
        //     validateCB(null, null);
        //   } else {
        validateCB(null, null);
        //   }
        // });
      } else {
        validateCB(null, validateObj);
      }
    });
  }

  function commonErrorRead(responseCreated, cb) {
    var studentsAssessmentUploadWork = server.models.StudentsAssessmentUploadWork;
    studentsAssessmentUploadWork.find({}, function(workErr, workResponse) {
      if (workErr) {
        studentsAssessmentUploadWork.destroyAll({}, function(error, destroy) {
          throwError(error, cb);
        });
      } else {
        // console.log('=================================', workResponse);
        var campusUploadLog = server.models.CampusUploadLog;
        var details = assessmentInput.fileDetails;
        var container = assessmentInput.fileDetails.container;
        getfailureMass(workResponse, function(err, failedArray) {
          // console.log('workkkkkkkkkkkkkkk', failedArray.length);
          var pathForm = require('path');
          var csvFileLocation = './attachments/' + container + '/' + details.name + '/' + details.originalFileName;
          var errorFileLocation = './attachments/' + container + '/' + 'download' + '/' + details.name;
          csvFileLocation = pathForm.join(__dirname, '../../../', csvFileLocation);
          errorFileLocation = pathForm.join(__dirname, '../../../', errorFileLocation);
          var logInput = {
            'campusUploadTypeValueId': assessmentInput.campusUploadTypeValueId,
            'campusId': assessmentInput.campusId,
            'inputParameters': 'string',
            'createUserId': 1,
            'createDatetime': new Date(),
            'csvFileLocation': csvFileLocation,
            'errorFileLocation': errorFileLocation,
            'totalNoRecs': assessmentData.length,
            'noFailRecs': failedArray.length,
            'noSuccessRecs': (responseCreated) ? cleanArray(responseCreated).length : 0,
          };
          campusUploadLog.create(logInput, function(logErr, logOutput) {
            async.map(responseCreated, calculateCGPA, function(cgpaErr, cgpaOut) {
              studentsAssessmentUploadWork.destroyAll({}, function(error, destroy) {
                cb(null, logOutput);
              });
            });
          });
        });
      }
    });
  }

  function calculateCGPA(resp, cgpaCB) {
    // console.log(resp);
    var enrollment = server.models.Enrollment;
    enrollment.findOne({
      'where': {
        'enrollmentId': resp.enrollmentId,
      },
    }, function(enrollErr, enrollOut) {
      // console.log('enrollllllllllllll ', enrollOut);
      var assessment = server.models.Assessment;
      assessment.find({
        'where': {
          'enrollmentId': resp.enrollmentId,
        },
      }, function(assessErr, assessOut) {
        // console.log('assssssssssssssss ', assessOut);
        var cgpaArr = 0;
        for (var i = 0; i < assessOut.length; i++) {
          cgpaArr = cgpaArr + assessOut[i].score;
        // console.log(cgpaArr);
        }
        var average = (cgpaArr / assessOut.length);
        var object = {};
        object = enrollOut;
        object.cgpaScore = average;
        enrollOut.updateAttributes(object, function(enrollError, enrollmentOut) {
          cgpaCB(null, enrollmentOut);
        });
      });
    });
  }

  function getNonRepeatData(array, cb) {
    if (array == []) {
      cb(null, []);
    } else {
      var studentsAssessmentUploadWork = server.models.StudentsAssessmentUploadWork;
      studentsAssessmentUploadWork.create(array, function(err, res) {
        cb(null, res);
      });
    }
  }

  function seeForRep(array, data, callBack) {
    var count = 0;
    for (var i = 0; i < array.length; i++) {
      if (array[i].admissionNo == data.admissionNo) {
        count++;
        if (count > 1) {
          repeated.push(array[i]);
        }
      }
      if (i == array.length - 1) {
        callBack(null, array);
      }
    }
  }

  function getFinalData(obj, cb) {
    if (obj == null) {
      cb(null, null);
    } else {
      var studentsAssessmentUploadWork = server.models.StudentsAssessmentUploadWork;
      studentsAssessmentUploadWork.find({
        'where': {
          'or': [{
            'admissionNo': obj.admissionNo,
          }],
        },
      }, function(err4, res4) {
        // console.log('length', res4.length, res4);
        // console.log(res4.length === 1, res4.length > 1);
        if (res4.length === 1) {
          finalNonRepeat.push(obj);
          cb(null, 'done');
        } else if (res4.length > 1) {
          repeated.push(obj);
          cb(null, 'done');
        } else {
          cb(null, 'done');
        }
      });
    }
  }

  function insertIntoTemp(obj, callbc) {
    obj.err = 'duplicate';
    createAssessmentUpload(obj, 'Admission Number is Duplicated', function(err3, res3) {
      callbc(null, 'done');
    });
  }
}
exports.createAssessment = createAssessment;
