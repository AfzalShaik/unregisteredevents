'use strict';
var server = require('../server/server');
var eventStudentList = server.models.EventStudentList;
var async = require('async');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
var logger = require('../server/boot/lib/logger');

function updateUnRegStudentEvents(stData, cb) {
  eventStudentList.findOne({
    'where': {
      'and': [{
        'studentListId': stData.studentListId,
      },
      {
        'campusId': stData.campusId,
      },
      ],
    },
  }, function(err, events) {
    var offeredObj = {};
    offeredObj = events;
    offeredObj.candidateStatusValueId = stData.candidateStatusValueId;
    offeredObj.totalCompValue = stData.totalCompValue;
    events.updateAttributes(offeredObj, function(offeredErr, offeredResp) {
      // if (stData.candidateStatusValueId == 381) {
      var pushInput = {
        'userId': stData.userId,
        'role': 'PLCDIR',
        'campusId': stData.campusId,
        'educationPersonId': stData.educationPersonId,
        // 'empEventId': stData.employerEventId,
        'campusEventId': events.campusEventId,
        'notificationName': (stData.candidateStatusValueId == 381) ? 13 : 11,
        'studentList': [{
          'studentId': events.studentId,
        }],
      };
      var notificationTemplate = server.models.NotificationMessageTemplates;
      var pushNotification = notificationTemplate.pushNotification;
      pushNotification(pushInput, function(pushErr, pushOut) {
        // console.log(pushErr, pushOut);
        if (pushErr) {
          cb(pushErr, null);
        } else {
          offeredResp.Notification = 'Notificaton Sent';
          // StudentArray = [];
          cb(null, offeredResp);
        }
      });
      // }
      // cb(null, offeredResp);
    });
  });
}
exports.updateUnRegStudentEvents = updateUnRegStudentEvents;
