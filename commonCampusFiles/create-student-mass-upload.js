'use strict';
var async = require('async');
var server = require('../server/server');
var lookup = require('../commonValidation/lookupMethods').lookupMethod;
var throwerror = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var postIntoUploadWork = require('./student-mass-upload').postIntoUploadWork;
var user = require('../server/config.json');
var host = user.host;
var port = user.port;
var globalPassword;
var nodemailer = require('nodemailer');
var path = require('path');
var signUpService = require('../commonValidation/signup-service.js').createSignUpService;
// console.log('hostttttttttttttttttttttttttttttttttt ', host, port);
function createStudent(finalArray, stDataa, cb) {
  async.map(finalArray, studentInfoCreation, function(studentErr, studentInfo) {
    // console.log('...........', studentInfo);
    cb(null, studentInfo);
  });

  function studentInfoCreation(obj, callBc) {
    var scorauser = server.models.ScoraUser;
    var enrollment = server.models.Enrollment;
    if (obj.skipUserInd == 'Y' || obj.skipUserInd == 'y') {
      var enrollmentObj = {
        'studentId': obj.studentId,
        'programId': obj.programId,
        'admissionNo': obj.admissionNo,
        'startDate': obj.startDate,
        'planedCompletionDate': obj.plannedCompletionDate,
        'dataVerifiedInd': 'Y',
        'createDatetime': new Date(),
        'updateDatetime': new Date(),
        'createUserId': 1,
        'updateUserId': 1,
        'campusId': obj.campusId,
      };
      enrollment.create(enrollmentObj, function(enrollError, enrollObj) {
        if (enrollError) {
          obj.error = 'Invalid Admission Number';
          postIntoUploadWork(obj, function(error, errorResponse) {
            callBc(null, null);
          });
        } else {
          callBc(null, obj);
        }
      });
    } else {
      lookup('ROLE_TYPE_CODE', function(err, response) {
        var studentIndicator = response.find(findStudent);
        // To Get Student Role_Type_Value_Id
        function findStudent(studentVal) {
          return studentVal.lookupValue === 'Student';
        }
        var input = {
          'email': obj.email,
          'roleTypeValueId': studentIndicator.lookupValueId,
          'firstName': obj.firstName,
          'lastName': obj.lastName,
          'programId': stDataa.programId,
          'admissionNo': obj.admissionNo,
          'startDate': obj.startDate,
          'planedCompletionDate': obj.plannedCompletionDate,
          'campusId': obj.campusId,
        };
        // console.log('input::::::::::;; ', obj);
        var url = 'http://' + host + ':' + port + '/api/ScoraUsers';
        // console.log('urllllllllllllllllllllllllllllllll ', url);
        var user = server.models.ScoraUser;
        var randomstring = require('randomstring');
        globalPassword = randomstring.generate({
          length: 12,
          charset: 'alphanumeric',
        });
        input.createDatetime = new Date();
        input.updateDatetime = new Date();
        input.createUserId = 1;
        input.updateUserId = input.createUserId;
        input.password = globalPassword;
        // console.log('inputttttttttttttttttttt ', input);
        user.create(input, function(err, reso) {
          var options = {
            type: 'email',
            to: reso.email,
            from: 'noreply@loopback.com',
            subject: 'Thanks for registering.',
            template: path.resolve(__dirname, '../server/views/verify.ejs'),
            redirect: '/verified',
            user: user,
            html: 'Use this Password to reset: ' + globalPassword,
          };
          // console.log('options', options);
          reso.verify(options, function(err, response, next) {
            // console.log('789999999999999999999999');
            // console.log('errrrrrrrr ', err);
            // if (err) {
            //   // signUpService(reso, function(campusErr, CampusResponse) {
            //   //   console.log('/////////////', CampusResponse, campusErr);
            //   //   if (campusErr) {
            //   //     callBc(null, null);
            //   //   } else {
            //       // console.log(CampusResponse);
            //   callBc(null, null);
            //   //   }
            //   // });
            // } else {
            var userRole = server.models.ScoraUserRole;
            var userRoleObj = {
              'id': reso.id,
              'roleCode': 'STUDENT',
              'startDate': new Date(),
              'createDatetime': new Date(),
              'updateDatetime': new Date(),
              'createUserId': 1,
              'updateUserId': 1,
            };
            userRole.create(userRoleObj, function(roleError, roleResponse) {
              // console.log('////////////////', roleError);
              if (roleError) {
                callBc(null, null);
              } else {
                var student = server.models.Student;
                var studentObj = {
                  'id': reso.id,
                  'studentStatusValueId': 246,
                  'firstName': reso.firstName,
                  'lastName': reso.lastName,
                  'createDatetime': new Date(),
                  'updateDatetime': new Date(),
                  'createUserId': 1,
                  'updateUserId': 1,
                };
                student.create(studentObj, function(studentErr, studentResp) {
                  // console.log('studenterrrrrrrrrrrrrrrrrrrrrrrrrrrrrr ', studentErr);
                  if (studentErr) {
                    throwError(studentErr, callBc);
                  } else {
                    var campusId = (reso.campusId) ? reso.campusId : undefined;
                    var enrollmentObj = {
                      'studentId': studentResp.studentId,
                      'programId': reso.programId,
                      'admissionNo': reso.admissionNo,
                      'startDate': reso.startDate,
                      'planedCompletionDate': (reso.planedCompletionDate) ? reso.planedCompletionDate : reso.plannedCompletionDate,
                      'dataVerifiedInd': 'N',
                      'createDatetime': new Date(),
                      'updateDatetime': new Date(),
                      'createUserId': 1,
                      'updateUserId': 1,
                      'campusId': campusId,
                    };
          
                    enrollment.create(enrollmentObj, function(enrollmentError, enrollmentResponse) {
              // console.log('enrollmenterrrrrrrrrrrrrrrrrrrrrrr ', enrollmentError);
                      if (enrollmentError) {
                        throwError(enrollmentError, callBc);
                      } else {
                        callBc(null, enrollmentResponse);
                      }
                    });
                  }
                });
              }
            });
            // }
          });
          // next();
        });
      });
    }
  }
}
exports.createStudent = createStudent;

function waitTillReqEnd(rs, callback) {
  setTimeout(function() {
  }, 10000);
}
