'use strict';
var server = require('../server/server');
var eventStudentList = server.models.EventStudentList;
var async = require('async');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
/**
 *getStudentEvents- To get student Event details by comaping with event status value id
 *@constructor
 * @param {number} studentId - Unique id for ecah and every student
 * @param {function} cb - deals with response
 */
function getStudentEvents(studentId, cb) {
  eventStudentList.find({
    'where': {
      'studentListId': studentListId,
    },
  }, function(err, eventResp) {
    if (eventResp.length > 0) {
      getEventList(eventResp[0].campusId, function(error, eventList) {
        if (eventList.length > 0) {
          async.map(eventList, getOpenEvents, function(err, openEvents) {
            var output = [];
            output = cleanArray(openEvents);
            cb(null, output);
          });
        } else {
          cb(error, null);
        }
      });
    } else {
      cb(err, null);
    }
  });
}
/**
 *getEventList- To get list of event details by taking required fields
 *@constructor
 * @param {number} campusId - unique id for each and every campus
 * @param {function} cb - deals with response
 */
function getEventList(campusId, cb) {
  var campusEvent = server.models.CampusEvent;
  campusEvent.find({
    'where': {
      'and': [{
        'campusId': campusId,
      }],
    },
  }, function(err, events) {
    cb(null, events);
  });
}

function getOpenEvents(obj, callBc) {
  var lookup = require('../commonValidation/lookupMethods').lookupMethod;
  lookup('EDUCATION_EVENT_STATUS_CODE', function(err, response) {
    var eventStatusValueId = response.find(findIndicatior);

    function findIndicatior(companyVal) {
      return companyVal.lookupValue === 'Closed';
    }
    if (obj.eventStatusValueId == eventStatusValueId.lookupValueId) {
      callBc(null, null);
    } else {
      callBc(null, obj);
    }
  });
}
exports.getStudentEvents = getStudentEvents;
