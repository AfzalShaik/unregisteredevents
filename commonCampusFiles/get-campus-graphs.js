'use strict';
var server = require('../server/server');
var department = server.models.Department;
var enrollment = server.models.Enrollment;
var errorFunction = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var async = require('async');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
var getLookups = require('./get-campus-dashboard').getLookups;
var eventStudentList = server.models.EventStudentList;
function getDepartmentGraphs(inputObj, callBc) {
  var program = server.models.Program;
  program.find({
    'where': {
      'and': [{
        'campusId': inputObj.campusId,
      },
      {
        'departmentId': inputObj.departmentId,
      },
      ],
    },
  }, function(departErr, departResponse) {
    if (departErr) {
      errorFunction(departErr, callBc);
    } else {
      async.map(departResponse, getEachProgram, function(err, response) {
        if (err) {
          errorFunction(err, callBc);
        } else {
          callBc(null, response);
        }
      });
    }
  });

  function getEachProgram(obj, cb) {
    var enrollment = server.models.Enrollment;
    enrollment.find({
      'where': {
        'programId': obj.programId,
      },
    }, function(err, resp) {
      if (err) {
        cb(err, null);
      } else {
        async.map(resp, getStudentsInfo, function(error, studentsResponse) {
          searchCampusName('yes', studentsResponse, function(rrrr, reposit) {
            // console.log(reposit);
            var studentsInfo = {};
            studentsInfo.noOfStudentsEnrolled = resp.length;
            studentsInfo.programName = obj.programName;
            studentsInfo.programId = obj.programId;
            studentsInfo.unAttendedStudents = resp.length - reposit.length;
            studentsInfo.attendedStudents = reposit.length;
            async.map(reposit, getPlacedStudents, function(placedErr, placed) {
              // console.log('..................', cleanArray(placed));
              studentsInfo.placedStudents = cleanArray(placed).length;
              studentsInfo.compensation = cleanArray(placed);
              async.map(reposit, getshortListedStudents, function(shortErr, shortListed) {
                studentsInfo.shortListedStudents = cleanArray(shortListed).length;
                console.log(studentsInfo);
                cb(null, studentsInfo);
              });
            });
          });
        });
      }
    });
  }

  function getStudentsInfo(object, callBack) {
    var eventStudentList = server.models.EventStudentList;
    var eventPlacedObj = {};
    eventPlacedObj['studentId'] = object.studentId;
    eventStudentList.find({
      'where': {
        'and': [eventPlacedObj],
      },
    }, function(error, result) {
      if (error) {
        callBack(error, null);
      } else {
        var studentsObj = {};
        studentsObj['list'] = (result.length > 0) ? 'yes' : 'no';
        studentsObj['studentId'] = (result.length > 0) ? object.studentId : null;
        callBack(null, studentsObj);
      }
    });
  }

  function searchCampusName(searchName, myArray, callBc) {
    async.map(myArray, searchWithName, function(error, response) {
      var output = [];
      output = cleanArray(response);
      callBc(null, output);
    });
    function searchWithName(object, cb) {
      var searchShort = (object.list) ? object.list.indexOf(searchName) >= 0 : false;
      if (searchShort) {
        cb(null, object);
      } else {
        cb(null, null);
      }
    }
  }
  function getPlacedStudents(inputObj, cb) {
    getLookups('CANDIDATE_STATUS_TYPE', 'Offered', function(candidateStatusOffered) {
      var eventPlacedObj = {};
      eventPlacedObj['studentId'] = inputObj.studentId;
      eventPlacedObj['candidateStatusValueId'] = candidateStatusOffered.lookupValueId;
      eventStudentList.find({
        'where': {
          'and': [eventPlacedObj],
        },
      }, function(error, result) {
        if (error) {
          cb(error, null);
        } else {
          var resultArray = cleanArray(result);
          cb(null, resultArray[0]);
        }
      });
    });
  }
  function getshortListedStudents(inputObj, cb) {
    getLookups('CANDIDATE_STATUS_TYPE', 'Shortlisted', function(candidateStatusShortListed) {
      var eventShortObj = {};
      eventShortObj['studentId'] = inputObj.studentId;
      eventShortObj['candidateStatusValueId'] = candidateStatusShortListed.lookupValueId;
      eventStudentList.find({
        'where': {
          'and': [eventShortObj],
        },
      }, function(error, result) {
        if (error) {
          cb(error, null);
        } else {
          cb(null, result[0]);
        }
      });
    });
  }
  function calculateCompensation(obj, cb) {
    // console.log('--------------------- ', obj);
    var compensationPackage = server.models.CompensationPackage;
    compensationPackage.findOne({'where': {'compPackageId': obj.compPackageId}}, function(compErr, compResp) {
      if (compErr) {
        errorFunction(compErr, cb);
      } else {
        // console.log('...............', compResp);
        cb(null, compResp);
      }
    });
  }
}
exports.getDepartmentGraphs = getDepartmentGraphs;
