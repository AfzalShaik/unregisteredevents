  'use strict';
  var server = require('../server/server');
  var logger = require('../server/boot/lib/logger');
  var contactValidation = require('../commonValidation/contactValidation'); // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Not being used. Remove unecessary imports if not used. Else check why not used.  
  var updateContact = require('../commonValidation/updateContact');
  var CampusContact = server.models.CampusContact;
  var updateContactService = require('../commonValidation/update-service-contact');
  var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *updateCampusContactService-function deals with updating the contact data of campus
   *@constructor
   * @param {object} contactData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
  function updateCampusContactService(contactData, cb) {
    //validating the input

    if (contactData.contactId && contactData.campusId) {
      //checking the record with campusId and contactId
      CampusContact.findOne({
        'where': {
          'and': [{
            'campusId': contactData.campusId,
          }, {
            'contactId': contactData.contactId,
          }],
        },
      }, function(err, contactList) {
        if (err) {
          cb(err, contactData);
        } else if (contactList) {
          logger.info('for provided campusId and contactId record found'); // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Use debug() method for such statements. 
          updateContactService.updateContactService(contactData, function(error, info) {
            if (error) {
              cb(error, contactData);
            } else {
              logger.info('campus contact updated successfully');
              cb(null, info);
            }
          });
        } else {
          //throws error in case of Invalid contatcId or campusId
          logger.error('Invalid contatcId or campusId ');
          throwError('Invalid contatcId or campusId', cb);
        }
      });
    } else {
      logger.error('ContactId and campusId are required field');
      throwError('ContactId and campusId are required field', cb);
    }
  };
  exports.updateCampusContactService = updateCampusContactService;
